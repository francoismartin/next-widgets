// Fix for TS1208
export {};

// Mock cookies
document.cookie = 'Mookie';

// Mock .env
process.env.NEXT_PUBLIC_UNIQUE_VALIDATION_DEBOUNCE = '750';
process.env.NEXT_PUBLIC_CONTACT_EMAIL = 'chuck@norris.private';

/**
 * Enables snapshot testing with stable id's.
 * Replaces id's like {@code id="mui-411903247"} with {@code id="mui-test-id"}.
 * @see https://github.com/mui-org/material-ui/issues/21293#issuecomment-850914765
 */
jest.mock('@mui/utils/useId', () => jest.fn().mockReturnValue('mui-test-id'));
