const config = {
  testEnvironment: 'jsdom',
  rootDir: '../..',
  roots: ['<rootDir>/src'],
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx)$'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'json'],
  setupFiles: ['<rootDir>/scripts/jest/testSetup.ts'],
  setupFilesAfterEnv: [
    '@testing-library/react-hooks/disable-error-filtering.js',
    '<rootDir>/scripts/jest/testSetupAfterEnv.ts',
  ],
  collectCoverage: true,
  collectCoverageFrom: [
    '**/src/**/*.{ts,tsx}',
    '!**/*.test.{ts,tsx}',
    '!**/*.stories.{ts,tsx}',
    '!**/src/types/**',
  ],
  snapshotSerializers: ['@emotion/jest/serializer'],
  moduleDirectories: ['node_modules', 'src'],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/lib/api/config.ts',
    '<rootDir>/src/lib/testUtils.ts',
    '<rootDir>/src/lib/testId.ts',
    '<rootDir>/src/lib/config.ts',
    '<rootDir>/src/lib/i18n.ts',
    '<rootDir>/.eslintrc.js',
    '<rootDir>/coverage/.*',
  ],
  moduleNameMapper: {
    // Jest 28 specific fix/hack.
    // See https://github.com/microsoft/accessibility-insights-web/pull/5421#issuecomment-1109168149
    '^uuid$': '<rootDir>/node_modules/uuid/dist/index.js',
  },
};

// eslint-disable-next-line no-undef
module.exports = config;
