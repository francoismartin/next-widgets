# Contributing to next-widgets

## Commit Message Guidelines

This project follows the [Angular commit message
guidelines](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines)
for its commit messages.

### Template

Fields shown in `[square brackets]` are optional.

```text
type[(scope)]: subject line - max 100 characters

[body] - extended description of commit that can stretch over
multiple lines. Max 100 character per line.

[footer] - links to issues with (Closes #, Fixes #, Relates #) and
BREAKING CHANGE:
```

### Examples

```text
feat(projects): limit names to only contain lowercase alphanumeric characters

Validates project names to only allow lowercase alphanumeric characters.
Having project names only consisting of lowercase alphanumeric characters
makes them more consistent, more easily readable and easier to remember.

BREAKING CHANGE: It is no longer possible to use any other characters in
project names than lowercase alphanumeric characters.

Handling it through a database migration instead of making this a breaking
change was considered, however we thought it would be preferable to keep
displaying the previous name until the project is edited for the first time,
where the validation enforces the correct naming scheme. This allows users to
choose which name fits best, without risking automatically migrating to a name
that is not suitable.

To migrate, edit the projects which have names including characters that
are not allowed, and save them with a new name which only includes
alphanumeric characters.


Closes #7, #123, Related #23
```

```text
fix(widgets/Accordion): ensure name is only visible once

Previously, adding an accordion resulted in the name being shown twice.


Closes #141
```

```text
docs(CONTRIBUTING): add examples of conventional commit messages

Add a few examples of conventional commit messages to CONTRIBUTING.md, so that
people don't have to click on the "Angular commit message guidelines" link to
read the full specifications. Add a concise summary of the guidelines to
provider a reminder of the main types and keywords.


Closes #114, #139
```

### Type and keywords summary

**Type:** the following types are allowed. Only types shown in **bold** - and
those containing the `BREAKING CHANGE:` keyword - are automatically added to the
changelog:

- **feat**: new feature
- **fix**: bug fix
- build: changes that affect the build system or external dependencies.
- ci: changes to CI configuration files and scripts.
- docs: documentation only changes
- perf: code change that improves performance.
- refactor: code change that neither fixes a bug nor adds a feature
- style: change in code formatting only (no effect on functionality).
- test: change in unittest files only.

**Scope:**

- name of the file/functionality/workflow affected by the commit.

**Subject line:**

- one line description of commit with max 100 characters.
- use the imperative form, e.g. "add new feature" instead of "adds new feature"
  or "added a new feature".
- no "." at the end of subject line.

**body:**

- Extended description of commit that can stretch over multiple lines.
- Max 100 characters per line.
- Explain things such as what problem the commit addresses, background info on
  why the change was made, alternative implementations that were tested but
  didn't work out.

**footer:**

- Reference to git issue with `Closes/Close`, `Fixes/Fix`, `Related`.
- Location for `BREAKING CHANGE:` keyword. Add this keyword followed by a
  description of what the commit breaks, why it was necessary, and how users
  should port their code to adapt to the new version.

### Commit messages and auto-versioning

The following rules are applied by the auto-versioning system to modify the
version number when a new commit is pushed to the `master` branch:

- Keyword `BREAKING CHANGE:`: increases the new major digit, e.g. 1.1.1 ->
  2.0.0
- Type `feat`: increases the minor version digit, e.g. 1.1.1 -> 1.2.0
- Type `fix`: increases the patch digit, e.g. 1.1.1 -> 1.1.2

**Note:** an exception to the behavior of `BREAKING CHANGE:` is for pre-release
versions (i.e. 0.x.x). In that case, a `BREAKING CHANGE:` keyword increases the
minor digit rather than the major digit. For more details, see the [conventional
commits](https://www.conventionalcommits.org/en/v1.0.0/) and the [semantic
versioning](https://semver.org/spec/v2.0.0.html) specifications.

### Setting up the commit message template

To show this template when running `git commit` from your terminal, run

```bash
git config commit.template .gitmessage
```

or manually add the following block to `.git/config`:

```text
[commit]
  template = .gitmessage
```

If you want this configuration to be applied to all the repositories in your
home directory, run `git config` with the `--global` option, or add the
above-mentioned block to your `~/.gitconfig` file instead. Be aware that
repository specific configurations will still override any user level - or
system level - defined configuration.

### Using `commitizen`

This project is [Commitizen friendly](http://commitizen.github.io/cz-cli/)! If
you want to be sure to comply to the template when writing your commit messages,
you can use `npx cz` instead of `git commit`. This will prompt you with a series
of questions, aimed at creating a template compliant commit message.
