import {
  correlationIdHeaderName,
  easterDate,
  formatDate,
  formatTimestamp,
  getCorrelationId,
  isBetweenIgnoreYear,
  isChristmas,
  isEaster,
  isHalloween,
  isInteger,
  isIpInRange,
  isNationalDay,
  isNewYear,
  isNotEmpty,
  joinIfArray,
  mailto,
  stringToBoolean,
  toArray,
} from './utils';
import { mockTimeZone } from './testUtils';
// Polyfill for `Response` and `isResponse` in a Node context
require('cross-fetch');

describe('utils', () => {
  describe('isIpInRange', () => {
    test('isIpInRange', () => {
      expect(isIpInRange('192.168.1.1', '192.168.1.1', 32)).toBe(true);
      expect(isIpInRange('193.168.1.1', '192.168.1.1', 8)).toBe(false);
      expect(isIpInRange('192.168.2.1', '192.168.0.0', 16)).toBe(true);
    });
  });

  describe('formatDate', () => {
    const dateLong = new Date('December 17, 1995 03:24:00');
    const dateIso = new Date('1995-12-17T03:24:00');
    const dateNumbers = new Date(1995, 11, 17, 3, 24, 0);
    const dateEpochString = 819167040000;
    const dateEpoch = new Date(dateEpochString);

    const dateString = '1995-12-17';

    it('should return only date without time when `withTime` is not specified', () => {
      expect(formatDate(dateIso)).toEqual(dateString);
    });

    it.each`
      date           | formattedDate
      ${undefined}   | ${undefined}
      ${null}        | ${undefined}
      ${dateLong}    | ${dateString}
      ${dateIso}     | ${dateString}
      ${dateNumbers} | ${dateString}
      ${dateEpoch}   | ${dateString}
    `(
      'should return "$formattedDate" when formatting "$date"',
      ({ date, formattedDate }) => {
        expect(formatDate(date, false)).toEqual(formattedDate);
      }
    );

    describe('formatTimestamp', () => {
      const dateLong = new Date('December 17, 1995 12:34:56.789');
      const dateIso = new Date('1995-12-17T12:34:56.789');
      const dateNumbers = new Date(1995, 11, 17, 12, 34, 56, 789);

      const dateString = '1995-12-17_12-34-56';
      const dateStringMilliseconds = dateString + '_789';

      it.each`
        withMilliseconds | outputRegex
        ${false}         | ${/\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}/}
        ${true}          | ${/\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}_\d{3}/}
      `(
        'should return timestamp with current date and time if no date is specified',
        ({ withMilliseconds, outputRegex }) => {
          expect(formatTimestamp(undefined, withMilliseconds)).toMatch(
            outputRegex
          );
        }
      );

      it.each`
        date           | withMilliseconds | formattedTimestamp
        ${dateLong}    | ${false}         | ${dateString}
        ${dateIso}     | ${false}         | ${dateString}
        ${dateNumbers} | ${false}         | ${dateString}
        ${dateLong}    | ${true}          | ${dateStringMilliseconds}
        ${dateIso}     | ${true}          | ${dateStringMilliseconds}
        ${dateNumbers} | ${true}          | ${dateStringMilliseconds}
      `(
        'should return "$formattedTimestamp" when formatting "$date" with milliseconds: $withMilliseconds',
        ({ date, withMilliseconds, formattedTimestamp }) => {
          expect(formatTimestamp(date, withMilliseconds)).toEqual(
            formattedTimestamp
          );
        }
      );
    });

    describe('Timezones', () => {
      const dateUtcString = '2020-11-03T17:22:18.896626Z';

      describe('Europe/London', () => {
        const dateTimeString = '1995-12-17 02:24';
        const dateTimeUtcString = '2020-11-03 17:22';

        beforeEach(() => {
          mockTimeZone();
        });

        it.each`
          dateString         | formattedDate
          ${dateEpochString} | ${dateTimeString}
          ${dateUtcString}   | ${dateTimeUtcString}
        `(
          'should return "$formattedDate" when formatting "$dateString"',
          ({ dateString, formattedDate }) => {
            const date = new Date(dateString);
            expect(formatDate(date, true)).toEqual(formattedDate);
          }
        );
      });

      describe('US/Pacific', () => {
        const dateTimeString = '1995-12-16 18:24';
        const dateTimeUtcString = '2020-11-03 09:22';

        beforeEach(() => {
          mockTimeZone('US/Pacific');
        });

        it.each`
          dateString         | formattedDate
          ${dateEpochString} | ${dateTimeString}
          ${dateUtcString}   | ${dateTimeUtcString}
        `(
          'should return "$formattedDate" when formatting "$dateString"',
          ({ dateString, formattedDate }) => {
            const date = new Date(dateString);
            expect(formatDate(date, true)).toEqual(formattedDate);
          }
        );
      });
    });
  });

  describe('toArray', () => {
    it.each`
      input        | expectedOutput
      ${null}      | ${[]}
      ${undefined} | ${[]}
      ${0}         | ${[0]}
      ${1}         | ${[1]}
      ${[]}        | ${[]}
      ${[1]}       | ${[1]}
      ${[1, 2]}    | ${[1, 2]}
    `(
      'should return "$expectedOutput" when "$input" has been ' +
        'wrapped as array (if not already yet)',
      ({ input, expectedOutput }) => {
        // when
        const result = toArray(input);

        // then
        expect(result).toMatchObject(expectedOutput);
      }
    );
  });

  describe('joinIfArray', () => {
    const prename = 'Chuck';
    const surname = 'Norris';

    it.each`
      input                 | separator    | expectedOutput
      ${prename}            | ${undefined} | ${prename}
      ${prename}            | ${','}       | ${prename}
      ${[prename, surname]} | ${undefined} | ${`${prename}-${surname}`}
      ${[prename, surname]} | ${','}       | ${`${prename},${surname}`}
    `(
      'should return "$expectedOutput" when joining together "$path" separated by "$separator"',
      ({ input, separator, expectedOutput }) => {
        // when
        const result = joinIfArray(input, separator);

        // then
        expect(result).toEqual(expectedOutput);
      }
    );
  });

  describe('mailto', () => {
    it.each`
      email        | subject       | body          | expectedOutput
      ${'email'}   | ${undefined}  | ${undefined}  | ${'mailto:email'}
      ${'email'}   | ${'lorem'}    | ${undefined}  | ${'mailto:email?subject=lorem'}
      ${'email'}   | ${undefined}  | ${'ipsum'}    | ${'mailto:email?body=ipsum'}
      ${'email'}   | ${'lorem'}    | ${'ipsum'}    | ${'mailto:email?subject=lorem&body=ipsum'}
      ${'a@a.com'} | ${'lorem'}    | ${'ipsum'}    | ${'mailto:a@a.com?subject=lorem&body=ipsum'}
      ${'email'}   | ${'François'} | ${'ipsum'}    | ${'mailto:email?subject=Fran%C3%A7ois&body=ipsum'}
      ${'email'}   | ${'François'} | ${'Jarosław'} | ${'mailto:email?subject=Fran%C3%A7ois&body=Jaros%C5%82aw'}
    `(
      'should return "$expectedOutput" when making a mailto: link with email: "$email", subject: "$subject" and body: "$body"',
      ({ email, subject, body, expectedOutput }) => {
        // when
        const result = mailto(email, subject, body);

        // then
        expect(result).toEqual(expectedOutput);
      }
    );
  });

  describe('isBetweenIgnoreYear', () => {
    beforeEach(() => {
      mockTimeZone();
    });

    it.each`
      date                          | fromMonth | fromDay | toMonth | toDay | expectedOutput
      ${'2020-11-15T12:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2020-11-30T12:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2020-11-30T23:59:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2020-11-30T23:59:59.999Z'} | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2020-12-01T00:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2020-12-01T12:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2020-12-01T23:59:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2020-12-02T00:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2020-12-02T23:59:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2020-12-02T23:59:59.999Z'} | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2020-12-03T00:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2019-11-15T12:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2019-11-30T12:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2019-11-30T23:59:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2019-11-30T23:59:59.999Z'} | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
      ${'2019-12-01T00:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2019-12-01T12:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2019-12-01T23:59:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2019-12-02T00:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2019-12-02T23:59:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2019-12-02T23:59:59.999Z'} | ${12}     | ${1}    | ${12}   | ${2}  | ${true}
      ${'2019-12-03T00:00:00Z'}     | ${12}     | ${1}    | ${12}   | ${2}  | ${false}
    `(
      'should return "$expectedOutput" when $date is between $fromMonth-$fromDay and $toMonth-$toDay, ignoring the year',
      ({ date, fromMonth, fromDay, toMonth, toDay, expectedOutput }) => {
        expect(
          isBetweenIgnoreYear(fromMonth, fromDay, toMonth, toDay, date)
        ).toEqual(expectedOutput);
      }
    );
  });

  describe('isNotEmpty', () => {
    it.each`
      input                             | expected
      ${{}}                             | ${false}
      ${{ id: 3 }}                      | ${true}
      ${{ id: '3' }}                    | ${true}
      ${{ id: undefined }}              | ${false}
      ${{ id: undefined, name: '' }}    | ${false}
      ${{ id: undefined, name: ' ' }}   | ${true}
      ${{ id: undefined, name: 'foo' }} | ${true}
    `(
      'should return "$expected" when input is $input',
      ({ input, expected }) => {
        expect(isNotEmpty(input)).toEqual(expected);
      }
    );
  });

  describe('isInteger', () => {
    it.each`
      input                              | expectedOutput
      ${null}                            | ${false}
      ${undefined}                       | ${false}
      ${0}                               | ${false}
      ${1}                               | ${false}
      ${2}                               | ${false}
      ${-1}                              | ${false}
      ${{}}                              | ${false}
      ${'foo'}                           | ${false}
      ${'0a0'}                           | ${false}
      ${'0 a 0'}                         | ${false}
      ${'0.1'}                           | ${false}
      ${'0'}                             | ${true}
      ${'0.0'}                           | ${true}
      ${'1'}                             | ${true}
      ${'1.0'}                           | ${true}
      ${'2'}                             | ${true}
      ${'2.0'}                           | ${true}
      ${String(Number.MAX_SAFE_INTEGER)} | ${true}
      ${String(Number.MIN_SAFE_INTEGER)} | ${true}
      ${'2 '}                            | ${true}
      ${' 2'}                            | ${true}
      ${' 2 '}                           | ${true}
      ${'    2   '}                      | ${true}
    `(
      'should return $expectedOutput for the value $input',
      ({ input, expectedOutput }) => {
        expect(isInteger(input)).toEqual(expectedOutput);
      }
    );
  });

  describe('stringToBoolean', () => {
    it.each`
      input        | expectedOutput
      ${null}      | ${false}
      ${undefined} | ${false}
      ${''}        | ${false}
      ${' '}       | ${false}
      ${0}         | ${false}
      ${1}         | ${false}
      ${'0'}       | ${false}
      ${'1'}       | ${false}
      ${{}}        | ${false}
      ${'true'}    | ${true}
      ${'True'}    | ${false}
      ${'false'}   | ${false}
      ${'False'}   | ${false}
    `(
      'should return $expectedOutput for the value $input',
      ({ input, expectedOutput }) => {
        expect(stringToBoolean(input)).toEqual(expectedOutput);
      }
    );
  });

  describe('getCorrelationId', () => {
    const backendCorrelationId = 'foo';
    const uuidLength = 22;

    it('should extract the correlationId from the headers if a response is provided', () => {
      const r = new Response();
      r.headers.set(correlationIdHeaderName, backendCorrelationId);
      expect(getCorrelationId(r)).toEqual(backendCorrelationId);
    });

    it.each`
      input             | type
      ${new Response()} | ${'a response is provided that does not have a header with a correlationId'}
      ${new Error()}    | ${'an error object is provided'}
      ${{}}             | ${'an empty object is provided'}
      ${'foo'}          | ${'a string is provided'}
    `('should generate a new correlationId if $type', ({ input }) => {
      expect(getCorrelationId(input)).toHaveLength(uuidLength);
    });
  });

  describe('seasonalChecks', () => {
    beforeEach(() => {
      mockTimeZone('UTC');
    });

    describe('easterDate', () => {
      // Source: https://www.census.gov/srd/www/genhol/easter500.html
      it.each`
        year    | expectedMonth | expectedDay
        ${1600} | ${4}          | ${2}
        ${1601} | ${4}          | ${22}
        ${1698} | ${3}          | ${30}
        ${1699} | ${4}          | ${19}
        ${1900} | ${4}          | ${15}
        ${1901} | ${4}          | ${7}
        ${1998} | ${4}          | ${12}
        ${1999} | ${4}          | ${4}
        ${2000} | ${4}          | ${23}
        ${2001} | ${4}          | ${15}
        ${2098} | ${4}          | ${20}
        ${2099} | ${4}          | ${12}
      `(
        'should return $year-$expectedMonth-$expectedDay for year $year',
        ({ year, expectedMonth, expectedDay }) => {
          const easter = easterDate(year);
          expect(easter.getUTCFullYear()).toEqual(year);
          // month is zero-based
          expect(easter.getUTCMonth() + 1).toEqual(expectedMonth);
          expect(easter.getUTCDate()).toEqual(expectedDay);
        }
      );
    });

    describe('isNewYear', () => {
      it.each`
        currentDate               | expectedOutput
        ${'2020-12-31T23:59:00Z'} | ${false}
        ${'2020-01-01T00:00:00Z'} | ${true}
        ${'2020-01-03T18:24:00Z'} | ${true}
        ${'2020-01-06T23:59:00Z'} | ${true}
        ${'2020-01-07T00:00:00Z'} | ${false}
        ${'2020-07-01T00:00:00Z'} | ${false}
        ${'2019-12-31T23:59:00Z'} | ${false}
        ${'2019-01-01T00:00:00Z'} | ${true}
        ${'2019-01-03T18:24:00Z'} | ${true}
        ${'2019-01-06T23:59:00Z'} | ${true}
        ${'2019-01-07T00:00:00Z'} | ${false}
        ${'2019-07-01T00:00:00Z'} | ${false}
      `(
        'should return "$expectedOutput" when the current date is $currentDate',
        ({ currentDate, expectedOutput }) => {
          expect(isNewYear(currentDate)).toEqual(expectedOutput);
        }
      );
    });

    describe('isEaster', () => {
      it.each`
        currentDate               | expectedOutput
        ${'2021-03-20T23:59:59Z'} | ${false}
        ${'2021-03-21T00:00:00Z'} | ${true}
        ${'2021-04-04T00:00:00Z'} | ${true}
        ${'2021-04-11T23:59:59Z'} | ${true}
        ${'2021-04-12T00:00:00Z'} | ${false}
        ${'2022-04-02T23:59:59Z'} | ${false}
        ${'2022-04-03T00:00:00Z'} | ${true}
        ${'2022-04-17T00:00:00Z'} | ${true}
        ${'2022-04-24T23:59:59Z'} | ${true}
        ${'2022-04-25T00:00:00Z'} | ${false}
      `(
        'should return "$expectedOutput" when the current date is $currentDate',
        ({ currentDate, expectedOutput }) => {
          expect(isEaster(currentDate)).toEqual(expectedOutput);
        }
      );
    });

    describe('isNationalDay', () => {
      it.each`
        currentDate               | expectedOutput
        ${'2020-07-25T23:59:59Z'} | ${false}
        ${'2020-07-26T00:00:00Z'} | ${true}
        ${'2020-08-01T18:24:00Z'} | ${true}
        ${'2020-08-08T23:59:59Z'} | ${true}
        ${'2020-08-09T00:00:00Z'} | ${false}
        ${'2020-07-01T00:00:00Z'} | ${false}
        ${'2019-07-25T23:59:59Z'} | ${false}
        ${'2019-07-26T00:00:00Z'} | ${true}
        ${'2019-08-01T18:24:00Z'} | ${true}
        ${'2019-08-08T23:59:59Z'} | ${true}
        ${'2019-08-09T00:00:00Z'} | ${false}
        ${'2019-07-01T00:00:00Z'} | ${false}
      `(
        'should return "$expectedOutput" when the current date is $currentDate',
        ({ currentDate, expectedOutput }) => {
          expect(isNationalDay(currentDate)).toEqual(expectedOutput);
        }
      );
    });

    describe('isHalloween', () => {
      it.each`
        currentDate               | expectedOutput
        ${'2020-10-24T23:59:00Z'} | ${false}
        ${'2020-10-25T00:00:00Z'} | ${true}
        ${'2020-10-31T18:24:00Z'} | ${true}
        ${'2020-11-07T23:59:00Z'} | ${true}
        ${'2020-11-08T00:00:00Z'} | ${false}
        ${'2020-07-01T00:00:00Z'} | ${false}
        ${'2019-10-24T23:59:00Z'} | ${false}
        ${'2019-10-25T00:00:00Z'} | ${true}
        ${'2019-10-31T18:24:00Z'} | ${true}
        ${'2019-11-07T23:59:00Z'} | ${true}
        ${'2019-11-08T00:00:00Z'} | ${false}
        ${'2019-07-01T00:00:00Z'} | ${false}
      `(
        'should return "$expectedOutput" when the current date is $currentDate',
        ({ currentDate, expectedOutput }) => {
          expect(isHalloween(currentDate)).toEqual(expectedOutput);
        }
      );
    });

    describe('isChristmas', () => {
      it.each`
        currentDate               | expectedOutput
        ${'2020-11-30T23:59:00Z'} | ${false}
        ${'2020-12-01T00:00:00Z'} | ${true}
        ${'2020-12-15T18:24:00Z'} | ${true}
        ${'2020-12-24T23:59:00Z'} | ${true}
        ${'2020-12-25T23:59:00Z'} | ${true}
        ${'2020-12-26T00:00:00Z'} | ${true}
        ${'2020-12-31T23:59:00Z'} | ${true}
        ${'2021-01-01T00:00:00Z'} | ${false}
        ${'2019-11-30T23:59:00Z'} | ${false}
        ${'2019-12-01T00:00:00Z'} | ${true}
        ${'2019-12-15T18:24:00Z'} | ${true}
        ${'2019-12-24T23:59:00Z'} | ${true}
        ${'2019-12-25T23:59:00Z'} | ${true}
        ${'2019-12-26T00:00:00Z'} | ${true}
        ${'2019-12-31T23:59:00Z'} | ${true}
        ${'2020-01-01T00:00:00Z'} | ${false}
      `(
        'should return "$expectedOutput" when the current date is $currentDate',
        ({ currentDate, expectedOutput }) => {
          expect(isChristmas(currentDate)).toEqual(expectedOutput);
        }
      );
    });
  });
});
