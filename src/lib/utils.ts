import isString from 'lodash/isString';
import short from 'short-uuid';
import dayjs from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';
import utc from 'dayjs/plugin/utc';
import { isNil } from 'lodash';

export function isIpInRange(ip: string, range: string, mask: number): boolean {
  return ipToBits(ip) >> (32 - mask) === ipToBits(range) >> (32 - mask);
}

export function ipToBits(ip: string): number {
  return ip
    .split('.')
    .map(parseInt)
    .reduce((sum, item) => (sum << 8) | item);
}

export function formatDate(
  date: Date | undefined | null,
  withTime = false
): string | undefined {
  if (!date) {
    return;
  }
  const format = withTime ? 'YYYY-MM-DD HH:mm' : 'YYYY-MM-DD';
  return dayjs(date).format(format);
}

/**
 * Formats the {@code date} as a timestamp.
 * @param date to format, uses current time if not defined
 * @param withMilliseconds whether to include milliseconds or not (not included by default)
 */
export function formatTimestamp(date?: Date, withMilliseconds = false): string {
  const format = 'YYYY-MM-DD_HH-mm-ss';
  return dayjs(date).format(withMilliseconds ? format + '_SSS' : format);
}

export function getTimestamp(): string {
  return formatTimestamp();
}

/**
 * Returns a filename including a timestamp.
 * @param filename of the file (for example: `data-transfer`)
 * @param extension of the file INCLUDING the leading dot (for example: `.csv`)
 */
export function getFilenameWithTimestamp(
  filename: string,
  extension = ''
): string {
  return `${filename}_${getTimestamp()}${extension}`;
}

/**
 * Highest z-index to make sure elements are on top of everything else.
 * @see https://psuter.net/2019/07/07/z-index
 */
export const Z_INDEX_TOP_MOST = 9999;

/**
 * If an array of strings is passed in, it will join the strings together,
 * separated by {@code separator}.
 * Otherwise, it will return the original string.
 *
 * @param stringOrArray might be a string or an array of strings
 * @param separator which separator to join {@code stringOrArray} with,
 *                  in case it's an array of strings
 * @example
 *
 * joinIfArray("string");
 * // => "test"
 *
 * joinIfArray(["string1", "string2"]);
 * // => "string1-string2"
 */
export function joinIfArray(
  stringOrArray: string | string[],
  separator = '-'
): string {
  if (isString(stringOrArray)) {
    return stringOrArray;
  } else {
    return stringOrArray.join(separator);
  }
}

/**
 * Ensures that given item is packed into an array (if not already).
 *
 * @param item might be an array or a single item, or 'undefined'
 */
export const toArray = <T>(item: T | T[] | undefined | null): T[] => {
  return ([] as T[]).concat(isNil(item) ? [] : item);
};

export const generateShortUuid = (): string => short.generate();

export type SerializedResponse = Omit<Partial<Response>, 'headers' | 'body'> & {
  headers: Record<string, string>;
  body: Record<string, unknown> | string;
};

export async function serializeResponse(
  response: Response
): Promise<SerializedResponse> {
  const serialized: SerializedResponse = {
    headers: Object.fromEntries(response.headers.entries()),
    ok: response.ok,
    redirected: response.redirected,
    status: response.status,
    statusText: response.statusText,
    type: response.type,
    url: response.url,
    body: { notSerialized: true },
  };
  const contentType = response.headers.get('content-type');
  if (contentType?.startsWith('application/json')) {
    return response
      .json()
      .then((bodyJson) => {
        serialized.body = bodyJson;
        return serialized;
      })
      .catch((reason) => {
        serialized.body = {
          serializationJsonError: reason,
        };
        return serialized;
      });
  } else {
    return response
      .text()
      .then((bodyText) => {
        serialized.body = bodyText;
        return serialized;
      })
      .catch((reason) => {
        serialized.body = {
          serializationTextError: reason,
        };
        return serialized;
      });
  }
}

/**
 * Serializes a fetch response to a JSON object and calls {@code callback} passing it in.
 * @param response from fetch to serialize
 * @param callback to call with the serialized response after serialization
 */
export function serializeFetch(
  response: Response,
  callback: (response: SerializedResponse) => void
): void {
  serializeResponse(response).then((serializedResponse) =>
    callback(serializedResponse)
  );
}

export function isError(maybeError: unknown): maybeError is Error {
  // may fail if Error was thrown in a different window / frame / iframe
  if (maybeError instanceof Error) {
    return true;
  }

  // check by duck typing (using a type guard)
  function isDuckError(maybeError: unknown): maybeError is Error {
    return (
      !!maybeError &&
      !!(maybeError as Error).stack &&
      !!(maybeError as Error).message
    );
  }

  return isDuckError(maybeError);
}

export function isResponse(maybeResponse: unknown): maybeResponse is Response {
  return maybeResponse instanceof Response;
}

/**
 * Whether given object is of type `SerializedResponse`, using duck typing.
 * @param mayBeSerializedResponse the object to check
 */
export function isSerializedResponse(
  mayBeSerializedResponse: unknown
): boolean {
  return (
    !!mayBeSerializedResponse &&
    !!(mayBeSerializedResponse as SerializedResponse).body &&
    !!(mayBeSerializedResponse as SerializedResponse).status &&
    !!(mayBeSerializedResponse as SerializedResponse).headers
  );
}

export function mailto(email: string, subject?: string, body?: string): string {
  let link = `mailto:${email}`;
  if (subject || body) {
    link += '?';
  }
  if (subject) {
    link += `subject=${encodeURIComponent(subject)}`;
    if (body) {
      link += '&';
    }
  }
  if (body) {
    link += `body=${encodeURIComponent(body)}`;
  }
  return link;
}

/**
 * Returns true if {@code date} is between {@code fromMonth/fromDay} (start of day) and
 * {@code toMonth/toDay} (end of day), ignoring the year.
 * Range is based on browser's timezone.
 *
 * @param fromMonth number between 1-12, representing the starting month
 * @param fromDay number between 1-31 (30, 28 or 29, depending on the month),
 *                representing the starting day
 * @param toMonth number between 1-12, representing the ending month
 * @param toDay number between 1-31 (30, 28 or 29, depending on the month),
 *              representing the ending day
 * @param date to take as a reference, current date will be used by default
 */
export function isBetweenIgnoreYear(
  fromMonth: number,
  fromDay: number,
  toMonth: number,
  toDay: number,
  date?: string
): boolean {
  dayjs.extend(isBetween);
  const reference = dayjs(date);
  const fromDate = reference
    .clone()
    .month(fromMonth - 1)
    .date(fromDay)
    .startOf('day');
  const toDate = reference
    .clone()
    .month(toMonth - 1)
    .date(toDay)
    .endOf('day');
  return reference.isBetween(fromDate, toDate, 'millisecond', '[]');
}

export function isChristmas(currentDate?: string): boolean {
  return isBetweenIgnoreYear(12, 1, 12, 31, currentDate);
}

/**
 * Returns {@code true} if the code is running on the server (during server side rendering)
 * or {@code false}, if the code is running on the client (user's browser).
 * Make sure to call this every time before using / accessing {@link window} or {@link document},
 * as they are NOT available during server side rendering (SSR) but on the client only!
 * See https://github.com/vercel/next.js/issues/5354#issuecomment-520305040
 */
export function isServer(): boolean {
  return typeof window === 'undefined';
}

/**
 * Returns {@code true} if the code is running on the client (user's browser) or {@code false},
 * if the code is running on the server (during server side rendering).
 * Make sure to call this every time before using / accessing {@link window} or {@link document},
 * as they are NOT available during server side rendering (SSR) but on the client only!
 *
 * Convenience method for better readability for {@see isServer}
 */
export function isClient(): boolean {
  return !isServer();
}

/**
 * Returns {@code true} if any of given object's property is not empty.
 */
export function isNotEmpty(obj: Record<string, string>): boolean {
  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key) && obj[key]) {
      return true;
    }
  }
  return false;
}

/**
 * Calculates the date of easter for a given {@code year}.
 *
 * Uses the 'Laurent Longre' algorithm and works
 * from year 1583 (the start of the gregorian calendar) onwards.
 *
 * Source: https://stackoverflow.com/a/66185479/9582113
 * @param year of which to get the easter date from
 */
export function easterDate(year: number): Date {
  let m = 3;
  const G = (year % 19) + 1;
  const C = ~~(year / 100) + 1;
  const L = ~~((3 * C) / 4) - 12;
  let E = (11 * G + 20 + ~~((8 * C + 5) / 25) - 5 - L) % 30;
  let d;
  E < 0 && (E += 30);
  ((E == 25 && G > 11) || E == 24) && E++;
  (d = 44 - E) < 21 && (d += 30);
  (d += 7 - ((~~((5 * year) / 4) - L - 10 + d) % 7)) > 31 &&
    ((d -= 31), (m = 4));
  return new Date(Date.UTC(year, m - 1, d));
}

export function isEaster(currentDate?: string): boolean {
  dayjs.extend(utc);
  dayjs.extend(isBetween);

  const now = dayjs(currentDate);
  const easter = dayjs(easterDate(now.year())).utc(true);
  const fromDate = easter.subtract(2, 'weeks').startOf('day');
  const toDate = easter.add(1, 'week').endOf('day');
  return now.isBetween(fromDate, toDate, 'millisecond', '[]');
}

/**
 * Returns {@code true} when {@code maybeInteger} is a string which consists solely of an integer.
 * @param maybeInteger string to check if it consists of an integer
 */
export function isInteger(maybeInteger: string): boolean {
  return (
    isString(maybeInteger) &&
    !isNaN(Number(maybeInteger)) &&
    Number.isInteger(Number(maybeInteger))
  );
}

/**
 * Returns {@code true} if {@code value} is the string {@code "true"}
 * and {@code "false"} in all other cases.
 *
 * @param value string to check if it consists of an integer
 */
export function stringToBoolean(value: string | null | undefined): boolean {
  return value === 'true';
}

export const correlationIdHeaderName = 'x-correlation-id';

/**
 * If {@code maybeResponse} is a {@link Response} object, it will return the
 * correlationId from the header 'X-Correlation-ID' (if present).
 * Otherwise, it will generate a new correlationId and return it.
 * @param maybeResponse {@link Response} object to extract the correlationId from
 */
export function getCorrelationId(maybeResponse: Response | unknown): string {
  let correlationId: string | null = null;
  if (isResponse(maybeResponse)) {
    correlationId = maybeResponse.headers.get(correlationIdHeaderName);
  }
  return correlationId ?? generateShortUuid();
}

export function isNewYear(currentDate?: string): boolean {
  return isBetweenIgnoreYear(1, 1, 1, 6, currentDate);
}

export function isNationalDay(currentDate?: string): boolean {
  return isBetweenIgnoreYear(7, 26, 8, 8, currentDate);
}

export function isHalloween(currentDate?: string): boolean {
  return isBetweenIgnoreYear(10, 25, 11, 7, currentDate);
}
