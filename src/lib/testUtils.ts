import SagaTester from 'redux-saga-tester';
import { Reducer, ReducersMapObject } from 'redux';
import { fireEvent, screen } from '@testing-library/react';
import equal from 'fast-deep-equal/es6';
import { AnyObject, DefaultRootState } from '../types';
import { setupServer } from 'msw/node';
import { MockedRequest, ResponseTransformer, RestContext } from 'msw';
import * as timeZoneMock from 'timezone-mock';
import { Saga } from 'redux-saga';
import { $Keys } from 'utility-types';

export function matchRequestBody(
  req: MockedRequest,
  expectedBody: AnyObject
): boolean {
  return equal(req.body, expectedBody);
}

export const mockI18n = (): void => {
  jest.mock('react-i18next', () => ({
    useTranslation: () => ({ t: (key: string) => key }),
    Trans: ({ i18nKey }: Record<'i18nKey', string>) => i18nKey,
  }));
};

export function expectRequestBody(
  req: MockedRequest,
  ...expectedBody: AnyObject[]
): void {
  if (!expectedBody.some((body) => matchRequestBody(req, body))) {
    console.error(
      'MockAPI: Expected request body(s):',
      expectedBody,
      'but instead got:',
      req.body,
      'for request:',
      req.method,
      req.url.href,
      'with ID:',
      req.id
    );
    throw req;
  }
}

const jsonHeaderKey = 'content-type';
const jsonHeaderValue = 'application/json';

export function expectJsonHeader(req: MockedRequest): void {
  if (req.headers.get(jsonHeaderKey) !== jsonHeaderValue) {
    console.error(
      'MockAPI: Expected JSON request header, but instead got:',
      req.headers.all(),
      'for request:',
      req.method,
      req.url.href,
      'with ID:',
      req.id
    );
    throw req;
  }
}

export function expectQueryParameter(
  req: MockedRequest,
  key: string,
  expectedValue: string
): void {
  const queryParameterValue = req.url.searchParams.get(key);
  if (queryParameterValue !== expectedValue) {
    console.error(
      'MockAPI: Expected query parameter:',
      key,
      'to be:',
      expectedValue,
      'but instead got:',
      queryParameterValue,
      'for request:',
      req.method,
      req.url.href,
      'with ID:',
      req.id
    );
    throw req;
  }
}

export function resJson(ctx: RestContext): ResponseTransformer {
  return ctx.set(jsonHeaderKey, jsonHeaderValue);
}

export function resBody(ctx: RestContext, body: unknown): ResponseTransformer {
  return ctx.body(JSON.stringify(body));
}

export type MockServer = ReturnType<typeof setupServer>;

export function setupMockApi(
  ...handlers: Parameters<typeof setupServer>
): MockServer {
  const server = setupServer(...handlers);
  server.listen({
    onUnhandledRequest(req) {
      // ignore unhandled CORS preflight requests
      if (req.method !== 'OPTIONS') {
        const debugRequest = {
          ...req,
          url: req.url.toString(),
          headers: req.headers.all(),
        };
        console.error('MockAPI: Unhandled request:', debugRequest);
        throw debugRequest;
      }
    },
  });
  return server;
}

export class RequestVerifier {
  requests: MockedRequest[] = [];

  constructor(server: ReturnType<typeof setupServer>) {
    server.events.on('request:start', (req: MockedRequest) =>
      this.requests.push(req)
    );
  }

  public assertCount(expectedCount: number): void {
    const current = this.requests.length;
    if (current !== expectedCount) {
      this.throwError(
        `Expected ${expectedCount} request(s) but got ${current}`
      );
    }
  }

  public assertCalled(url: string, expectedCount = 1): void {
    const calls = this.requests.reduce<Record<string, number>>((acc, curr) => {
      const p = curr.url.toString();
      acc[p] = (p in acc ? acc[p] : 0) + 1;
      return acc;
    }, {});
    const current = url in calls ? calls[url] : 0;
    if (current !== expectedCount) {
      this.throwError(
        `Expected ${expectedCount} calls to ${url} but got ${current}`
      );
    }
  }

  private throwError(message: string) {
    const allRequests = this.requests.map(
      (req) =>
        `${req.method} ${req.url.toString()} Headers: ${JSON.stringify(
          req.headers.all()
        )}} Body: ${JSON.stringify(req.body)}`
    );
    throw {
      message: `Message: ${message}, All Requests: ${JSON.stringify(
        allRequests
      )}`,
      allRequests,
      requests: this.requests,
    };
  }
}

/**
 * Initializes a mocked redux store with redux-saga for async testing.
 *
 * @example Usage:
 * ```typescript
 * let store;
 *
 * beforeEach(() => {
 *     store = makeMockStore({project: initialProjectState}, {project: projectReducer});
 * });
 *
 * it('...', async () => {
 *     store.dispatch({type: LOAD_PROJECTS.request, id: userId});
 *     await store.waitFor(LOAD_PROJECTS.success);
 *
 *     const projectState = store.getState().project;
 *     expect(projectState.itemList).toHaveLength(1);
 * ```
 *
 * @param initialState
 * @param reducers
 * @param rootSaga
 */
export const makeMockStore = <State = DefaultRootState>(
  initialState: Partial<State>,
  reducers: ReducersMapObject | Reducer<Partial<State>>,
  rootSaga: Saga
): SagaTester<Partial<State>> => {
  const sagaTester = new SagaTester({
    initialState,
    reducers,
  });
  sagaTester.start(rootSaga);

  return sagaTester;
};

/**
 * Simulates clicking on the down arrow inside of a {@link AutocompleteField},
 * which opens the dropdown with the choices.
 *
 * @param label same as the one passed to {@link AutocompleteField}
 */
export const openAutocompleteDropdown = (label: string): void => {
  fireEvent.click(screen.getByLabelText('Open ' + label));
};

/**
 * Mocks the timezone to be a certain {@code timeZone} or UTC by default.
 */
export function mockTimeZone(timeZone?: timeZoneMock.TimeZone): void {
  timeZoneMock.register(timeZone ?? 'Europe/London');
}

export function mockConsoleWarn(): jest.SpyInstance<
  void,
  Parameters<typeof console.warn>
> {
  return mockConsole('warn');
}

export function mockConsoleError(): jest.SpyInstance<
  void,
  Parameters<typeof console.error>
> {
  return mockConsole('error');
}

function mockConsole(
  method: jest.FunctionPropertyNames<Required<Console>>
): jest.SpyInstance<
  ReturnType<Required<Console>[jest.FunctionPropertyNames<Required<Console>>]>,
  jest.ArgsType<
    Required<Console>[jest.FunctionPropertyNames<Required<Console>>]
  >
> {
  return jest.spyOn(console, method).mockImplementation(() => undefined);
}

/**
 * Sets the text of {@code input} to to {@code text}.
 * @param input element of the input field to set the text on
 * @param text to be set in the input field
 */
export function setInputValue(
  input: HTMLElement,
  text: string | undefined | null
): void {
  if (!text) {
    throw new Error(`Unexpected text ${text} for input ${input}`);
  }
  fireEvent.change(input, { target: { value: text } });
}

// object is necessary since the generic type of `$Keys` is `<T extends object>`
// eslint-disable-next-line @typescript-eslint/ban-types
export type TextboxField<T extends object> = {
  name: $Keys<T>;
  value: string;
};

// object is necessary since the generic type of `$Keys` is `<T extends object>`
// eslint-disable-next-line @typescript-eslint/ban-types
export function fillTextboxes<T extends object>(
  textboxes: HTMLElement[],
  fields: TextboxField<T>[]
): void {
  if (textboxes.length !== fields.length) {
    throw new Error('Textboxes and fields length must be the same!');
  }
  for (let i = 0; i < fields.length; i++) {
    expect(textboxes[i]).toHaveAttribute('name', fields[i]['name']);
    setInputValue(textboxes[i], fields[i]['value']);
  }
}

/**
 * If {@code isPresent} is {@code true}, will throw an error if {@code element}
 * is NOT present in the document.
 * If {@code isPresent} is {@code false}, will throw an error if {@code element}
 * IS present in the document.
 * @param element to check whether or not it is present in the document
 * @param isPresent if the {@code element} is expected to be present or not
 */
export function expectToBeInTheDocument(
  element: HTMLElement | null,
  isPresent: boolean
): void {
  if (isPresent) {
    expect(element).toBeInTheDocument();
  } else {
    expect(element).not.toBeInTheDocument();
  }
}

/**
 * If {@code isPresent} is {@code true}, will throw an error if any of
 * {@code elements} are NOT present in the document.
 * If {@code isPresent} is {@code false}, will throw an error if any of
 * {@code elements} ARE present in the document.
 * @param elements to check whether or not they are present in the document
 * @param isPresent if the {@code elements} are expected to be present or not
 */
export function expectAllToBeInTheDocument(
  elements: HTMLElement[] | null,
  isPresent: boolean
): void {
  elements?.forEach((element) => {
    expectToBeInTheDocument(element, isPresent);
  });
}
