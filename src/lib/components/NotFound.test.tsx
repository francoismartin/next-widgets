import { mockConsoleError } from '../testUtils';

import * as React from 'react';
import * as utils from '../utils';
import { render, screen } from '@testing-library/react';
import equal from 'fast-deep-equal/es6';

import { NotFound } from './NotFound';

describe('NotFound', function () {
  describe('Component', function () {
    it('should contain a correlationId and the "contact us" should mail to including the correlationId and current URL', async () => {
      const spy = mockConsoleError();

      const correlationId = 'RandomlyGeneratedMockedCorrelationId';
      const url = 'currentUrl';
      const mailtoText = 'mailto:mocked';
      const errorTitle = '404 Page not found';
      const contactEmail = 'chuck@norris.roundhousekick';

      jest.spyOn(utils, 'generateShortUuid').mockReturnValue(correlationId);

      const mailtoMock = jest
        .spyOn(utils, 'mailto')
        .mockReturnValue(mailtoText);

      Object.defineProperty(window, 'location', {
        value: { href: url },
      });

      render(<NotFound contactEmail={contactEmail} />);

      await screen.findByText(correlationId);

      const contactUs = (await screen.findByText(
        'contact us'
      )) as HTMLAnchorElement;
      const contactUsLink = contactUs['href'];
      expect(contactUsLink).toContain(mailtoText);
      const mailtoCalls = mailtoMock.mock.calls;
      expect(mailtoCalls).toHaveLength(1);
      expect(mailtoCalls[0]).toEqual([
        contactEmail,
        errorTitle,
        `Correlation ID: ${correlationId}\r\n\r\nURL: ${url}`,
      ]);

      expect(spy).toHaveBeenCalledTimes(1);
      expect(
        equal(spy.mock.calls[0][0], {
          namespace: 'NotFound',
          level: 'error',
          stack: 'frontend',
          correlationId: correlationId,
          json: { href: url },
          message: errorTitle,
        })
      ).toBe(true);

      spy.mockRestore();
    });
  });
});
