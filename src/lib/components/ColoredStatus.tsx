import React, { ReactElement } from 'react';
import Chip, { ChipPropsColorOverrides } from '@mui/material/Chip';
import { OverridableStringUnion } from '@mui/types';

export interface Status<T> {
  color?: OverridableStringUnion<
    | 'default'
    | 'primary'
    | 'secondary'
    | 'error'
    | 'info'
    | 'success'
    | 'warning',
    ChipPropsColorOverrides
  >;
  value: T;
  text?: string;
}

export type ColoredStatusProps<T> = {
  readonly value: T | undefined;
  readonly statuses: Status<T>[];
};

export function ColoredStatus<T>({
  value,
  statuses,
}: ColoredStatusProps<T>): ReactElement | null {
  if (value === undefined) {
    return null;
  }
  const status = statuses.find((item) => item.value === value);
  if (!status) {
    return null;
  }

  return (
    <Chip
      label={String(status.text ?? status.value).toUpperCase()}
      color={status.color}
    />
  );
}
