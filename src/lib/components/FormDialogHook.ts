import { useState } from 'react';

export interface UseFormDialogHook {
  <T>(selector?: T): {
    openFormDialog: (data?: T) => void;
    data: T | undefined;
    closeFormDialog: () => void;
  };
}

// Return type is defined in `UseFormDialogHook`
function useFormDialogFn<F>(initialValue: F) {
  const [data, setData] = useState<F>();
  return {
    openFormDialog:
      initialValue !== undefined
        ? (data = initialValue) => setData(data)
        : setData,
    data,
    closeFormDialog: () => setData(undefined),
  };
}

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore // TODO
export const useFormDialog: UseFormDialogHook = useFormDialogFn;
