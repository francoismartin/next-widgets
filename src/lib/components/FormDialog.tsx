import React, { ReactElement } from 'react';
import { Dialog, DialogProps } from '../widgets';
import { Form, FormProps } from './forms';
import { FieldValues } from 'react-hook-form';

export type FormDialogProps<T extends FieldValues> = Omit<
  FormProps<T>,
  'title'
> &
  Omit<DialogProps, 'onSubmit'>;

export function FormDialog<T extends FieldValues>({
  onSubmit,
  children,
  noValidate,
  ...other
}: FormDialogProps<T>): ReactElement {
  return (
    <Dialog
      confirmButtonProps={{
        // make sure submit button is always present (as `onSubmit` is required)
        autoFocus: false,
      }}
      {...other}
    >
      <Form onSubmit={onSubmit} id="dialog-form" noValidate={noValidate}>
        {children}
      </Form>
    </Dialog>
  );
}
