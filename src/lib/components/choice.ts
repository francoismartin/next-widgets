import { $Values } from 'utility-types';
import { MenuItemProps } from '@mui/material/MenuItem';
import { useMemo } from 'react';
import get from 'lodash/get';
import isString from 'lodash/isString';
import { AnyObject } from '../../types';

export const useChoices = <T>(
  objects: T[],
  key: string,
  label: string | ((obj: T) => string)
): Choice[] =>
  useMemo(() => {
    return objects.map((object) => {
      return {
        key: get(object, key) as string,
        value: get(object, key) as string,
        label: (isString(label) ? get(object, label) : label(object)) as string,
      };
    });
  }, [objects, key, label]);

export const useEnumChoices = (enumObject: AnyObject): Choice[] =>
  useMemo(() => {
    return Object.entries(enumObject).map(
      ([key, val]) =>
        ({
          key: key,
          value: val,
          label: val,
        } as Choice)
    );
  }, [enumObject]);

export type Choice = Omit<MenuItemProps, 'value'> & {
  readonly label: string;
  readonly value: string | number;
};

export function isChoice(choice?: Choice): choice is Choice {
  return !!choice;
}

export type ChoiceValue = $Values<Pick<Choice, 'value'>>;

export const choicesByValue = (choices: Choice[]): Record<string, Choice> =>
  choices.reduce<Record<string, Choice>>((acc, choice) => {
    acc[choice.value] = choice;
    return acc;
  }, {});
