import React, { ReactElement, useCallback } from 'react';

import { styled } from '@mui/material/styles';

import { Toolbar, Typography } from '@mui/material';
import { GlobalFilter, GlobalFilterProps } from './GlobalFilter';
import AddIcon from '@mui/icons-material/Add';
import { addIconButtonLabelPrefix, Button } from '../../widgets/Buttons';

const LeftContainer = styled('span')(() => ({
  flex: '1 1 100%',
}));

export type TableToolbarProps = {
  globalFilterProps?: GlobalFilterProps;
  title?: string;
  canAdd?: boolean;
  onAdd?: () => void;
  addButtonLabel?: string;
  inline?: boolean;
};

export const tableAddButtonAriaLabelPrefix = addIconButtonLabelPrefix;

export function TableToolbar({
  globalFilterProps,
  title,
  canAdd,
  onAdd,
  addButtonLabel,
  inline,
}: TableToolbarProps): ReactElement {
  const showAddButton = inline && canAdd;
  const addOnClick = useCallback((): void => onAdd && onAdd(), [onAdd]);

  return (
    <Toolbar
      sx={{
        '&.MuiToolbar-root': {
          paddingLeft: 2,
          paddingRight: 1,
          minHeight: 'inherit',
        },
      }}
    >
      {(!!title || !!showAddButton) && (
        <LeftContainer>
          {title && (
            <Typography
              variant="subtitle1"
              sx={{
                display: 'inline',
                paddingRight: 2,
              }}
            >
              {title}
            </Typography>
          )}
          {showAddButton && (
            <Button
              variant="outlined"
              color="primary"
              startIcon={<AddIcon />}
              onClick={addOnClick}
              aria-label={tableAddButtonAriaLabelPrefix + addButtonLabel}
            >
              {addButtonLabel}
            </Button>
          )}
        </LeftContainer>
      )}
      {globalFilterProps && <GlobalFilter {...globalFilterProps} />}
    </Toolbar>
  );
}
