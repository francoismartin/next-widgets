import { IdType, Row } from 'react-table';

// object is necessary since the generic type of `SortByFn` is `<T extends object>`
// eslint-disable-next-line @typescript-eslint/ban-types
export const caseInsensitive = <D extends object>(
  rowA: Row<D>,
  rowB: Row<D>,
  columnId: IdType<D>
): number => {
  function getLowerCaseValue(row: Row<D>): string {
    let value = row.values[columnId];
    if (Array.isArray(value)) {
      value = value.join();
    }
    value = value.toLowerCase();
    return value;
  }

  return Intl.Collator().compare(
    getLowerCaseValue(rowA),
    getLowerCaseValue(rowB)
  );
};
