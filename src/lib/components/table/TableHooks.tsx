import React, { ReactElement, useCallback } from 'react';
import {
  ButtonBox,
  DeleteIconButton,
  deleteIconButtonLabelPrefix,
  EditIconButton,
  editIconButtonLabelPrefix,
} from '../../widgets';
import { AdditionalActionButtonFactory, IdType } from '../../../types';
import { CellProps } from 'react-table';
import { isFunction } from 'lodash';

export type TableActionButtonsProps<T extends IdType> = {
  addButtonLabel?: string;
  canEdit?: boolean;
  canDelete?: boolean;
  onEdit?: () => void;
  onDelete?: () => void;
  additionalActionButtons?: Array<AdditionalActionButtonFactory<T>>;
};

function TableActionButtons<T extends IdType>({
  addButtonLabel,
  canEdit,
  canDelete,
  onEdit,
  onDelete,
  additionalActionButtons,
}: TableActionButtonsProps<T>): ReactElement {
  const tableRowButtonLabelGeneric = 'row';

  return (
    <ButtonBox sx={{ display: 'flex', flexWrap: 'nowrap' }}>
      {canEdit && (
        <EditIconButton
          aria-label={
            editIconButtonLabelPrefix +
            (addButtonLabel ?? tableRowButtonLabelGeneric)
          }
          onClick={onEdit}
        />
      )}
      {canDelete && (
        <DeleteIconButton
          aria-label={
            deleteIconButtonLabelPrefix +
            (addButtonLabel ?? tableRowButtonLabelGeneric)
          }
          onClick={onDelete}
        />
      )}
      {additionalActionButtons}
    </ButtonBox>
  );
}

type useTableActionsCellProps<T extends IdType> = {
  addButtonLabel?: string;
  onEdit?: (item: T) => void;
  handleDeleteOpen: (
    id?: number | undefined,
    name?: string | undefined
  ) => void;
  itemList: T[];
  getDeleteConfirmationText?: (item: T) => string;
  additionalActionButtons?: Array<AdditionalActionButtonFactory<T>>;
};

export function useTableActionsCell<T extends IdType>({
  addButtonLabel,
  onEdit,
  handleDeleteOpen,
  getDeleteConfirmationText,
  itemList,
  additionalActionButtons,
}: useTableActionsCellProps<T>): {
  hasAdditionalActionButtons: boolean;
  // Matches generic signature of `CellProps`
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  TableActionsCell: <D extends T, V = any>(
    cellProps: CellProps<D, V>
  ) => ReactElement;
} {
  const hasAdditionalActionButtons = itemList.some((item) =>
    additionalActionButtons?.some((actionButtonFn) => actionButtonFn(item))
  );

  const TableActionsCell = useCallback(
    // Matches generic signature of `CellProps`
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    <D extends T, V = any>({
      row,
      canEdit,
      canDelete,
      additionalActionButtons,
    }: CellProps<D, V>) => (
      <TableActionButtons
        addButtonLabel={addButtonLabel}
        canEdit={isFunction(canEdit) ? canEdit(row.original) : canEdit}
        canDelete={isFunction(canDelete) ? canDelete(row.original) : canDelete}
        onEdit={() => onEdit && onEdit(row.original)}
        onDelete={() =>
          handleDeleteOpen(
            row.original.id,
            getDeleteConfirmationText && getDeleteConfirmationText(row.original)
          )
        }
        additionalActionButtons={additionalActionButtons?.map(
          (actionButtonFn: AdditionalActionButtonFactory<T>) =>
            actionButtonFn(row.original)
        )}
      />
    ),
    [addButtonLabel, getDeleteConfirmationText, handleDeleteOpen, onEdit]
  );

  return { hasAdditionalActionButtons, TableActionsCell };
}
