import { Unpacked } from '../../../types';
import React, { useMemo } from 'react';
import { EnhancedTable } from './EnhancedTable';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { Column } from 'react-table';
import { FabButton } from '../../widgets';

const itemList = [
  {
    id: 0,
    name: 'Item 1',
    description: 'This is item 1',
  },
  {
    id: 1,
    name: 'Item 2',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris pretium, elit in viverra porttitor, ' +
      'est mi faucibus diam, vitae lobortis sapien lorem a odio. Praesent suscipit commodo libero, id interdum ' +
      'sapien commodo ac. Vestibulum a turpis non enim fermentum fringilla. Morbi ex eros, pulvinar vel nulla id, ' +
      'ultrices feugiat odio. Sed ut lorem nisl. Donec eleifend vehicula fringilla. Phasellus fermentum a velit ' +
      'tincidunt vehicula. Nulla vehicula finibus imperdiet. Nunc sollicitudin rutrum risus. Duis condimentum ' +
      'nec turpis gravida volutpat. Suspendisse rutrum lacus nisi, et tincidunt dolor consequat vitae.',
  },
];

type ListItem = Unpacked<typeof itemList>;

const isFirstItem = (item?: ListItem) => item?.name === itemList[0].name;

export default {
  title: 'Components/Table/EnhancedTable',
  component: EnhancedTable,
} as ComponentMeta<typeof EnhancedTable>;

const Template: ComponentStory<typeof EnhancedTable<ListItem>> = (args) => {
  const columns = useMemo<Array<Column<ListItem>>>(
    () => [
      {
        id: 'name',
        Header: 'Name',
        accessor: 'name',
      },
      {
        id: 'description',
        Header: 'Description',
        accessor: 'description',
        widthPercentage: 80,
      },
    ],
    []
  );

  return (
    <EnhancedTable<ListItem>
      {...args}
      columns={columns}
      itemList={itemList}
      isFetching={false}
      isSubmitting={false}
      addButtonLabel={'Custom Item'}
      inline
    />
  );
};

export const Default = Template.bind({});
Default.args = {};

export const WithTitleInline = Template.bind({});
WithTitleInline.args = { title: 'Custom Item', inline: true };

export const CanAdd = Template.bind({});
CanAdd.args = { canAdd: true };

export const CanEdit = Template.bind({});
CanEdit.args = { canEdit: true };

export const CanEditFunction = Template.bind({});
CanEditFunction.args = { canEdit: isFirstItem };

export const CanDelete = Template.bind({});
CanDelete.args = { canDelete: true };

export const CanDeleteFunction = Template.bind({});
CanDeleteFunction.args = { canDelete: isFirstItem };

export const WithDeleteConfirmationText = Template.bind({});
WithDeleteConfirmationText.args = {
  ...CanDelete.args,
  getDeleteConfirmationText: (item: ListItem) =>
    'Type this to delete ' + item.name,
};

export const CanEditAndDelete = Template.bind({});
CanEditAndDelete.args = { ...CanEdit.args, ...CanDelete.args };

export const WithAdditionalActionButtons = Template.bind({});
WithAdditionalActionButtons.args = {
  additionalActionButtons: [
    (item?: ListItem) => (
      <FabButton
        key={item?.id}
        icon="search"
        size="small"
        // eslint-disable-next-line no-console
        onClick={() => console.log(item)}
      />
    ),
  ],
};

export const CanEditAndDeleteWithAdditionalActionButtons = Template.bind({});
CanEditAndDeleteWithAdditionalActionButtons.args = {
  ...WithAdditionalActionButtons.args,
  ...CanEditAndDelete.args,
};
