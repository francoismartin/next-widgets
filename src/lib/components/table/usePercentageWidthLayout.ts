import {
  CellPropGetter,
  ColumnInstance,
  HeaderPropGetter,
  Hooks,
} from 'react-table';
import { AnyObject } from '../../../types';

export interface ColumnOptions extends ColumnInstance {
  widthPercentage?: number;
}

export const cellStyle = (widthPercentage?: number) => {
  return {
    style: {
      width: widthPercentage ? `${widthPercentage}%` : undefined,
    },
  };
};

const headerProps: HeaderPropGetter<AnyObject> = (props, { column }) => {
  return [props, cellStyle((column as ColumnOptions).widthPercentage)];
};

const cellProps: CellPropGetter<AnyObject> = (props, { cell }) => {
  return [props, cellStyle((cell.column as ColumnOptions).widthPercentage)];
};

export const usePercentageWidthLayout: ((hooks: Hooks<AnyObject>) => void) &
  Record<'pluginName', string> = (hooks) => {
  hooks.getHeaderProps.push(headerProps);
  hooks.getCellProps.push(cellProps);
};

usePercentageWidthLayout.pluginName = 'usePercentageWidthLayout';
