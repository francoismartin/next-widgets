import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
} from 'react';
import MaterialUiTable from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import {
  Column,
  FilterValue,
  PluginHook,
  useGlobalFilter,
  UseGlobalFiltersOptions,
  usePagination,
  useSortBy,
  useTable,
} from 'react-table';
import { TableToolbar } from './TableToolbar';
import { TablePaginationActions } from './TablePaginationActions';
import { useTableActionsCell } from './TableHooks';
import {
  ListBase,
  useDeleteDialog,
  UseListProps,
  useLoadItems,
  UseLoadItemsProps,
} from '../list';
import {
  AdditionalActionButtonFactory,
  AnyObject,
  IdType,
} from '../../../types';
import clsx from 'clsx';
import { GlobalClass } from '../../widgets';
import { ValuesType } from 'utility-types';
import { caseInsensitive } from './sortTypes';
import isFunction from 'lodash/isFunction';
import { usePercentageWidthLayout } from './usePercentageWidthLayout';
import { Typography } from '@mui/material';

export type GlobalFilterFunction<T extends AnyObject> = Exclude<
  ValuesType<Pick<UseGlobalFiltersOptions<T>, 'globalFilter'>>,
  undefined | string
>;

export type EnhancedTableProps<T extends IdType> = UseLoadItemsProps<T> & {
  columns: Column<T>[];
  title?: string;
  canAdd?: boolean;
  canEdit?: boolean | ((item?: T) => boolean);
  canDelete?: boolean | ((item?: T) => boolean);
  onRowClick?: (item: T) => void;
  onAdd?: () => void;
  onEdit?: (item: T) => void;
  onDelete?: (itemId: number) => void;
  addButtonLabel?: string;
  isFetching: boolean;
  isSubmitting: boolean;
  inline?: boolean;
  emptyMessage?: string;
  form?: React.ReactNode;
  canFilter?: boolean;
  globalFilter?: GlobalFilterFunction<T>;
  pagination?: boolean;
  additionalActionButtons?: Array<AdditionalActionButtonFactory<T>>;
} & Pick<UseListProps<T>, 'getDeleteConfirmationText'>;

export const actionsColumnHeaderLabel = 'Actions';

export function EnhancedTable<T extends IdType>({
  columns,
  itemList,
  title,
  onRowClick,
  canAdd,
  canEdit,
  canDelete,
  onAdd,
  onEdit,
  onDelete,
  addButtonLabel,
  loadItems,
  isFetching,
  isSubmitting,
  inline,
  emptyMessage,
  form,
  canFilter = true,
  globalFilter,
  pagination = true,
  getDeleteConfirmationText,
  additionalActionButtons,
}: EnhancedTableProps<T>): ReactElement {
  const {
    handleDeleteOpen,
    handleDeleteClose,
    deleteOpen,
    deleteConfirmationText,
  } = useDeleteDialog({
    deleteItem: onDelete,
  });
  const { hasAdditionalActionButtons, TableActionsCell } = useTableActionsCell({
    addButtonLabel,
    onEdit,
    handleDeleteOpen,
    getDeleteConfirmationText,
    itemList,
    additionalActionButtons,
  });
  const canEditAtLeastOne = isFunction(canEdit)
    ? itemList.some((row) => canEdit(row))
    : canEdit;
  const canDeleteAtLeastOne = isFunction(canDelete)
    ? itemList.some((row) => canDelete(row))
    : canDelete;
  const showActions = !!(
    canEditAtLeastOne ||
    canDeleteAtLeastOne ||
    hasAdditionalActionButtons
  );
  const actionsColumnId = 'actions';
  const actionsColumnVisible = useRef<boolean>(showActions);

  useLoadItems({ itemList, loadItems });

  const getHiddenColumns = useCallback(
    // if actions column should be shown, do NOT HIDE any columns
    () => (showActions ? [] : [actionsColumnId]),
    [showActions]
  );

  const initialState = useMemo(
    () => ({
      hiddenColumns: getHiddenColumns(),
    }),
    // initialState can only be set on the first render, changes don't apply
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const {
    getTableProps,
    headerGroups,
    prepareRow,
    page,
    gotoPage,
    setPageSize,
    setGlobalFilter,
    state: { pageIndex, pageSize, globalFilter: stateGlobalFilter },
    allColumns,
    setHiddenColumns,
  } = useTable<T>(
    {
      columns,
      data: itemList,
      autoResetSortBy: false,
      autoResetPage: false,
      globalFilter: globalFilter,
      sortTypes: {
        caseInsensitive,
        datetime: (rowA, rowB, columnId) => {
          const zero = new Date(0);
          const valueA = (rowA.values[columnId] || zero).getTime();
          const valueB = (rowB.values[columnId] || zero).getTime();
          return valueA - valueB;
        },
      },
      initialState,
    },
    useGlobalFilter,
    useSortBy,
    usePagination,
    usePercentageWidthLayout as PluginHook<T>,
    (hooks) =>
      hooks.allColumns.push((columns) => [
        ...columns,
        {
          id: actionsColumnId,
          Header: actionsColumnHeaderLabel,
          Cell: TableActionsCell,
        },
      ])
  );

  useEffect(() => {
    // prevent setting hidden columns too often, as calling `setHiddenColumns` results in a dispatch
    if (actionsColumnVisible.current !== showActions) {
      actionsColumnVisible.current = showActions;
      setHiddenColumns(getHiddenColumns());
    }
  }, [showActions, getHiddenColumns, setHiddenColumns]);

  const handleChangePage = useCallback(
    (_event, newPage) => gotoPage(newPage),
    [gotoPage]
  );

  const handleChangeRowsPerPage = useCallback(
    (event) => setPageSize(Number(event.target.value)),
    [setPageSize]
  );

  const handleSetGlobalFilter = useCallback(
    (filterValue: FilterValue) => {
      /**
       * Prevents no results being visible in case of the current page being greater than
       * the amount of results after filtering.
       *
       * For example: current page is `41-50`, but there are only 3 results after filtering.
       * The results after filtering are only visible on the first page, which means no results
       * are visible unless the user navigates to the first page manually.
       */
      if (pageIndex !== 0) {
        gotoPage(0);
      }
      setGlobalFilter(filterValue);
    },
    [pageIndex, gotoPage, setGlobalFilter]
  );

  const unsortableColumnId = actionsColumnId;
  const isEmpty = !itemList || !itemList.length;

  const globalFilterProps =
    canFilter && !isEmpty
      ? {
          setGlobalFilter: handleSetGlobalFilter,
          globalFilter: stateGlobalFilter,
        }
      : undefined;

  return (
    <ListBase
      deleteOpen={deleteOpen}
      handleDeleteClose={handleDeleteClose}
      isFetching={isFetching}
      itemList={itemList}
      canAdd={canAdd}
      deleteConfirmationText={deleteConfirmationText}
      inline={inline}
      form={form}
      isSubmitting={isSubmitting}
    >
      <TableContainer>
        <TableToolbar
          globalFilterProps={globalFilterProps}
          title={title}
          onAdd={onAdd}
          canAdd={canAdd}
          addButtonLabel={addButtonLabel}
          inline={inline}
        />
        {!isEmpty && (
          <MaterialUiTable {...getTableProps()}>
            <TableHead>
              {headerGroups.map((headerGroup) => (
                // key is included in `getHeaderGroupProps`
                // eslint-disable-next-line react/jsx-key
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    // key is included in `getHeaderProps`
                    // eslint-disable-next-line react/jsx-key
                    <TableCell
                      sx={{ fontWeight: 'bold' }}
                      {...(column.id === unsortableColumnId
                        ? column.getHeaderProps()
                        : column.getHeaderProps(
                            column.getSortByToggleProps({
                              title: `Sort By ${column.Header}`,
                            })
                          ))}
                    >
                      {column.render('Header')}
                      {column.id !== unsortableColumnId ? (
                        <TableSortLabel
                          active={column.isSorted}
                          // react-table has a unsorted state which is not treated here
                          direction={column.isSortedDesc ? 'desc' : 'asc'}
                        />
                      ) : null}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody>
              {page.map((row) => {
                prepareRow(row);
                return (
                  // key is included in `getRowProps`
                  // eslint-disable-next-line react/jsx-key
                  <TableRow {...row.getRowProps()}>
                    {row.cells.map((cell) => {
                      const isColumnSelectable =
                        cell.column.id !== unsortableColumnId;
                      const cellProps = cell.getCellProps();
                      const dataProps = { 'data-testid': cellProps.key };
                      return (
                        // key is included in `getCellProps`
                        // eslint-disable-next-line react/jsx-key
                        <TableCell
                          {...cellProps}
                          onClick={(): void => {
                            if (isColumnSelectable && !!onRowClick) {
                              onRowClick(row.original);
                            }
                          }}
                          className={clsx(row.getRowProps().className, {
                            [GlobalClass.selectable]:
                              isColumnSelectable && !!onRowClick,
                          })}
                          {...dataProps}
                        >
                          {cell.render(
                            'Cell',
                            cell.column.id === actionsColumnId
                              ? { canEdit, canDelete, additionalActionButtons }
                              : undefined
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
            <TableFooter>
              <TableRow>
                {pagination && (
                  <TablePagination
                    sx={{ borderBottom: 'none' }}
                    rowsPerPageOptions={[
                      5,
                      10,
                      25,
                      { label: 'All', value: itemList.length },
                    ]}
                    colSpan={allColumns.length}
                    count={itemList.length}
                    rowsPerPage={pageSize}
                    page={pageIndex}
                    SelectProps={{
                      inputProps: { 'aria-label': 'rows per page' },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                )}
                {!pagination && (
                  <TableCell sx={{ border: 'none' }}>&nbsp;</TableCell>
                )}
              </TableRow>
            </TableFooter>
          </MaterialUiTable>
        )}
        {isEmpty && !!emptyMessage && (
          <Typography
            variant="body2"
            component="pre"
            sx={{
              paddingLeft: 2,
              paddingTop: 2,
            }}
          >
            {emptyMessage}
          </Typography>
        )}
      </TableContainer>
    </ListBase>
  );
}
