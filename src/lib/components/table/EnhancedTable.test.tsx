import * as React from 'react';
import { useMemo } from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import { Unpacked } from '../../../types';
import { renderHook } from '@testing-library/react-hooks';
import { Column } from 'react-table';
import { actionsColumnHeaderLabel, EnhancedTable } from './EnhancedTable';
import {
  addIconButtonLabelPrefix,
  deleteIconButtonLabelPrefix,
  editIconButtonLabelPrefix,
} from '../../widgets';
import { expectToBeInTheDocument } from '../../testUtils';

const itemList = [
  {
    id: 0,
    name: 'Item 1',
  },
];

type ListItem = Unpacked<typeof itemList>;

const label = 'Custom Item';
const addLabel = addIconButtonLabelPrefix + label;
const editLabel = editIconButtonLabelPrefix + label;
const deleteLabel = deleteIconButtonLabelPrefix + label;

describe('EnhancedTable', function () {
  describe('Component', function () {
    it.each`
      firstCanAdd | firstCanEdit | firstCanDelete | secondCanAdd | secondCanEdit | secondCanDelete
      ${false}    | ${false}     | ${false}       | ${false}     | ${true}       | ${false}
      ${false}    | ${false}     | ${false}       | ${false}     | ${false}      | ${true}
      ${false}    | ${false}     | ${false}       | ${false}     | ${true}       | ${true}
      ${false}    | ${true}      | ${false}       | ${false}     | ${true}       | ${true}
      ${false}    | ${false}     | ${true}        | ${false}     | ${true}       | ${true}
      ${false}    | ${true}      | ${false}       | ${false}     | ${false}      | ${false}
      ${false}    | ${false}     | ${true}        | ${false}     | ${false}      | ${false}
      ${false}    | ${true}      | ${true}        | ${false}     | ${false}      | ${false}
      ${false}    | ${true}      | ${true}        | ${false}     | ${true}       | ${false}
      ${false}    | ${true}      | ${true}        | ${false}     | ${false}      | ${true}
      ${false}    | ${false}     | ${false}       | ${true}      | ${false}      | ${false}
      ${true}     | ${false}     | ${false}       | ${false}     | ${false}      | ${false}
    `(
      'should allow adding ($firstCanAdd), editing ($firstCanEdit) and allow deletion ($firstCanDelete) after the first render; and allow adding ($secondCanAdd), editing ($secondCanEdit) and allow deletion ($secondCanDelete) after the second render',
      async ({
        firstCanAdd,
        firstCanEdit,
        firstCanDelete,
        secondCanAdd,
        secondCanEdit,
        secondCanDelete,
      }) => {
        const { result } = renderHook(() =>
          useMemo<Array<Column<ListItem>>>(
            () => [
              {
                id: 'name',
                Header: 'Name',
                accessor: 'name',
              },
            ],
            []
          )
        );
        const columns = result.current;
        const { rerender } = render(
          <EnhancedTable<ListItem>
            columns={columns}
            itemList={itemList}
            canAdd={firstCanAdd}
            canEdit={firstCanEdit}
            canDelete={firstCanDelete}
            isFetching={false}
            isSubmitting={false}
            addButtonLabel={label}
            inline
          />
        );

        await screen.findByText(itemList[0].name);

        expectToBeInTheDocument(
          screen.queryByText(actionsColumnHeaderLabel),
          firstCanEdit || firstCanDelete
        );

        expectToBeInTheDocument(
          screen.queryByRole('button', {
            name: addLabel,
          }),
          firstCanAdd
        );
        expectToBeInTheDocument(
          screen.queryByRole('button', {
            name: editLabel,
          }),
          firstCanEdit
        );
        expectToBeInTheDocument(
          screen.queryByRole('button', {
            name: deleteLabel,
          }),
          firstCanDelete
        );

        rerender(
          <EnhancedTable<ListItem>
            columns={columns}
            itemList={itemList}
            canAdd={secondCanAdd}
            canEdit={secondCanEdit}
            canDelete={secondCanDelete}
            isFetching={false}
            isSubmitting={false}
            addButtonLabel={label}
            inline
          />
        );

        expectToBeInTheDocument(
          screen.queryByText(actionsColumnHeaderLabel),
          secondCanEdit || secondCanDelete
        );

        await waitFor(() => {
          expectToBeInTheDocument(
            screen.queryByRole('button', {
              name: addLabel,
            }),
            secondCanAdd
          );
          expectToBeInTheDocument(
            screen.queryByRole('button', {
              name: editLabel,
            }),
            secondCanEdit
          );
          expectToBeInTheDocument(
            screen.queryByRole('button', {
              name: deleteLabel,
            }),
            secondCanDelete
          );
        });
      }
    );
  });
});
