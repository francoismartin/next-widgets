import { ButtonBox, DeleteIconButton, EditIconButton } from '../../widgets';
import React, { FunctionComponent, ReactElement, useState } from 'react';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { AdditionalActionButtonFactory, IdType } from '../../../types';
import isFunction from 'lodash/isFunction';
import { ListBaseProps } from './ListBase';

export type ListModel<T> = {
  getCaption?: (item: T, index: number) => ReactElement | string;
  fields: ListModelField<T>[];
};

export type ListModelField<T> = {
  caption?: string;
  key: string;
  render: FunctionComponent<T>;
  hideIf?: (data: T) => boolean;
  tooltip?: string;
};

export type UseListProps<T extends IdType> = UseDeleteDialogProps &
  UseLoadItemsProps<T> & {
    openForm?: (item?: T) => void; // is undefined when "add" is called, since no particular item is affected
    canEdit?: boolean | ((item?: T) => boolean);
    canDelete?: boolean;
    model: ListModel<T>;
    getDeleteConfirmationText?: (item: T) => string;
    additionalActionButtons?: Array<AdditionalActionButtonFactory<T>>;
  } & Pick<ListBaseProps<unknown>, 'itemName'>;

export type UseListHook = Pick<
  UseDeleteDialogHook,
  'handleDeleteClose' | 'deleteOpen' | 'deleteConfirmationText'
> & {
  items: FormattedItem[];
  hasActionButtons: boolean;
};

export type FormattedItem = {
  id: number;
  caption: ReactElement | string; // for example: Test Project 1
  fields: FormattedItemField[];
  actionButtons: React.ReactNode | null; // is null when user is not privileged to perform any modifying actions or when the openform is not defined
};

export type FormattedItemField = {
  key?: string;
  caption?: string; // for example: Project ID
  component: React.ReactNode;
};

export type UseDeleteDialogProps = {
  deleteItem?: (itemId: number) => void;
};

export type UseDeleteDialogHook = {
  handleDeleteClose: (isConfirmed: boolean) => void;
  deleteOpen: boolean;
  handleDeleteOpen: (id?: number, name?: string) => void;
  deleteConfirmationText?: string;
};

export function useDeleteDialog({
  deleteItem,
}: UseDeleteDialogProps): UseDeleteDialogHook {
  const [deleteItemId, setDeleteItemId] = useState<number | undefined>(
    undefined
  );

  const [deleteConfirmationText, setDeleteConfirmationText] = useState<
    string | undefined
  >(undefined);

  const handleDeleteOpen = (id?: number, confirmationText?: string): void => {
    setDeleteItemId(id);
    setDeleteConfirmationText(confirmationText);
  };

  const handleDeleteClose = (isConfirmed: boolean): void => {
    if (isConfirmed && deleteItemId !== undefined) {
      deleteItem && deleteItem(deleteItemId);
    }
    setDeleteItemId(undefined);
    setDeleteConfirmationText(undefined);
  };

  return {
    handleDeleteClose,
    deleteOpen: deleteItemId !== undefined,
    handleDeleteOpen,
    deleteConfirmationText,
  };
}

export type UseLoadItemsProps<T> = {
  itemList: T[];
  loadItems?: () => void; // gets called when itemList is empty
};

export function useLoadItems<T>({
  itemList,
  loadItems,
}: UseLoadItemsProps<T>): void {
  useDeepCompareEffect(() => {
    if ((!itemList || !itemList.length) && loadItems) {
      loadItems();
    }
  }, [itemList, loadItems]);
}

export function formatItemFields<T>(
  model: ListModel<T>,
  item: T
): FormattedItemField[] {
  return model.fields
    .filter((field: ListModelField<T>) => {
      if (field.hideIf) {
        return !field.hideIf(item);
      }
      // if hideIf is not defined, always show field
      return true;
    })
    .map((field: ListModelField<T>) => {
      const { render, ...rest } = field;
      return {
        ...rest,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore TODO
        component: React.createElement(render, { ...item }),
      };
    });
}

export function useList<T extends IdType>({
  openForm,
  canEdit,
  canDelete,
  deleteItem,
  itemList,
  itemName,
  loadItems,
  model,
  getDeleteConfirmationText,
  additionalActionButtons,
}: UseListProps<T>): UseListHook {
  useLoadItems({ itemList, loadItems });

  const {
    handleDeleteOpen,
    handleDeleteClose,
    deleteOpen,
    deleteConfirmationText,
  } = useDeleteDialog({
    deleteItem,
  });

  let hasActionButtons = false;

  function canEditItem(item?: T): boolean {
    if (isFunction(canEdit)) {
      return canEdit(item);
    }
    return !!canEdit;
  }

  function actionButtons(item?: T): ReactElement | null {
    const showEditButton = canEditItem(item);

    const buttons: Array<React.ReactNode | React.ReactNodeArray> = [];

    const canModify = showEditButton || canDelete;
    if (openForm && canModify) {
      const open = () => openForm(item);
      if (showEditButton) {
        buttons.push(
          <EditIconButton
            key={`edit-${itemName}`}
            onClick={open}
            itemName={itemName}
          />
        );
      }
      if (canDelete) {
        buttons.push(
          <DeleteIconButton
            key={`delete-${itemName}`}
            onClick={(): void =>
              handleDeleteOpen(
                item?.id,
                getDeleteConfirmationText && !!item
                  ? getDeleteConfirmationText(item)
                  : undefined
              )
            }
            itemName={itemName}
          />
        );
      }
    }

    if (additionalActionButtons) {
      additionalActionButtons.forEach((actionButtonFn) => {
        const button = actionButtonFn(item);
        if (button) {
          buttons.push(actionButtonFn(item));
        }
      });
    }

    if (buttons.length) {
      hasActionButtons = true;
      return <ButtonBox>{buttons}</ButtonBox>;
    } else {
      return null;
    }
  }

  const items: FormattedItem[] = itemList.map((item: T, index: number) => ({
    id: item.id,
    caption: (model.getCaption && model.getCaption(item, index)) ?? '',
    fields: formatItemFields(model, item),
    actionButtons: actionButtons(item),
  }));

  return {
    handleDeleteClose,
    deleteOpen,
    items,
    hasActionButtons,
    deleteConfirmationText,
  };
}
