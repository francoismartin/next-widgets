import React, { ReactElement } from 'react';

import MaterialUiList from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

export type ListProps = {
  items: string[];
  icon?: React.ReactNode;
};

export function List(props: ListProps): ReactElement {
  const { items, icon } = props;
  return (
    <MaterialUiList>
      {items.map((item) => (
        <ListItem key={item}>
          {icon && <ListItemIcon>{icon}</ListItemIcon>}
          <ListItemText primary={item} />
        </ListItem>
      ))}
    </MaterialUiList>
  );
}
