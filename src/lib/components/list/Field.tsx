import React, { PropsWithChildren, ReactElement, ReactNode } from 'react';
import { ListModelField } from './ListHooks';
import { formatDate } from '../../utils';
import { IdType } from '../../../types';

type ComponentFieldProps<ModelData, RenderProps> = {
  key: string;
  caption?: string;
  hideIf?: (modelData: ModelData) => boolean;
  tooltip?: string;
  getProperty: (modelData: ModelData) => RenderProps;
  render?: React.FunctionComponent<RenderProps>;
};

export function Field<ModelData, RenderProps>(
  props: ComponentFieldProps<ModelData, RenderProps>
): ListModelField<ModelData> {
  const { render, getProperty, ...rest } = props;
  return {
    render: (modelData: ModelData) =>
      (render ?? renderLabel)(
        getProperty(modelData) as PropsWithChildren<RenderProps>
      ),
    ...rest,
  };
}

export function renderLabel(data: ReactNode): ReactElement {
  let text = data;
  if (data instanceof Date) {
    text = formatDate(data) ?? '';
  }
  return <>{text}</>;
}

export function renderFieldArray<RenderProps extends IdType>(
  key: string,
  renderItem: (item: RenderProps) => React.ReactNode
): (data: RenderProps[]) => ReactElement {
  function _FieldArray(data: RenderProps[]): ReactElement {
    return (
      <ul
        style={{
          listStyleType: 'none',
          padding: 0,
          marginBlockStart: 0,
          marginBlockEnd: 0,
        }}
      >
        {data.map((item) => (
          <li key={key + '-' + item.id}>{renderItem(item)}</li>
        ))}
      </ul>
    );
  }

  return _FieldArray;
}
