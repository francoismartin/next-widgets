import React, { ReactElement, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import Link from 'next/link';

import ListItemIcon, { ListItemIconClassKey } from '@mui/material/ListItemIcon';
import ListItemText, { ListItemTextClassKey } from '@mui/material/ListItemText';
import ListItem from '@mui/material/ListItem';
import { Action } from 'redux';
import { ClassNameMap } from '@mui/material';

type ListItemBaseProps = {
  icon?: React.ReactNode;
  primary?: React.ReactNode;
  secondary?: React.ReactNode;
  textClasses?: Partial<ClassNameMap<ListItemTextClassKey>>;
  iconClasses?: Partial<ClassNameMap<ListItemIconClassKey>>;
  selected?: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  listItemProps?: any; // FIXME 2020-07-12, François Martin
};

const ListItemBase = React.forwardRef<HTMLButtonElement, ListItemBaseProps>(
  (
    {
      icon,
      primary,
      secondary,
      textClasses,
      iconClasses,
      listItemProps,
      ...other
    },
    ref
  ): ReactElement => (
    <ListItem ref={ref} button {...listItemProps} {...other}>
      {icon && (
        <ListItemIcon
          sx={{ color: 'rgb(117, 117, 117)' }}
          classes={iconClasses}
        >
          {icon}
        </ListItemIcon>
      )}
      <ListItemText
        primary={primary}
        secondary={secondary}
        sx={{
          '& .MuiListItemText-primary': {
            fontSize: 18,
            textDecoration: 'none',
            textDecorationStyle: 'unset',
          },
        }}
        classes={textClasses}
      />
    </ListItem>
  )
);
ListItemBase.displayName = 'ListItemBase';

export type ListItemLinkProps = ListItemBaseProps & {
  href: string;
};

export const ListItemLink = ({
  href,
  ...listItemBaseProps
}: ListItemLinkProps): ReactElement => {
  listItemBaseProps.listItemProps = {
    ...listItemBaseProps.listItemProps,
    component: 'a',
  };
  return (
    <Link href={href} passHref>
      <ListItemBase {...listItemBaseProps} />
    </Link>
  );
};

export type ListItemActionProps = ListItemBaseProps & {
  action: Action;
};

export const ListItemAction = ({
  action,
  ...listItemBaseProps
}: ListItemActionProps): ReactElement => {
  const dispatch = useDispatch();
  const onClick = useCallback(() => dispatch(action), [action, dispatch]);

  listItemBaseProps.listItemProps = {
    ...listItemBaseProps.listItemProps,
    onClick,
  };

  return <ListItemBase {...listItemBaseProps} />;
};

type ListItemButtonProps = ListItemBaseProps & {
  value: string;
  submit?: boolean; // whether or not the button should be a submit button
};

export const ListItemButton = ({
  value,
  submit,
  ...listItemBaseProps
}: ListItemButtonProps): ReactElement => {
  listItemBaseProps.listItemProps = {
    ...listItemBaseProps.listItemProps,
    value,
    component: 'button',
  };

  if (submit) {
    listItemBaseProps.listItemProps.type = 'submit';
  }

  return <ListItemBase {...listItemBaseProps} />;
};
