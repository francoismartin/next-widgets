import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { DeleteDialog } from './DeleteDialog';

export default {
  title: 'Components/List/DeleteDialog',
  component: DeleteDialog,
} as ComponentMeta<typeof DeleteDialog>;

const Template: ComponentStory<typeof DeleteDialog> = (args) => (
  <DeleteDialog
    {...args}
    onDelete={(isConfirmed) =>
      // eslint-disable-next-line no-console
      console.log('onDelete, isConfirmed:', isConfirmed)
    }
    isSubmitting={false}
  />
);

export const Open = Template.bind({});
Open.args = { open: true };

export const Closed = Template.bind({});
Closed.args = { open: false };

export const WithItemName = Template.bind({});
WithItemName.args = { ...Open.args, itemName: 'repository' };

export const WithConfirmationText = Template.bind({});
WithConfirmationText.args = { ...Open.args, confirmationText: 'next-widgets' };

export const WithItemNameAndDeleteItem = Template.bind({});
WithItemNameAndDeleteItem.args = {
  ...WithItemName.args,
  ...WithConfirmationText.args,
};

export const WithCustomTitleAndText = Template.bind({});
WithCustomTitleAndText.args = {
  ...Open.args,
  title: 'Custom Title',
  text: 'Custom Text',
};
