import React, { ReactElement, useCallback, useMemo, useState } from 'react';
import { Optional } from 'utility-types';
import { Dialog, DialogProps, itemNameGeneric } from '../../widgets';
import TextField from '@mui/material/TextField';
import { Box, DialogContentText } from '@mui/material';

export type DeleteDialogProps = {
  itemName?: string;
  confirmationText?: string;
  onDelete: (isConfirmed: boolean) => void;
  confirmationHelper?: React.ReactNode;
} & Optional<DialogProps, 'title'>;

/**
 * Dialog to delete an item, requiring the user to enter the {@code deleteItem}
 * into a text field in order to be able to delete the item.
 * @param title of the dialog
 * @param text to appear after {@code title}
 * @param confirmationHelper text which appears above the text field where the
 *                         user needs to enter {@code confirmationText} to be
 *                         able to delete the item
 * @param itemName type of item to be deleted (for example: "Project", "User")
 * @param confirmationText text that the user needs to enter to be able to
 *                         delete the item
 * @param onDelete method to be called if the user either dismisses the dialog
 *                 (in which case "isConfirmed" will be "false") or confirms the
 *                 deletion (in which case "isConfirmed" will be "true").
 * @param other all other props passed in
 * @constructor
 */
export const DeleteDialog = ({
  title,
  text,
  confirmationText,
  itemName,
  confirmationHelper,
  onDelete,
  ...other
}: DeleteDialogProps): ReactElement => {
  const [confirmed, setConfirmed] = useState<boolean>(false);

  itemName = itemName ?? itemNameGeneric;

  const onChange = useCallback<React.ChangeEventHandler<HTMLInputElement>>(
    (event) => {
      setConfirmed(event.target.value === confirmationText);
    },
    [confirmationText]
  );

  const disabled = useMemo(() => {
    if (confirmationText) {
      return !confirmed;
    }
    return false;
  }, [confirmationText, confirmed]);

  return (
    <Dialog
      {...other}
      title={title ?? 'Confirm deletion'}
      text={
        text ??
        `You are about to permanently delete this ${itemName}. Once it is permanently deleted, it cannot be recovered. This action cannot be undone.`
      }
      confirmButtonProps={{
        autoFocus: !confirmationText,
        onClick: () => onDelete(true),
        disabled,
      }}
      onClose={() => onDelete(false)}
    >
      {confirmationText && (
        <>
          <DialogContentText sx={{ marginBottom: 0.5, marginTop: 1.5 }}>
            {confirmationHelper ?? `Please type the following to confirm:`}
          </DialogContentText>
          <Box
            fontFamily="Monospace"
            fontSize="body1.fontSize"
            sx={{ marginBottom: 1.5 }}
          >
            {confirmationText}
          </Box>
          <TextField
            autoFocus={true}
            size="small"
            onChange={onChange}
            variant="outlined"
            fullWidth
          />
        </>
      )}
    </Dialog>
  );
};
