import React, { ReactElement } from 'react';

import { styled } from '@mui/material/styles';

import { Accordion } from '../../widgets';
import { Description } from '../Description';
import { FormattedItem, useList, UseListProps } from './ListHooks';
import { ListBase, ListBaseProps } from './ListBase';
import { IdType } from '../../../types';
import { toArray } from '../../utils';

const StyledTable = styled('table')(() => ({
  width: '100%',
  borderCollapse: 'collapse',
}));

const StyledFooter = styled('tfoot')(({ theme }) => ({
  borderTop: `solid 1px ${theme.palette.divider}`,
}));

const StyledTd = styled('td')(() => ({
  padding: 0,
}));

const StyledActionButtonsTd = styled('td')(({ theme }) => ({
  paddingTop: theme.spacing(2),
}));

export type ListPageProps<T extends IdType> = UseListProps<T> &
  ListBaseProps<T> & { defaultExpanded?: number | number[] };

export function ListPage<T extends IdType>({
  openForm,
  form,
  canEdit = false,
  canDelete = false,
  getDeleteConfirmationText,
  deleteItem,
  itemList,
  itemName,
  loadItems,
  model,
  canAdd = false,
  emptyMessage,
  inline,
  isFetching,
  isSubmitting,
  defaultExpanded,
  additionalActionButtons,
}: ListPageProps<T>): ReactElement {
  const { handleDeleteClose, items, deleteOpen, deleteConfirmationText } =
    useList<T>({
      openForm,
      canEdit,
      canDelete,
      deleteItem,
      itemList,
      itemName,
      loadItems,
      model,
      getDeleteConfirmationText,
      additionalActionButtons,
    });

  return (
    <ListBase
      canAdd={canAdd}
      handleDeleteClose={handleDeleteClose}
      itemList={itemList}
      itemName={itemName}
      deleteOpen={deleteOpen}
      emptyMessage={emptyMessage}
      inline={inline}
      deleteConfirmationText={deleteConfirmationText}
      openForm={openForm}
      form={form}
      isFetching={isFetching}
      isSubmitting={isSubmitting}
    >
      {items.map(
        ({ id, caption, fields, actionButtons }: FormattedItem, index) => {
          const hasFooter = !!actionButtons;

          return (
            <Accordion
              defaultExpanded={toArray(defaultExpanded).includes(id)}
              caption={caption}
              key={id}
              firstChild={index == 0}
            >
              <StyledTable>
                <tbody>
                  <tr>
                    <StyledTd>
                      <Description entries={fields} labelWidth="12%" />
                    </StyledTd>
                  </tr>
                </tbody>
                {hasFooter && (
                  <StyledFooter>
                    <tr>
                      <StyledActionButtonsTd>
                        {actionButtons}
                      </StyledActionButtonsTd>
                    </tr>
                  </StyledFooter>
                )}
              </StyledTable>
            </Accordion>
          );
        }
      )}
    </ListBase>
  );
}
