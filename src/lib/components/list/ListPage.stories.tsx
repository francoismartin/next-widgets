import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { ListPage } from './ListPage';
import { Field } from './Field';
import { ListModel } from './ListHooks';
import { Unpacked } from '../../../types';
import { FabButton } from '../../widgets';

export default {
  title: 'Components/List/ListPage',
  component: ListPage,
} as ComponentMeta<typeof ListPage>;

const itemList = [
  {
    id: 0,
    name: 'Item 1',
    code: 1,
  },
  {
    id: 1,
    name: 'Item 2',
    code: 2,
  },
];

type ListItem = Unpacked<typeof itemList>;

const model: ListModel<ListItem> = {
  getCaption: (item) => item.name,
  fields: [
    Field({
      caption: 'Code',
      getProperty: (item) => item.code,
      key: 'code',
    }),
  ],
};

const Template: ComponentStory<typeof ListPage> = (args) => (
  <ListPage<ListItem>
    {...args}
    model={model}
    itemList={itemList}
    defaultExpanded={1}
    // eslint-disable-next-line no-console
    openForm={() => console.log('Open Form')}
  />
);

export const Default = Template.bind({});
Default.args = {};

export const WithEditButtons = Template.bind({});
WithEditButtons.args = {
  canEdit: true,
};

export const WithDeleteButtons = Template.bind({});
WithDeleteButtons.args = {
  canDelete: true,
};

export const WithEditAndDeleteButtons = Template.bind({});
WithEditAndDeleteButtons.args = {
  ...WithEditButtons.args,
  ...WithDeleteButtons.args,
};

export const WithAdditionalButtons = Template.bind({});
WithAdditionalButtons.args = {
  additionalActionButtons: [
    (item) => (
      <FabButton
        key={item?.id}
        size="small"
        icon="add"
        // eslint-disable-next-line no-console
        onClick={() => console.log('Add:', item)}
      />
    ),
  ],
};

export const WithEditAndAdditionalButtons = Template.bind({});
WithEditAndAdditionalButtons.args = {
  ...WithEditButtons.args,
  ...WithAdditionalButtons.args,
};

export const WithAdditionalEmptyButtons = Template.bind({});
WithAdditionalEmptyButtons.args = {
  additionalActionButtons: [() => null],
};

export const WithDeleteConfirmationText = Template.bind({});
WithDeleteConfirmationText.args = {
  ...WithDeleteButtons.args,
  getDeleteConfirmationText: (item) =>
    'Type this to delete ' + (item as ListItem).name,
};
