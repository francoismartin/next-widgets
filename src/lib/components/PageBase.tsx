import React, { FunctionComponent, ReactElement, useMemo } from 'react';
import Paper from '@mui/material/Paper';
import { StyleMap } from '../../types';
import { grey } from '@mui/material/colors';
import { Divider } from '@mui/material';

const globalStyles: StyleMap = {
  navigation: {
    fontSize: 15,
    color: grey[600],
    display: 'block',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  paper: {
    padding: '30px 30px 75px 30px',
    boxShadow: 'none',
  },
  clear: {
    clear: 'both',
  },
  content: {
    marginTop: 20,
  },
  titleContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
};

type PageBaseProps<P extends Record<string, unknown>> = {
  title: string | string[];
  content: FunctionComponent<P>;
  toolbar?: FunctionComponent;
  props?: P;
};

export function PageBase<P extends Record<string, unknown>>({
  title,
  content: Content,
  toolbar: Toolbar,
  props = {} as P,
}: PageBaseProps<P>): ReactElement {
  const breadcrumb = Array.isArray(title) ? title.join(' / ') : title;
  const pageTitle = useMemo(() => {
    if (Array.isArray(title)) {
      return title.length ? title[title.length - 1] : '';
    }
    return title;
  }, [title]);
  return (
    <div>
      <span style={globalStyles.navigation}>{breadcrumb}</span>
      <Paper style={globalStyles.paper}>
        <span style={globalStyles.titleContainer}>
          <h3 style={globalStyles.title}>{pageTitle}</h3>
          {!!Toolbar && <Toolbar />}
        </span>
        <Divider />
        <div id="content" style={globalStyles.content}>
          <Content {...props} />
        </div>
        <div style={globalStyles.clear} />
      </Paper>
    </div>
  );
}
