import { ColoredStatus, Status } from './ColoredStatus';
import React from 'react';
import { Meta, Story } from '@storybook/react';

enum Color {
  GREEN = 'green',
  GREY = 'grey',
  RED = 'red',
}

const statuses: Status<Color>[] = [
  {
    color: 'success',
    value: Color.GREEN,
  },
  {
    value: Color.GREY,
  },
  {
    color: 'error',
    value: Color.RED,
  },
];

export default {
  title: 'Components/ColoredStatus',
  component: ColoredStatus,
} as Meta<typeof ColoredStatus>;

const Template: Story<typeof ColoredStatus> = (args) => {
  return <ColoredStatus value={Color.GREY} {...args} statuses={statuses} />;
};

export const Green = Template.bind({});
Green.args = { value: Color.GREEN };

export const Default = Template.bind({});

export const Red = Template.bind({});
Red.args = { value: Color.RED };
