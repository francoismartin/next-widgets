import React, { ErrorInfo, ReactNode } from 'react';
import { Environment, redirectExternal } from '../api';
import { generateShortUuid, isClient } from '../utils';
import { logger } from '../logger';
import { serializeError } from 'serialize-error';
import { InternalErrorContent, InternalErrorContentProps } from '../widgets';

type Props = Omit<InternalErrorContentProps, 'correlationId'>;

type State = {
  hasError: boolean;
  correlationId?: string;
};

export class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(): State {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, correlationId: generateShortUuid() };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    const errorObject = {
      error: serializeError(error),
      errorInfo,
    };

    const log = logger('Uncaught exception');
    log.error({
      json: errorObject,
      correlationId: this.state.correlationId,
      message: 'Exception was thrown and not caught or handled.',
    });
  }

  render(): ReactNode {
    if (isClient()) {
      const location = window.location;
      const homeUrl = location.protocol + '//' + location.host;
      if (
        this.state.hasError &&
        process.env.NODE_ENV === Environment.PRODUCTION // make sure we still get create-react-app's error boundary during development
      ) {
        return (
          <div role="alert">
            <p>
              <InternalErrorContent
                correlationId={this.state.correlationId as string}
                subjectPrefix={this.props.subjectPrefix}
                contactEmail={this.props.contactEmail}
              />
            </p>
            <button onClick={() => redirectExternal(homeUrl)}>
              Back to the home page
            </button>
          </div>
        );
      }
    }
    return this.props.children;
  }
}
