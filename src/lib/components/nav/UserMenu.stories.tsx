import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { UserMenu } from './UserMenu';
import { WarningIcon } from '../../widgets';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { toast } from '../../reducers';
import { ListItemAction } from '../list';

export default {
  title: 'Components/Nav/UserMenu',
  component: UserMenu,
} as ComponentMeta<typeof UserMenu>;

const Template: ComponentStory<typeof UserMenu> = () => {
  return (
    <Provider store={createStore(toast)}>
      <UserMenu>
        <ListItemAction
          action={{ type: 'NOOP' }}
          primary={'Test'}
          icon={<WarningIcon />}
        />
      </UserMenu>
    </Provider>
  );
};

export const Default = Template.bind({});
Default.args = {};
