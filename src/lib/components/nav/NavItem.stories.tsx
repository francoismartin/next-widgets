import React from 'react';
import { Meta, Story } from '@storybook/react';
import { Nav } from './Nav';
import { HomeIcon, MessageIcon } from '../../widgets';
import { NavItem } from './NavItem';
import { Theme } from '@mui/material';

const homeLink = '/';

const menuItems = [
  {
    title: 'Home',
    link: homeLink,
    icon: <HomeIcon />,
  },
  {
    title: 'Contact',
    link: '/contact',
    icon: <MessageIcon />,
  },
];

export default {
  title: 'Components/Nav/NavItem',
  component: NavItem,
} as Meta<typeof NavItem>;

const Template: Story<typeof NavItem> = (args) => {
  return (
    <Nav>
      {menuItems.map((menuItem) => (
        <NavItem
          {...args}
          selected={menuItem.link == homeLink}
          href={menuItem.link ?? '#'}
          key={'MenuItem-' + menuItem.title}
          primary={menuItem.title}
          icon={menuItem.icon}
        />
      ))}
    </Nav>
  );
};

export const Default = Template.bind({});
Default.args = {};

export const WithCustomColors = Template.bind({});
WithCustomColors.args = {
  textColor: (theme: Theme) => theme.palette.secondary.light,
  backgroundColor: (theme: Theme) => theme.palette.secondary.main,
};
