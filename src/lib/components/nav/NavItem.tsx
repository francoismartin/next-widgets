import { ListItemLink, ListItemLinkProps } from '../list';
import { styled, Theme } from '@mui/material/styles';
import React, { ReactElement, useMemo } from 'react';

const PREFIX = 'NavItem';

const classes = {
  menuItem: `${PREFIX}-menuItem`,
  menuIconStyles: `${PREFIX}-menuIconStyles`,
};

const StyledListItemLink = styled(ListItemLink, {
  shouldForwardProp: (prop) =>
    !['textColor', 'backgroundColor'].includes(String(prop)),
})<NavItemProps>(({ theme, textColor, backgroundColor }) => {
  const navItemTextColor = useMemo(
    () => (textColor ? textColor(theme) : theme.palette.primary.contrastText),
    [textColor, theme]
  );
  const navItemBackgroundColor = useMemo(
    () =>
      backgroundColor ? backgroundColor(theme) : theme.palette.primary.light,
    [backgroundColor, theme]
  );
  return {
    [`& .${classes.menuItem}`]: {
      color: navItemTextColor,
      fontSize: 18,
      textDecoration: 'none',
      textDecorationStyle: 'unset',
    },
    [`& .${classes.menuIconStyles}`]: {
      color: navItemTextColor,
    },
    '&.Mui-selected': {
      backgroundColor: navItemBackgroundColor,
    },
    '&.Mui-selected:hover': {
      backgroundColor: navItemBackgroundColor,
    },
  };
});

export type NavItemProps = {
  textColor?: (theme: Theme) => string;
  backgroundColor?: (theme: Theme) => string;
} & ListItemLinkProps;

export const NavItem = ({
  href,
  ...navItemProps
}: NavItemProps): ReactElement => (
  <StyledListItemLink
    {...navItemProps}
    href={href}
    textClasses={{ primary: classes.menuItem }}
    iconClasses={{ root: classes.menuIconStyles }}
  />
);
