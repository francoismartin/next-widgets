import React, { ReactElement } from 'react';
import { Drawer } from '@mui/material';

const drawerWidth = 230;

type NavProps = {
  children: React.ReactNode;
  backgroundColor?: string;
};

export const Nav = React.forwardRef<
  HTMLDivElement,
  Omit<NavProps, 'variant' | 'sx'>
>(({ children, backgroundColor, ...navProps }, ref): ReactElement => {
  return (
    <Drawer
      variant="permanent"
      ref={ref}
      sx={{
        width: drawerWidth,
        flexShrink: 0,
        '& .MuiPaper-root': {
          color: 'white',
          backgroundColor: backgroundColor ?? 'primary.main',
          width: drawerWidth,
          borderRightWidth: 0,
        },
      }}
      {...navProps}
    >
      {children}
    </Drawer>
  );
});
Nav.displayName = 'Nav';
