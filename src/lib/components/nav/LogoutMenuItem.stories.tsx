import { LogoutMenuItem } from './LogoutMenuItem';
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { UserMenu } from './UserMenu';

export default {
  title: 'Components/Nav/LogoutMenuItem',
  component: LogoutMenuItem,
} as ComponentMeta<typeof LogoutMenuItem>;

const Template: ComponentStory<typeof LogoutMenuItem> = () => {
  return (
    <UserMenu>
      <LogoutMenuItem logoutUrl={'javascript:void(0)'} />
    </UserMenu>
  );
};

export const Default = Template.bind({});
Default.args = {};
