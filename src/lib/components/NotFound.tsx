import React, { ReactElement } from 'react';
import Link from 'next/link';
import { logger } from '../logger';
import { generateShortUuid, mailto } from '../utils';
import { PageBase } from './PageBase';
import { Typography } from '@mui/material';

const log = logger('NotFound');

type NotFoundContentProps = {
  errorId: string;
  url: string;
  contactEmail: string;
};

const NotFoundContent = ({
  errorId,
  url,
  contactEmail,
}: NotFoundContentProps): ReactElement => {
  const body = `Correlation ID: ${errorId}\r\n\r\nURL: ${url}`;
  return (
    <>
      <Typography variant="body2">
        We are sorry, we cannot find the page you were looking for.
      </Typography>
      <br />
      <Typography variant="body2">
        This page might have moved or possibly no longer exists.
      </Typography>
      <br />
      <Typography variant="body2">
        You can <Link href="/">return to our home page</Link> or{' '}
        <a href={mailto(contactEmail ?? '', '404 Page not found', body)}>
          contact us
        </a>{' '}
        if you cannot find what you are looking for and refer to the following
        ID:{' '}
        <Typography variant={'subtitle2'} component="span">
          {errorId}
        </Typography>
      </Typography>
    </>
  );
};

type NotFoundProps = {
  contactEmail: string;
};

export const NotFound = ({ contactEmail }: NotFoundProps): ReactElement => {
  const errorId = generateShortUuid();
  const location = window.location;
  log.error({
    json: location,
    correlationId: errorId,
    message: '404 Page not found',
  });

  return (
    <PageBase<NotFoundContentProps>
      title={'Page not found'}
      content={NotFoundContent}
      props={{ errorId, url: location?.href, contactEmail }}
    />
  );
};
