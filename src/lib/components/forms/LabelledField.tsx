import 'setimmediate';
import React, { ReactElement, useRef } from 'react';
import {
  EnhancedArrayFieldProps,
  EnhancedFieldProps,
  useEnhancedArrayField,
  useEnhancedField,
  UseEnhancedFieldHookReturn,
} from './hooks';
import { FieldError, RegisterOptions, useFormContext } from 'react-hook-form';
import debouncePromise from 'awesome-debounce-promise';
import { StatusCodes } from 'http-status-codes';
import { uniqueValidationDebounce } from '../../config';
import { AnyObject } from '../../../types';
import * as merge from 'deepmerge';
import { isResponse, serializeResponse, toArray } from '../../utils';
import { HiddenArrayField, HiddenField } from './HiddenField';
import { TextField, TextFieldProps } from '@mui/material';

export interface LabelledFieldBaseProps
  extends UseEnhancedFieldHookReturn,
    Omit<TextFieldProps, 'error'> {
  name: string;
  validations?: RegisterOptions | RegisterOptions[];
  /**
   * A function which is being called when checking for uniqueness.
   *
   * Should throw a `Response` object with `response.status === StatusCodes.CONFLICT`
   * in case of the value NOT being unique.
   */
  unique?: (value: AnyObject) => Promise<void>;
  autofocus?: boolean;
}

const LabelledFieldBase = ({
  name,
  error,
  defaultValue,
  validations,
  unique,
  autofocus,
  disabled,
  ...textFieldProps
}: LabelledFieldBaseProps): ReactElement => {
  const { register } = useFormContext();
  // prevent re-renders on value changes
  const lastUniqueValue = useRef<string | null>(null);
  const validationArray = toArray(validations);

  if (unique) {
    validationArray.push({
      validate: {
        unique: debouncePromise(
          async (value) => {
            /**
             * Initial value is always valid.
             *
             * Without this check, if an existing resource is edited,
             * where the value is changed to something else and
             * then back to the initial value, it would falsely show the value as NOT being unique,
             * as it would claim it to be already existing.
             */
            const isInitialValue = defaultValue === value;
            // prevent calling backend multiple times if the value is still the same
            const isAlreadyValidated = lastUniqueValue.current === value;
            if (isInitialValue || isAlreadyValidated) {
              return true;
            }
            try {
              await unique({ [name]: value });
              lastUniqueValue.current = value;
              return true;
            } catch (e) {
              if (isResponse(e)) {
                if (e.status === StatusCodes.CONFLICT) {
                  return 'Must be unique.';
                } else {
                  // unhandled response
                  throw new Error(JSON.stringify(await serializeResponse(e)));
                }
              } else {
                throw e;
              }
            }
          },
          uniqueValidationDebounce ? Number(uniqueValidationDebounce) : 750,
          // instantly validate after the first change, then debounce every call after that
          { leading: true }
        ),
      },
    });
  }
  const { ref, ...rest } = register(name, merge.all(validationArray));

  return (
    <TextField
      {...textFieldProps}
      {...rest}
      inputRef={disabled ? undefined : ref}
      error={!!error}
      helperText={error?.message}
      defaultValue={defaultValue}
      autoFocus={autofocus}
      size="small"
      disabled={disabled}
    />
  );
};

export type LabelledFieldProps<TInitialValues extends Record<string, unknown>> =
  EnhancedFieldProps<TInitialValues> &
    Pick<LabelledFieldBaseProps, 'validations' | 'unique' | 'autofocus'> &
    Omit<TextFieldProps, 'error'> & {
      fieldError?: FieldError;
    };

export function LabelledField<TInitialValues extends Record<string, unknown>>({
  name,
  initialValues,
  validations,
  unique,
  autofocus,
  fieldError,
  disabled,
  ...textFieldProps
}: LabelledFieldProps<TInitialValues>): ReactElement {
  const enhancedFieldProps = {
    name,
    initialValues,
  };
  const { error, defaultValue } =
    useEnhancedField<TInitialValues>(enhancedFieldProps);

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise its value becomes 'undefined'
        <HiddenField {...enhancedFieldProps} />
      )}
      <LabelledFieldBase
        {...textFieldProps}
        name={name}
        disabled={disabled}
        defaultValue={defaultValue ? String(defaultValue) : undefined}
        error={fieldError ?? error}
        validations={validations}
        unique={unique}
        autofocus={autofocus}
      />
    </>
  );
}

export type LabelledArrayFieldProps = EnhancedArrayFieldProps &
  Pick<LabelledFieldBaseProps, 'validations' | 'unique' | 'autofocus'> &
  TextFieldProps;
export const LabelledArrayField = ({
  arrayName,
  fieldName,
  field,
  index,
  validations,
  unique,
  autofocus,
  disabled,
  ...textFieldProps
}: LabelledArrayFieldProps): ReactElement => {
  const enhancedFieldProps = {
    arrayName,
    fieldName,
    field,
    index,
  };
  const { error, name, defaultValue } =
    useEnhancedArrayField(enhancedFieldProps);

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise its value becomes 'undefined'
        <HiddenArrayField {...enhancedFieldProps} />
      )}
      <LabelledFieldBase
        {...textFieldProps}
        disabled={disabled}
        name={name}
        defaultValue={defaultValue}
        error={error}
        unique={unique}
        validations={validations}
        autofocus={autofocus}
      />
    </>
  );
};
