import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import * as React from 'react';
import { SelectField, SelectFieldProps } from './SelectField';
import { AnyObject } from '../../../types';
import { useEnhancedForm } from './hooks';
import { FormProvider } from 'react-hook-form';
import { mockConsoleWarn } from '../../testUtils';

const testSelectName = 'projectId';
const testSelectLabel = 'Project ID';

const choices = [
  {
    key: 'choiceKey0',
    value: 'choiceValue0',
    label: 'choiceLabel0',
  },
  {
    key: 'choiceKey1',
    value: 'choiceValue1',
    label: 'choiceLabel1',
  },
];

function TestSelectField(
  labelledSelectProps: Partial<SelectFieldProps<AnyObject>>
) {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <SelectField
        name={testSelectName}
        label={testSelectLabel}
        choices={choices}
        {...labelledSelectProps}
      />
    </FormProvider>
  );
}

describe('SelectField', function () {
  it('should render a SelectField control with no initial value', async () => {
    render(<TestSelectField />);
    await screen.findAllByText(testSelectLabel);
  });

  it('should set the value to a SelectField control', async () => {
    render(<TestSelectField />);
    // 'div' with role 'button'. You first have to 'mouseDown' it
    // to make the 'listbox' visible
    const selectFieldButton = await screen.findByRole('button');

    // Search for the options
    fireEvent.mouseDown(selectFieldButton);
    const options = await screen.findAllByRole('option');
    expect(options.length).toEqual(2);
    fireEvent.click(options[0]);
    // When making a selection, corresponding 'input' will be updated
    await screen.findByDisplayValue('choiceValue0');
    expect(screen.queryByDisplayValue('choiceValue1')).not.toBeInTheDocument();

    // Using 'listbox'
    fireEvent.mouseDown(selectFieldButton);
    const listbox = await screen.findByRole('listbox');
    fireEvent.click(await within(listbox).findByText('choiceLabel1'));
    await screen.findByDisplayValue('choiceValue1');
    expect(screen.queryByDisplayValue('choiceValue0')).not.toBeInTheDocument();
  });

  it('should render a multiple SelectField control with no initial value', async () => {
    render(<TestSelectField multiple />);
    await screen.findAllByText(testSelectLabel);
  });

  it('should be accessible via test ID', async () => {
    const testID = 'testID';
    render(<TestSelectField testId={testID} />);
    // Returns the 'input' which could then be directly updated
    const listboxInput = await screen.findByTestId(testID);
    expect(listboxInput).toHaveValue('');
    fireEvent.change(listboxInput, { target: { value: 'choiceValue0' } });
    await screen.findByDisplayValue('choiceValue0');
    expect(screen.queryByDisplayValue('choiceValue1')).not.toBeInTheDocument();
  });

  it('should render a SelectField control with an empty label if the selected value cannot be found in choices', async () => {
    const consoleSpy = mockConsoleWarn();
    const nonExistingValue = 'nonExistingValue';
    render(
      <TestSelectField initialValues={{ [testSelectName]: nonExistingValue }} />
    );
    await screen.findAllByText(testSelectLabel);
    await waitFor(() => {
      expect(screen.queryByText(nonExistingValue)).not.toBeInTheDocument();
    });
    const warnings = consoleSpy.mock.calls;
    expect(warnings).toHaveLength(2);
    const expectedWarning = `MUI: You have provided an out-of-range value \`${nonExistingValue}\` for the select component.`;
    expect(warnings[0][0]).toContain(expectedWarning);
    expect(warnings[1][0]).toContain(expectedWarning);
  });
});
