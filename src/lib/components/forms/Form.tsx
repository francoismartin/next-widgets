import React, { ReactElement, ReactNode } from 'react';
import { FieldValues, SubmitHandler, useFormContext } from 'react-hook-form';

export type FormProps<T extends FieldValues> = {
  onSubmit: SubmitHandler<T>;
  children: ReactNode;
} & Omit<
  React.DetailedHTMLProps<
    React.FormHTMLAttributes<HTMLFormElement>,
    HTMLFormElement
  >,
  'onSubmit'
>;

export function Form<T extends FieldValues>({
  onSubmit,
  children,
  ...formProps
}: FormProps<T>): ReactElement {
  const { handleSubmit } = useFormContext<T>();
  return (
    <form onSubmit={handleSubmit(onSubmit)} {...formProps}>
      {children}
    </form>
  );
}
