import { fireEvent, render, screen } from '@testing-library/react';
import * as React from 'react';
import { AutocompleteField, AutocompleteFieldProps } from './AutocompleteField';
import { useChoices } from '../choice';
import { useEnhancedForm } from './hooks';
import { FormProvider } from 'react-hook-form';

const testSelectLabel = 'Favorite Color';

const colors = [
  { name: 'Red', code: 'red' },
  { name: 'Green', code: 'green' },
  { name: 'Blue', code: 'blue' },
];

function TestAutocompleteField(
  autocompleteFieldProps: Partial<AutocompleteFieldProps>
) {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <AutocompleteField
        name="favoriteColor"
        label={testSelectLabel}
        choices={useChoices(colors, 'code', 'name')}
        {...autocompleteFieldProps}
      />
    </FormProvider>
  );
}

const selectOption = async (index: number) => {
  const textbox = await screen.findByRole('combobox');
  // textbox.click() does NOT make the options appear
  fireEvent.mouseDown(textbox);

  const options = await screen.findAllByRole('option');
  fireEvent.click(options[index]);
};

describe('AutocompleteField', function () {
  describe('without default value', function () {
    beforeEach(() => {
      render(<TestAutocompleteField />);
    });

    it('should render a AutocompleteField with no initial value', async () => {
      await screen.findAllByText(testSelectLabel);
    });

    it('should select a value', async () => {
      await selectOption(0);

      // Verify: first options maps to first color
      await screen.findByDisplayValue(colors[0].name);
      expect(
        screen.queryByDisplayValue(colors[1].name)
      ).not.toBeInTheDocument();
    });
  });

  describe('with default value', function () {
    beforeEach(() => {
      const green = colors[1];
      render(
        <TestAutocompleteField initialValues={{ favoriteColor: green.code }} />
      );
    });

    it('should render a AutocompleteField with default value pre-selected', async () => {
      const green = colors[1];
      await screen.findByDisplayValue(green.name);
    });

    it('should clear the selected value', async () => {
      const green = colors[1];
      await screen.findByDisplayValue(green.name);

      const clearButton = await screen.findByTitle('Clear Favorite Color');
      fireEvent.click(clearButton);
      // Selection has been cleared
      expect(screen.queryByDisplayValue(green.name)).not.toBeInTheDocument();
    });
  });
});
