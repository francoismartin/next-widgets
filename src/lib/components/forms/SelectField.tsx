import {
  EnhancedArrayFieldProps,
  EnhancedFieldProps,
  useEnhancedArrayField,
  useEnhancedField,
  UseEnhancedFieldHookReturn,
} from './hooks';
import { styled } from '@mui/material/styles';
import { Choice, choicesByValue } from '../choice';
import React, { ReactElement, useCallback, useMemo } from 'react';
import { joinIfArray } from '../../utils';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import { Control, Controller, FieldValues } from 'react-hook-form';
import Select, { SelectProps } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { required } from './validators';
import FormHelperText from '@mui/material/FormHelperText';
import Checkbox from '@mui/material/Checkbox';
import { CircularProgress, ListItemText } from '@mui/material';

type MultipleSelectItemProps = {
  readonly value: string[];
  readonly choice: Choice;
};

const MultipleSelectItem = ({ value, choice }: MultipleSelectItemProps) => {
  return (
    <>
      <Checkbox checked={!!value.find((it) => it === choice.value)} />
      <ListItemText primary={choice.label} />
    </>
  );
};

export type SelectFieldBaseProps = EnhancedFieldProps &
  UseEnhancedFieldHookReturn &
  Pick<SelectProps, 'inputProps'> & {
    choices: Choice[];
    className?: string;
    control?: Control;
    disabled?: boolean;
    fullWidth?: boolean;
    isLoading?: boolean;
    label: string;
    multiple?: boolean;
    required?: boolean;
    testId?: string;
  };

const StyledFormControl = styled(FormControl)({
  minWidth: 250,
  paddingBottom: 20,
});

export function SelectFieldBase(props: SelectFieldBaseProps): ReactElement {
  const {
    name,
    label,
    error,
    control,
    defaultValue,
    choices,
    disabled,
    isLoading,
    className,
    multiple,
    testId,
    fullWidth,
    ...selectProps
  } = props;

  const byValue = useMemo(() => {
    return choicesByValue(choices);
  }, [choices]);

  const toLabelString = useCallback(
    (selected) => {
      const arr = Array.isArray(selected) ? selected : [selected];
      const labels = arr
        .filter((sel: string) => sel in byValue)
        .map((sel: string) => byValue[sel].label);
      return joinIfArray(labels, ', ');
    },
    [byValue]
  );

  if (isLoading) {
    return <CircularProgress />;
  }

  return (
    <StyledFormControl
      key={'select-formcontrol-' + label}
      className={className}
      error={!!error?.message}
      fullWidth={fullWidth}
    >
      <InputLabel
        key={'select-inputlabel-' + label}
        id={'select-inputlabel-' + label}
      >
        {label}
      </InputLabel>
      <Controller
        render={({ field: { value, onChange, onBlur } }) => {
          return (
            <Select
              labelId={'select-inputlabel-' + label}
              label={label}
              key={'select-' + label}
              type="text"
              disabled={disabled}
              multiple={multiple}
              value={value || (multiple ? [] : '')}
              onChange={onChange}
              onBlur={onBlur}
              renderValue={toLabelString}
              inputProps={{ ...selectProps.inputProps, 'data-testid': testId }}
              {...selectProps}
            >
              {choices.map((choice) => (
                <MenuItem
                  key={'select-menuitem-' + choice.key}
                  value={choice.value}
                >
                  {multiple ? (
                    <MultipleSelectItem
                      value={value as string[]}
                      choice={choice}
                    />
                  ) : (
                    choice.label
                  )}
                </MenuItem>
              ))}
            </Select>
          );
        }}
        name={name}
        defaultValue={defaultValue}
        control={control}
        rules={props.required ? required : undefined}
      />
      <FormHelperText key={'select-formhelpertext-' + label} error={true}>
        {error?.message}
      </FormHelperText>
    </StyledFormControl>
  );
}

export type SelectFieldProps<TInitialValues extends FieldValues = FieldValues> =
  EnhancedFieldProps<TInitialValues> &
    Omit<SelectFieldBaseProps, 'defaultValue' | 'error' | 'control'>;

export function SelectField<TInitialValues extends FieldValues = FieldValues>({
  name,
  initialValues,
  ...props
}: SelectFieldProps<TInitialValues>): ReactElement {
  const { defaultValue, error, control } = useEnhancedField<TInitialValues>({
    name,
    initialValues,
  });

  return (
    <SelectFieldBase
      name={name}
      defaultValue={defaultValue}
      error={error}
      control={control}
      {...props}
    />
  );
}

export type SelectArrayFieldProps = EnhancedArrayFieldProps &
  Omit<SelectFieldBaseProps, 'error' | 'name' | 'defaultValue' | 'control'>;

export function SelectArrayField({
  arrayName,
  fieldName,
  field,
  index,
  ...props
}: SelectArrayFieldProps): ReactElement {
  const { error, name, defaultValue, control } = useEnhancedArrayField({
    arrayName,
    fieldName,
    field,
    index,
  });

  return (
    <SelectFieldBase
      error={error}
      name={name}
      defaultValue={defaultValue}
      control={control}
      {...props}
    />
  );
}
