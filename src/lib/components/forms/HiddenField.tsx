import {
  EnhancedArrayFieldProps,
  EnhancedFieldProps,
  useEnhancedArrayField,
  useEnhancedField,
} from './hooks';
import React, { ReactElement, useEffect, useMemo } from 'react';
import { Controller, useFormContext } from 'react-hook-form';
import get from 'lodash/get';

type HiddenFieldBaseProps = {
  name: string;
  defaultValue?: unknown;
};
const HiddenFieldBase = ({
  name,
  defaultValue,
}: HiddenFieldBaseProps): ReactElement => {
  const { control, getValues, setValue } = useFormContext();

  /**
   * Converts `defaultValue` to either `string` or `undefined` data type.
   * If `undefined`, React will not pass it down to the `input` component.
   * Must be `string` otherwise, to follow the standards: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/hidden#value
   */
  const safeDefaultValue = useMemo(() => {
    if (Array.isArray(defaultValue)) {
      return defaultValue;
    }
    if (
      defaultValue !== null &&
      defaultValue !== undefined &&
      defaultValue !== '' &&
      !Number.isNaN(defaultValue)
    ) {
      return String(defaultValue);
    } else {
      return undefined;
    }
  }, [defaultValue]);

  useEffect(() => {
    const value = get(getValues(), name);
    /**
     * Avoid triggering rerender if the type is different but the content is the same
     * (for example "9" vs. 9) as it's converted to a string anyways.
     */
    if (String(value) !== String(safeDefaultValue)) {
      setValue(name, safeDefaultValue);
    }
  }, [safeDefaultValue, getValues, name, setValue]);

  return (
    <Controller
      render={({ field: { onChange, onBlur, ref } }) => (
        <input type="hidden" onChange={onChange} onBlur={onBlur} ref={ref} />
      )}
      defaultValue={safeDefaultValue}
      name={name}
      control={control}
    />
  );
};

/**
 * Can be used to include data in the form state that should not be modifiable by the user,
 * but needs to be sent to the backend anyways.
 * @see defaultValues at https://react-hook-form.com/api/#useForm
 */
export function HiddenField<TInitialValues extends Record<string, unknown>>(
  props: EnhancedFieldProps<TInitialValues>
): ReactElement {
  const { defaultValue } = useEnhancedField(props);
  return <HiddenFieldBase name={props.name} defaultValue={defaultValue} />;
}

/**
 * Can be used to include data in the form state that should not be modifiable by the user,
 * but needs to be sent to the backend anyways.
 * @see defaultValues at https://react-hook-form.com/api/#useForm
 */
export const HiddenArrayField = (
  props: EnhancedArrayFieldProps
): ReactElement => {
  const { name, defaultValue } = useEnhancedArrayField(props);
  return <HiddenFieldBase name={name} defaultValue={defaultValue} />;
};
