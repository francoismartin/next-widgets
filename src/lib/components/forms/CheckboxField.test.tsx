import { FormProvider, UseFormReturn } from 'react-hook-form';
import * as React from 'react';
import { ReactElement } from 'react';
import { CheckboxField } from './CheckboxField';
import { fireEvent, render, screen } from '@testing-library/react';
import { useEnhancedForm } from './hooks';
import { renderHook } from '@testing-library/react-hooks';
import { AnyObject } from '../../../types';

const name = 'accept';
const label = 'I accept';

type FieldValues = Record<typeof name, number>;
type FieldContext = AnyObject;

type TestCheckboxFieldProps = {
  form: UseFormReturn<FieldValues, FieldContext>;
};

const TestCheckboxField = ({ form }: TestCheckboxFieldProps): ReactElement => {
  return (
    <FormProvider {...form}>
      <CheckboxField name={name} label={label} />
    </FormProvider>
  );
};

describe('CheckboxField', function () {
  describe('Component', function () {
    let form: UseFormReturn<FieldValues, FieldContext>;

    beforeEach(() => {
      form = renderHook(() => useEnhancedForm<FieldValues, FieldContext>())
        .result.current;
    });

    it('should render a CheckboxField', async () => {
      render(<TestCheckboxField form={form} />);
      await screen.findByText(label);
    });

    it('should change its value when clicked', async () => {
      render(<TestCheckboxField form={form} />);
      const checkbox = await screen.findByText(label);
      expect(form.getValues()[name]).toBe(false);
      fireEvent.click(checkbox);
      expect(form.getValues()[name]).toBe(true);
    });
  });
});
