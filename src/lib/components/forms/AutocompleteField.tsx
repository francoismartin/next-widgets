import React, {
  ReactElement,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {
  EnhancedArrayFieldProps,
  EnhancedFieldProps,
  useEnhancedArrayField,
  useEnhancedField,
} from './hooks';
import { Choice, ChoiceValue, isChoice } from '../choice';
import { Controller, FieldValues, UseFormReturn } from 'react-hook-form';
import { Autocomplete, CircularProgress } from '@mui/material';
import TextField, { BaseTextFieldProps } from '@mui/material/TextField';
import { required } from './validators';
import { HiddenArrayField, HiddenField } from './HiddenField';
import { SelectFieldBaseProps } from './SelectField';
import { $Values } from 'utility-types';
import { styled } from '@mui/material/styles';
import { toArray } from '../../utils';

export type SelectedChoice = Choice | Choice[] | null;

const StyledTextField = styled(TextField)<{ height?: number }>(
  ({ height }) => ({
    ...(height && {
      '& .MuiInputBase-root': {
        height,
      },
      '& .MuiInputLabel-outlined:not(.MuiInputLabel-shrink)': {
        transform: `translate(14px, ${height / 2 - 11}px) scale(1);`,
      },
    }),
  })
);

// `fullWidth` would conflict with `width` (precedence would be unclear),
// but can be replicated by using `width: '100%'`
type AutocompleteFieldBaseProps = Omit<SelectFieldBaseProps, 'fullWidth'> &
  Pick<UseFormReturn, 'getValues' | 'setValue'> & {
    onChange?: (selectedChoice: SelectedChoice) => void;
    width?: number | string;
    inputSize?: $Values<Pick<BaseTextFieldProps, 'size'>>;
    inputHeight?: number;
    resetToEmpty?: boolean;
  };

function AutocompleteFieldBase(
  props: AutocompleteFieldBaseProps
): ReactElement {
  const {
    name,
    label,
    choices,
    width,
    isLoading,
    testId,
    multiple,
    defaultValue,
    control,
    setValue,
    getValues,
    error,
    disabled,
    onChange: propsOnChange,
    inputSize = 'small',
    inputHeight,
    resetToEmpty = false,
    ...autocompleteProps
  } = props;
  const [touched, setTouched] = useState<boolean>(false);
  const emptyChoice = useMemo(() => (multiple ? [] : null), [multiple]);

  const choiceByValue: Record<string, Choice> = useMemo(() => {
    const map: Record<string, Choice> = {};
    choices.forEach((choice) => {
      map[choice.value] = choice;
    });
    return map;
  }, [choices]);

  const defaultChoice = useMemo(() => {
    if (Array.isArray(defaultValue)) {
      return defaultValue.map((val) => choiceByValue[val]).filter(isChoice);
    } else {
      return choiceByValue[defaultValue as string] ?? emptyChoice;
    }
  }, [choiceByValue, defaultValue, emptyChoice]);

  const [selectedChoice, setSelectedChoice] =
    useState<SelectedChoice>(defaultChoice);

  const getChoiceValues = useCallback(
    (choice: SelectedChoice) => {
      let value: ChoiceValue | ChoiceValue[] | null = emptyChoice;
      if (choice) {
        const singleChoiceValue = (choice as Choice).value;
        if (singleChoiceValue) {
          value = singleChoiceValue;
        } else {
          value = (choice as Choice[]).map((c) => c.value);
        }
      }
      return value;
    },
    [emptyChoice]
  );

  useEffect(() => {
    /**
     * `choices` can be loaded asynchronously.
     * If `choices` are empty on first render, even if `defaultValue` is defined,
     * it will result in an empty `defaultChoice` and consequently also `selectedChoice`.
     *
     * As soon as the `choices` were loaded and are not empty anymore,
     * `selectedChoice` needs to be set again.
     *
     * After the user touched the control for the first time,
     * we should not override their choice with the default.
     */
    if (!touched) {
      setSelectedChoice(defaultChoice);
    }
  }, [defaultChoice, touched]);

  useEffect(() => {
    // value in the form state
    const selectedChoiceForm: string | string[] | undefined = getValues(name);
    const selectedChoiceFormArray: Choice[] | undefined = selectedChoiceForm
      ? toArray(selectedChoiceForm)?.map((value) => choiceByValue[value])
      : undefined;

    // value selected in the component
    const selectedChoiceState: SelectedChoice | undefined =
      selectedChoice ?? undefined;
    const selectedChoiceStateArray: Choice[] | undefined = selectedChoiceState
      ? toArray<Choice>(selectedChoiceState)
      : undefined;

    /**
     * If the form already has values selected on the first render
     * (e.g. when editing an existing resource), {@link selectedChoiceForm} will be defined
     * while {@link selectedChoiceState} will NOT be defined.
     *
     * In all other cases, {@link selectedChoiceState} will be used as reference,
     * since it is used to set {@link selectedChoiceForm} based on it and thus
     * will always be the first value to be up-to-date.
     */
    const selected: Choice[] | undefined = selectedChoiceState
      ? selectedChoiceStateArray
      : selectedChoiceFormArray;

    // if ANY of the selected choices is NOT in the choices, it should clear the selection
    const isSelectedChoiceInChoices = selected?.every(
      (choice) =>
        choice?.value in choiceByValue &&
        choiceByValue[choice?.value].label === choice?.label
    );

    const isAnyChoiceSelectedInComponent = !!selectedChoiceState;

    /**
     * Choices should not be empty (since you would have nothing to choose),
     * except if they are in a loading state. While choices are loaded, the
     * selection should NOT be cleared, as the new choices might be valid.
     */
    const choicesAreLoaded = choices.length;
    if (
      resetToEmpty ||
      (isAnyChoiceSelectedInComponent &&
        !isSelectedChoiceInChoices &&
        choicesAreLoaded)
    ) {
      /**
       * When choices are updated, if the component has a choice selected and
       * the selected value is NOT present in the new choices, clear the selection.
       */
      setValue(name, emptyChoice);
      setSelectedChoice(emptyChoice);
    }
  }, [
    resetToEmpty,
    choices,
    emptyChoice,
    choiceByValue,
    getValues,
    name,
    selectedChoice,
    setValue,
  ]);

  if (isLoading) {
    return <CircularProgress />;
  }

  return (
    <Controller
      render={({ field: { onChange, onBlur, ref } }) => (
        <Autocomplete<Choice, boolean>
          ref={disabled ? undefined : ref}
          style={{ width: width ?? 300 }}
          disabled={disabled}
          options={choices}
          onChange={(_e, choice) => {
            if (!touched) {
              setTouched(true);
            }
            setSelectedChoice(choice); // update Autocomplete with whole choice object
            onChange(getChoiceValues(choice)); // update react-hook-forms with only value
            if (propsOnChange) {
              propsOnChange(choice);
            }
          }}
          getOptionLabel={(choice: Choice) => choice?.label}
          isOptionEqualToValue={(option, choice) =>
            option.value === choice.value
          }
          renderInput={(params) => {
            return (
              <StyledTextField
                {...params}
                inputProps={{ ...params.inputProps, 'data-testid': testId }}
                label={label}
                error={!!error}
                helperText={error?.message}
                size={inputSize}
                height={inputHeight}
              />
            );
          }}
          defaultValue={defaultChoice}
          value={selectedChoice}
          onBlur={onBlur}
          openText={'Open ' + label}
          clearText={'Clear ' + label}
          multiple={!!multiple}
          {...autocompleteProps}
        />
      )}
      defaultValue={defaultValue}
      name={name}
      control={control}
      rules={props.required ? required : undefined}
    />
  );
}

export type AutocompleteFieldProps<
  TInitialValues extends FieldValues = FieldValues
> = EnhancedFieldProps<TInitialValues> &
  Omit<
    AutocompleteFieldBaseProps,
    'defaultValue' | 'error' | 'control' | 'setValue' | 'getValues'
  >;

export function AutocompleteField<
  TInitialValues extends FieldValues = FieldValues
>({
  name,
  initialValues,
  disabled,
  ...props
}: AutocompleteFieldProps<TInitialValues>): ReactElement {
  const enhancedFieldProps = {
    name,
    initialValues,
  };
  const { defaultValue, control, setValue, getValues, error } =
    useEnhancedField<TInitialValues>(enhancedFieldProps);

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise its value becomes 'undefined'
        <HiddenField {...enhancedFieldProps} />
      )}
      <AutocompleteFieldBase
        {...props}
        disabled={disabled}
        name={name}
        defaultValue={defaultValue}
        control={control}
        setValue={setValue}
        getValues={getValues}
        error={error}
      />
    </>
  );
}

export type AutocompleteArrayFieldProps = EnhancedArrayFieldProps &
  Omit<
    AutocompleteFieldBaseProps,
    'error' | 'name' | 'defaultValue' | 'control' | 'getValues' | 'setValue'
  >;

export function AutocompleteArrayField({
  arrayName,
  fieldName,
  field,
  index,
  disabled,
  ...props
}: AutocompleteArrayFieldProps): ReactElement {
  const enhancedFieldProps = {
    arrayName,
    fieldName,
    field,
    index,
  };
  const { error, name, defaultValue, control, setValue, getValues } =
    useEnhancedArrayField(enhancedFieldProps);

  return (
    <>
      {disabled && (
        // Add another hidden field when the field is disabled.
        // Otherwise its value becomes 'undefined'
        <HiddenArrayField {...enhancedFieldProps} />
      )}
      <AutocompleteFieldBase
        {...props}
        error={error}
        name={name}
        disabled={disabled}
        defaultValue={defaultValue}
        control={control}
        setValue={setValue}
        getValues={getValues}
      />
    </>
  );
}
