import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { FormProvider } from 'react-hook-form';
import { useEnhancedForm } from './hooks';
import { LabelledArrayField } from './LabelledField';
import { MultilineField } from './MultilineField';

const fieldName = 'field';
const arrayName = fieldName + 'Array';
const defaultProps = {
  arrayName: arrayName,
  renderChild: (field: Record<'key', string>, index: number) => (
    <LabelledArrayField
      field={field}
      index={index}
      fieldName={fieldName}
      arrayName={arrayName}
    />
  ),
};

function TestMultilineField() {
  const form = useEnhancedForm({
    defaultValues: { fieldArray: [] },
  });
  return (
    <FormProvider {...form}>
      <MultilineField {...defaultProps} />
    </FormProvider>
  );
}

describe('MultilineField', function () {
  beforeEach(() => render(<TestMultilineField />));

  it('should add a new line only if empty or if previous lines are filled', async () => {
    const addButton = await screen.findByRole('button');

    // It should only show the Add button at first
    expect(await screen.findAllByRole('button')).toHaveLength(1);
    expect(screen.queryByRole('textbox')).not.toBeInTheDocument();

    // It should add a line if empty
    fireEvent.click(addButton);
    expect(await screen.findAllByRole('button')).toHaveLength(2);
    expect(await screen.findAllByRole('textbox')).toHaveLength(1);

    // It should NOT add a line if an empty line is available
    fireEvent.click(addButton);
    expect(await screen.findAllByRole('button')).toHaveLength(2);
    expect(await screen.findAllByRole('textbox')).toHaveLength(1);

    const textBox = await screen.findByRole('textbox');
    fireEvent.change(textBox, { target: { value: 'Hello world' } });

    // It should add a line if all lines are filled
    fireEvent.click(addButton);
    expect(await screen.findAllByRole('button')).toHaveLength(3);
    expect(await screen.findAllByRole('textbox')).toHaveLength(2);
  });

  it('should remove the line when pressing the delete button', async () => {
    const addButton = await screen.findByRole('button');
    fireEvent.click(addButton);

    // Check that a line has been rendered
    expect(await screen.findAllByRole('textbox')).toHaveLength(1);

    const buttons = await screen.findAllByRole('button');
    const [deleteButton] = buttons.filter((button) => button !== addButton);
    fireEvent.click(deleteButton);

    // Check that the line has been removed
    expect(screen.queryByRole('textbox')).not.toBeInTheDocument();
  });
});
