import {
  FieldArrayPath,
  FieldArrayWithId,
  FieldError,
  FieldValues,
  useForm,
  useFormContext,
  UseFormProps,
  UseFormReturn,
} from 'react-hook-form';
import get from 'lodash/get';

/**
 * Specifies some default options all of the forms should have,
 * without having to pass them explicitly to all of the usages of `useForm` from `react-hook-form`.
 * All of the options can be overridden in {@code formOptions}.
 * @param formOptions additional options to set, as passed when using `useForm` directly
 */
export function useEnhancedForm<
  TFieldValues extends FieldValues = FieldValues,
  TContext extends Record<string, unknown> = Record<string, unknown>
>(
  formOptions?: UseFormProps<TFieldValues, TContext>
): UseFormReturn<TFieldValues, TContext> {
  const defaultOptions: UseFormProps<TFieldValues, TContext> = {
    mode: 'onTouched',
  };

  return useForm<TFieldValues, TContext>({ ...defaultOptions, ...formOptions });
}

export type EnhancedArrayFieldProps<
  TFieldValues extends FieldValues = FieldValues,
  TFieldArrayName extends FieldArrayPath<TFieldValues> = FieldArrayPath<TFieldValues>,
  TKeyName extends string = 'key'
> = {
  arrayName: string;
  fieldName: string;
  field: Partial<FieldArrayWithId<TFieldValues, TFieldArrayName, TKeyName>>;
  index: number;
};

export function useEnhancedArrayField<
  TFieldValues extends FieldValues = FieldValues,
  TFieldArrayName extends FieldArrayPath<TFieldValues> = FieldArrayPath<TFieldValues>,
  TKeyName extends string = 'key'
>(props: EnhancedArrayFieldProps<TFieldValues, TFieldArrayName, TKeyName>) {
  const { arrayName, field, fieldName, index } = props;

  const methods = useFormContext<TFieldValues>();
  const {
    formState: { errors },
  } = methods;

  const name = `${arrayName}[${index}].${fieldName}`;
  const defaultValue = get(field, fieldName);
  const error: FieldError = get(errors, [arrayName, index, fieldName]);

  return { ...methods, name, defaultValue, error };
}

export type UseEnhancedFieldHookReturn = {
  defaultValue?: string | number | string[] | number[] | unknown;
  error: FieldError;
};

export interface UseEnhancedFieldHook {
  <TInitialValues extends FieldValues = FieldValues>(
    props: EnhancedFieldProps<TInitialValues>
  ): ReturnType<typeof useFormContext> & UseEnhancedFieldHookReturn;
}

export type EnhancedFieldProps<
  TInitialValues extends FieldValues = FieldValues
> = {
  name: string;
  initialValues?: TInitialValues;
};

export const useEnhancedField: UseEnhancedFieldHook = (props) => {
  const { name, initialValues } = props;

  const methods = useFormContext();
  const {
    formState: { errors },
  } = methods;
  const defaultValue = get(initialValues, name) ?? '';
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const error: any = get(errors, name); // FIXME FIXME 2020-07-12, François Martin

  return { ...methods, defaultValue, error };
};
