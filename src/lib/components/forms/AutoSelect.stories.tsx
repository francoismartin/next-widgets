import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { AutoSelectBase } from './AutoSelect';

export default {
  title: 'Components/Forms/AutoSelect',
  component: AutoSelectBase,
} as ComponentMeta<typeof AutoSelectBase>;

const colors = [
  { name: 'Red', code: 'red' },
  { name: 'Green', code: 'green' },
  { name: 'Blue', code: 'blue' },
];

const Template: ComponentStory<typeof AutoSelectBase> = (args) => {
  return (
    <AutoSelectBase
      {...args}
      label={'Favorite Color'}
      placeholder="Choose a color"
      choices={colors}
      fields={['name', 'code']}
    />
  );
};

export const Default = Template.bind({});
Default.args = {};
