import * as React from 'react';
import {
  ID_REGEX,
  IP_ADDRESS_REGEX,
  mustBe,
  NAME_REGEX,
  PGP_FINGERPRINT_REGEX,
  URL_REGEX,
  USERNAME_REGEX,
} from './validators';
import { FormProvider } from 'react-hook-form';
import { LabelledField } from './LabelledField';
import { useEnhancedForm } from './hooks';
import { fireEvent, render, screen } from '@testing-library/react';

describe('validators', () => {
  describe('regex', () => {
    describe('username regex', () => {
      it.each`
        input                  | isValid
        ${'CHRISTIANR'}        | ${false}
        ${'foo-bar'}           | ${false}
        ${'chri'}              | ${false}
        ${'christianr'}        | ${true}
        ${'abcdefghijklmnop'}  | ${true}
        ${'abcdefghijklmnopq'} | ${false}
        ${'ululu'}             | ${false}
        ${'ululul'}            | ${true}
      `(
        'should validate the username "$input" as $isValid',
        ({ input, isValid }) => {
          expect(USERNAME_REGEX.test(input)).toEqual(isValid);
        }
      );
    });

    describe('pgpFingerprint regex', () => {
      it.each`
        input                                                 | isValid
        ${'0123456789ABCDEF0123456789ABCDEF01234567'}         | ${true}
        ${'0123456789ABCDEF'}                                 | ${false}
        ${'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF'} | ${false}
        ${'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123'}         | ${false}
      `(
        'should validate the fingerprint "$input" as $isValid',
        ({ input, isValid }) => {
          expect(PGP_FINGERPRINT_REGEX.test(input)).toEqual(isValid);
        }
      );
    });

    describe('name regex', () => {
      it.each`
        input                  | isValid
        ${'Chuck_Nörriß-1940'} | ${true}
        ${'Roundhouse Kick!'}  | ${false}
      `(
        'should validate the name "$input" as $isValid',
        ({ input, isValid }) => {
          expect(NAME_REGEX.test(input)).toEqual(isValid);
        }
      );
    });

    describe('id regex', () => {
      it.each`
        input                  | isValid
        ${'chuck_norris_1940'} | ${true}
        ${'Chuck_Norris_1940'} | ${false}
        ${'chuck-norris-1940'} | ${false}
      `('should validate the id "$input" as $isValid', ({ input, isValid }) => {
        expect(ID_REGEX.test(input)).toEqual(isValid);
      });
    });

    describe('ipAddress regex', () => {
      it.each`
        input             | isValid
        ${'127.0.0.1'}    | ${true}
        ${'127.O.O.1'}    | ${false}
        ${'127.0.0.1/32'} | ${false}
        ${'127.0.0.2555'} | ${false}
        ${'1277.0.0.1'}   | ${false}
        ${'127.0.1'}      | ${false}
      `(
        'should validate the IP Address "$input" as $isValid',
        ({ input, isValid }) => {
          expect(IP_ADDRESS_REGEX.test(input)).toEqual(isValid);
        }
      );
    });

    describe('url regex', () => {
      it.each`
        input                     | isValid
        ${'https://chuck.norris'} | ${true}
        ${'http://chuck.norris'}  | ${true}
        ${'ftp://chuck.norris'}   | ${true}
        ${'sftp://chuck.norris'}  | ${true}
        ${'ssh://chuck.norris'}   | ${true}
        ${'https//chuck.norris'}  | ${false}
        ${'https:/chuck.norris'}  | ${false}
        ${'htps://chuck.norris'}  | ${false}
      `(
        'should validate the IP Address "$input" as $isValid',
        ({ input, isValid }) => {
          expect(URL_REGEX.test(input)).toEqual(isValid);
        }
      );
    });
  });

  describe('custom validators', () => {
    function TestLabelledField({ validator }) {
      const fieldName = 'name';
      const form = useEnhancedForm();
      return (
        <FormProvider {...form}>
          <LabelledField name={fieldName} validations={validator} />
        </FormProvider>
      );
    }

    describe('mustBe', () => {
      const target = 'correct';
      const errorMessage = 'Must be ' + target;

      beforeEach(() => {
        render(<TestLabelledField validator={mustBe(target)} />);
      });

      it.each`
        input      | isValid
        ${target}  | ${true}
        ${'wrong'} | ${false}
      `(
        'should validate input "$input" as $isValid',
        async ({ input, isValid }) => {
          const textBox = await screen.findByRole('textbox');
          fireEvent.change(textBox, { target: { value: input } });
          fireEvent.blur(textBox);

          if (isValid) {
            expect(screen.queryByText(errorMessage)).not.toBeInTheDocument();
          } else {
            await screen.findByText(errorMessage);
          }
        }
      );
    });
  });
});
