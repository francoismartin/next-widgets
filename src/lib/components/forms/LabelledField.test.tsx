import { useEnhancedForm } from './hooks';
import { FormProvider } from 'react-hook-form';
import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import {
  expectJsonHeader,
  expectRequestBody,
  MockServer,
  setupMockApi,
} from '../../testUtils';
import { StatusCodes } from 'http-status-codes';
import { AnyObject } from '../../../types';
import { rest } from 'msw';
import { LabelledField, LabelledFieldProps } from './LabelledField';

const testName = 'username';
const testLabel = 'Username';

const url = 'http://localhost:8000/backend/identity/users/unique/';

function TestLabelledField(
  labelledFieldProps: Partial<LabelledFieldProps<AnyObject>>
) {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <LabelledField
        name={testName}
        type="text"
        unique={async (value) => {
          const response = await fetch(url, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(value),
          });
          if (response.status < 200 || response.status >= 300) {
            throw response;
          }
        }}
        label={testLabel}
        {...labelledFieldProps}
      />
    </FormProvider>
  );
}

describe('LabelledField', function () {
  const uniqueValidationErrorText = 'Must be unique.';

  let server: MockServer;

  afterEach(() => {
    server.close();
  });

  const text = 'chuck_norris_research';
  const requestBody = {
    [testName]: text,
  };

  describe('without default value', function () {
    beforeEach(() => {
      render(<TestLabelledField />);
    });

    it.each`
      responseStatusCode      | isUniqueError
      ${StatusCodes.CONFLICT} | ${true}
      ${StatusCodes.OK}       | ${false}
    `(
      'should show unique validation error text ($isUniqueError) if response status code from backend is $responseStatusCode',
      async ({ responseStatusCode, isUniqueError }) => {
        server = setupMockApi(
          rest.post(url, (req, res, ctx) => {
            expectJsonHeader(req);
            expectRequestBody(req, requestBody);
            return res(ctx.status(responseStatusCode));
          })
        );

        const input = await screen.findByRole('textbox');
        fireEvent.change(input, { target: { value: text } });
        fireEvent.blur(input); // triggers validation

        if (isUniqueError) {
          await screen.findByText(uniqueValidationErrorText);
        } else {
          expect(
            screen.queryByText(uniqueValidationErrorText)
          ).not.toBeInTheDocument();
        }
      }
    );
  });

  describe('with default value', function () {
    const defaultValue = 'chuck';

    beforeEach(() => {
      render(
        <TestLabelledField initialValues={{ [testName]: defaultValue }} />
      );
    });

    it('should not show unique validation error text if the current value is equal to the initial value', async () => {
      const uniqueValidationErrorText = 'Must be unique.';

      server = setupMockApi(
        rest.post(url, (req, res, ctx) => {
          expectJsonHeader(req);
          expectRequestBody(req, requestBody);
          return res(ctx.status(StatusCodes.CONFLICT));
        })
      );

      const input = await screen.findByRole('textbox');
      fireEvent.change(input, { target: { value: text } });
      fireEvent.blur(input); // triggers validation

      const uniqueValidationError = await screen.findByText(
        uniqueValidationErrorText
      );

      fireEvent.change(input, { target: { value: defaultValue } });
      fireEvent.blur(input); // triggers validation

      await waitForElementToBeRemoved(uniqueValidationError);
    });
  });
});
