import React, { ReactElement, ReactNode, useCallback } from 'react';
import { useFieldArray, useFormContext } from 'react-hook-form';
import { styled } from '@mui/material/styles';
import { AddIconButton, DeleteIconButton } from '../../widgets';
import { Box } from '@mui/material';
import { isNotEmpty } from '../../utils';

export type MultilineFieldProps = {
  arrayName: string;
  addButtonDisabled?: boolean;
  deleteButtonDisabled?: boolean;
  renderChild: (field: Record<'key', string>, index: number) => ReactNode;
};

const ListItem = styled('div')(({ theme }) => ({
  marginLeft: theme.spacing(5),
  paddingBottom: theme.spacing(1),
  '&:last-child': {
    paddingBottom: 0,
  },
}));

const StyledDeleteIconButton = styled(DeleteIconButton)(({ theme }) => ({
  marginRight: theme.spacing(1),
}));

export function MultilineField({
  arrayName,
  addButtonDisabled,
  deleteButtonDisabled,
  renderChild,
}: MultilineFieldProps): ReactElement {
  const { control, getValues } = useFormContext();

  const { fields, append, remove } = useFieldArray({
    control,
    name: arrayName,
    keyName: 'key',
  });

  const onAddClick = useCallback(() => {
    // 'getValues' could return 'undefined'
    const values = getValues()[arrayName] || [];
    const len = values.length;
    // Append only if empty or if last entry is not empty
    if (len === 0 || isNotEmpty(values[len - 1])) {
      append({});
    }
  }, [append, getValues, arrayName]);

  return (
    <>
      <AddIconButton onClick={onAddClick} disabled={addButtonDisabled} />
      {fields.map((field, index) => (
        <ListItem key={field.key}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              flexWrap: 'nowrap',
              justifyContent: 'flex-start',
              alignItems: 'flex-start',
            }}
          >
            <StyledDeleteIconButton
              onClick={() => remove(index)}
              disabled={deleteButtonDisabled}
            />
            {renderChild(field, index)}
          </Box>
        </ListItem>
      ))}
    </>
  );
}
