import { RegisterOptions } from 'react-hook-form';
import isemail from 'isemail';

export const NAME_REGEX = /^[a-zA-Z0-9\- _()ßàÀéÉèÈäÄëËïÏöÖüÜçÇâÂêÊîÎôÔûÛ']*$/;
export const USERNAME_REGEX = /^[a-z][a-z0-9_]{5,15}$/;
export const ID_REGEX = /^[a-z0-9_]*$/;
export const IP_ADDRESS_REGEX =
  /^(0|[1-9][0-9]{0,2})(\.(0|[1-9][0-9]{0,2})){3}$/;
export const PGP_FINGERPRINT_REGEX = /^[A-F0-9]{40}$/;
export const URL_REGEX = /^(ssh|http|https|ftp|sftp):\/\/.*$/;

export const nameValidations = <T>(
  idFieldLabel?: string,
  idFieldValue?: T
): RegisterOptions[] => {
  const validations: RegisterOptions[] = [required, nameChars, maxLength(512)];
  if (!!idFieldLabel && !!idFieldValue) {
    validations.push(notSameAs(idFieldLabel, idFieldValue));
  }
  return validations;
};

export const idValidations = <T>(
  nameFieldLabel?: string,
  nameFieldValue?: T
): RegisterOptions[] => {
  const validations: RegisterOptions[] = [required, idChars, maxLength(32)];
  if (!!nameFieldLabel && !!nameFieldValue) {
    validations.push(notSameAs(nameFieldLabel, nameFieldValue));
  }
  return validations;
};

export const required: RegisterOptions = {
  required: 'Required',
};

export const nameChars: RegisterOptions = {
  pattern: {
    value: NAME_REGEX,
    message:
      "Only alphanumeric characters, - _ ( ) ' spaces and diacritics are allowed",
  },
};

export const idChars: RegisterOptions = {
  pattern: {
    value: ID_REGEX,
    message: 'Only lowercase alphanumeric characters and _ are allowed.',
  },
};

export const ipAddress: RegisterOptions = {
  pattern: {
    value: IP_ADDRESS_REGEX,
    message: 'Must be an IP address',
  },
};

export const url: RegisterOptions = {
  pattern: {
    value: URL_REGEX,
    message: 'Must be a valid URL',
  },
};

export const pgpFingerprint: RegisterOptions = {
  pattern: {
    value: PGP_FINGERPRINT_REGEX,
    message: 'Must be a valid PGP fingerprint',
  },
};

export const minValue = (value: number): RegisterOptions => {
  return {
    min: {
      value,
      message: `Must be >= ${value}`,
    },
  };
};

export const maxValue = (value: number): RegisterOptions => {
  return {
    max: {
      value,
      message: `Must be <= ${value}`,
    },
  };
};

export const minLength = (value: number): RegisterOptions => {
  return {
    minLength: {
      value,
      message: `Must have at least ${value} characters`,
    },
  };
};

export const maxLength = (value: number): RegisterOptions => {
  return {
    maxLength: {
      value,
      message: `Must have less than ${value} characters`,
    },
  };
};

export const username: RegisterOptions = {
  pattern: {
    value: USERNAME_REGEX,
    message:
      'Must be between 6 and 16 characters, only include lowercase alphanumeric characters and underscores and start with a letter',
  },
};

export const email: RegisterOptions = {
  validate: {
    email: (value) => {
      // minDomainAtoms: the minimum number of domain levels.
      // e.g. for 2, test@foo is invalid, but test@foo.bar or
      // test@subdomain.foo.bar are valid
      if (!value || value.length === 0) {
        return true;
      }
      return (
        isemail.validate(value, { minDomainAtoms: 2 }) || 'Invalid email format'
      );
    },
  },
};

export const notSameAs = <T>(
  otherFieldLabel: string,
  otherFieldValue: T
): RegisterOptions => ({
  validate: {
    notSameAs: (value) =>
      value !== otherFieldValue || `Must not be the same as ${otherFieldLabel}`,
  },
});

export const mustBe = <T>(target: T): RegisterOptions => {
  return {
    validate: (value) => value === target || `Must be ${target}`,
  };
};
