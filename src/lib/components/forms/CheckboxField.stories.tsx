import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { CheckboxField } from './CheckboxField';
import { FormProvider } from 'react-hook-form';
import { useEnhancedForm } from './hooks';

export default {
  title: 'Components/Forms/CheckboxField',
  component: CheckboxField,
} as ComponentMeta<typeof CheckboxField>;

const Template: ComponentStory<typeof CheckboxField> = (args) => {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <CheckboxField {...args} name="accept" label={'I accept'} />
    </FormProvider>
  );
};

export const Empty = Template.bind({});
Empty.args = {};

export const InitialValue = Template.bind({});
InitialValue.args = { initialValues: { accept: true } };

export const Required = Template.bind({});
Required.args = { required: true };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };
