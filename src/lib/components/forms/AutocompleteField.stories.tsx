import React, { useState } from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { AutocompleteField } from './AutocompleteField';
import { FormProvider } from 'react-hook-form';
import { useEnhancedForm } from './hooks';
import { useChoices } from '../choice';
import { Button } from '../../widgets';

export default {
  title: 'Components/Forms/AutocompleteField',
  component: AutocompleteField,
} as ComponentMeta<typeof AutocompleteField>;

const defaultColors = [
  { name: 'Red', code: 'red' },
  { name: 'Green', code: 'green' },
  { name: 'Blue', code: 'blue' },
];

const otherColors = [
  { name: 'Black', code: 'black' },
  { name: 'Yellow', code: 'yellow' },
  { name: 'Cyan', code: 'cyan' },
];

const Template: ComponentStory<typeof AutocompleteField> = (args) => {
  const form = useEnhancedForm();

  const [colors, setColors] = useState<typeof defaultColors>(defaultColors);

  return (
    <>
      <FormProvider {...form}>
        <AutocompleteField
          {...args}
          name="favoriteColor"
          label={'Favorite Color'}
          choices={useChoices(colors, 'code', 'name')}
        />
      </FormProvider>
      <Button onClick={() => setColors(defaultColors)}>Default Colors</Button>
      <Button onClick={() => setColors(otherColors)}>Other Colors</Button>
    </>
  );
};

export const Default = Template.bind({});
Default.args = {
  initialValues: {
    favoriteColor: 'red',
  },
};

export const InputHeight = Template.bind({});
InputHeight.args = { inputHeight: 50 };

export const FullWidth = Template.bind({});
FullWidth.args = { width: '100%' };

export const Multiple = Template.bind({});
Multiple.args = { multiple: true };

export const MediumSized = Template.bind({});
MediumSized.args = { inputSize: 'medium' };
