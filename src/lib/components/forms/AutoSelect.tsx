// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck TODO
import React, { ReactElement } from 'react';
import { styled } from '@mui/material/styles';
import Downshift from 'downshift';
import { Paper, TextField } from '@mui/material';
import { grey } from '@mui/material/colors';

const PREFIX = 'AutoSelect';

const classes = {
  container: `${PREFIX}-container`,
  paper: `${PREFIX}-paper`,
  inputRoot: `${PREFIX}-inputRoot`,
  inputInput: `${PREFIX}-inputInput`,
};

const StyledDownshift = styled(Downshift)(({ theme }) => ({
  [`& .${classes.container}`]: {
    flexGrow: 1,
    position: 'relative',
  },

  [`& .${classes.paper}`]: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },

  [`& .${classes.inputRoot}`]: {
    flexWrap: 'wrap',
  },

  [`& .${classes.inputInput}`]: {
    width: 'auto',
    flexGrow: 1,
  },
}));

const StyledRow = styled('tr')(() => ({
  '&:hover': { backgroundColor: grey[300], cursor: 'pointer' },
}));

const StyledCell = styled('td')(({ theme }) => ({
  padding: theme.spacing(2),
}));

function getSuggestions(suggestions, fields, value) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;

  return inputLength === 0
    ? []
    : suggestions.filter((suggestion) => {
        const keep =
          count < 5 &&
          fields.some((f) =>
            suggestion[f].toLowerCase().startsWith(inputValue)
          );
        if (keep) {
          count += 1;
        }
        return keep;
      });
}

export function AutoSelectBase({
  label,
  placeholder,
  choices,
  fields,
  textFieldProps,
  onSelect,
  onChange,
  error,
  ...other
}): ReactElement {
  return (
    <StyledDownshift
      itemToString={(item) => (item ? item.label : '')}
      onChange={onSelect}
      {...other}
    >
      {({
        getInputProps,
        getItemProps,
        getLabelProps,
        getMenuProps,
        inputValue,
        isOpen,
      }) => {
        const {
          onBlur,
          onFocus,
          onChange: _onChange,
          ...inputProps
        } = getInputProps({ placeholder });
        return (
          <div className={classes.container}>
            <TextField
              size="small"
              InputProps={{
                classes: {
                  root: classes.inputRoot,
                  input: classes.inputInput,
                },
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore // TODO
                onBlur,
                onFocus,
                onChange: (ev) => {
                  onChange(ev);
                  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                  // @ts-ignore // TODO
                  _onChange(ev);
                },
                ...inputProps,
              }}
              fullWidth={true}
              placeholder={placeholder || label}
              error={!!error}
              helperText={error?.message}
              label={label}
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore // TODO
              InputLabelProps={getLabelProps({ shrink: true })}
              inputProps={inputProps}
              {...textFieldProps}
            />
            <div {...getMenuProps()}>
              {isOpen ? (
                <Paper className={classes.paper} square>
                  <table style={{ borderSpacing: 0 }}>
                    <tbody>
                      {getSuggestions(choices, fields, inputValue).map(
                        (suggestion) => (
                          // eslint-disable-next-line react/jsx-key
                          <StyledRow
                            {...getItemProps({
                              item: suggestion,
                              key: suggestion.id,
                            })}
                          >
                            {fields.map((field) => (
                              <StyledCell key={field}>
                                {suggestion[field]}
                              </StyledCell>
                            ))}
                          </StyledRow>
                        )
                      )}
                    </tbody>
                  </table>
                </Paper>
              ) : null}
            </div>
          </div>
        );
      }}
    </StyledDownshift>
  );
}
