import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { SelectField } from './SelectField';
import { FormProvider } from 'react-hook-form';
import { useEnhancedForm } from './hooks';
import { useChoices } from '../choice';

export default {
  title: 'Components/Forms/SelectField',
  component: SelectField,
} as ComponentMeta<typeof SelectField>;

const colors = [
  { name: 'Red', code: 'red' },
  { name: 'Green', code: 'green' },
  { name: 'Blue', code: 'blue' },
];

const Template: ComponentStory<typeof SelectField> = (args) => {
  const form = useEnhancedForm();
  return (
    <FormProvider {...form}>
      <SelectField
        {...args}
        name="favoriteColor"
        label={'Favorite Color'}
        choices={useChoices(colors, 'code', 'name')}
      />
    </FormProvider>
  );
};

export const Empty = Template.bind({});
Empty.args = {};

export const InitialValue = Template.bind({});
InitialValue.args = { initialValues: { favoriteColor: 'blue' } };

export const Loading = Template.bind({});
Loading.args = { isLoading: true };

export const Required = Template.bind({});
Required.args = { required: true };

export const Disabled = Template.bind({});
Disabled.args = { disabled: true };

export const FullWidth = Template.bind({});
FullWidth.args = { fullWidth: true };
