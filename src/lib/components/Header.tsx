import { styled } from '@mui/material/styles';
import React, { ReactElement } from 'react';
import Image from 'next/image';
import { Link } from '@mui/material';

const Root = styled('div')(({ theme }) => ({
  textAlign: 'right',
  paddingRight: theme.spacing(1),
  paddingTop: theme.spacing(1),
}));

export type HeaderProps = {
  logoSrc: string;
  logoText: string;
  logoWidth: number | string;
  logoHeight: number | string;
  logoHref?: string;
};

export const Header = ({
  logoSrc,
  logoText,
  logoWidth,
  logoHeight,
  logoHref,
}: HeaderProps): ReactElement => (
  <Root>
    <Link href={logoHref} target={'_blank'} rel={'noopener noreferrer'}>
      <Image
        src={logoSrc}
        width={logoWidth}
        height={logoHeight}
        alt={logoText}
      />
    </Link>
  </Root>
);
