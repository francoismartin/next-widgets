import { Button, Typography } from '@mui/material';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';
import { PageBase } from './PageBase';

type CustomComponentProps = {
  text: string;
};

const CustomComponent = ({ text }: CustomComponentProps) => (
  <Typography>{text}</Typography>
);

export default {
  title: 'Components/PageBase',
  component: PageBase,
} as ComponentMeta<typeof PageBase>;

const Template: ComponentStory<typeof PageBase> = (args) => (
  <PageBase
    {...args}
    title={['Foo', 'Bar', 'Baz']}
    content={() => <Typography>{'42'}</Typography>}
  />
);

const TemplateWithGeneric: ComponentStory<typeof PageBase> = (args) => (
  <PageBase<CustomComponentProps>
    {...args}
    title={['Foo', 'Bar', 'Baz']}
    content={CustomComponent}
    props={{ text: '42' }}
  />
);

export const Default = Template.bind({});

export const WithToolbar = Template.bind({});
WithToolbar.args = {
  toolbar: () => <Button>{'toolbar'}</Button>,
};

export const WithContentProps = TemplateWithGeneric.bind({});
