import { Action } from 'redux';
import produce from 'immer';
import React from 'react';

export enum ToastSeverity {
  ERROR = 'error',
  INFO = 'info',
  SUCCESS = 'success',
  WARNING = 'warning',
}

export type Toast = {
  message?: React.ReactNode;
  severity?: ToastSeverity;
  correlationId?: string;
};

export type ToastState = Toast & {
  queue: Toast[];
};

const initialState: ToastState = {
  message: undefined,
  severity: undefined,
  queue: [],
};

export const SET_TOAST = 'SET_TOAST' as const;
export const setToast = (toast: Toast): Action<typeof SET_TOAST> & Toast => ({
  type: SET_TOAST,
  ...toast,
});

export const CLEAR_TOAST = 'CLEAR_TOAST' as const;
export const clearToast = (): Action<typeof CLEAR_TOAST> => ({
  type: CLEAR_TOAST,
});

type ActionType = ReturnType<typeof setToast> | ReturnType<typeof clearToast>;

export function toast(
  state: ToastState = initialState,
  action: ActionType
): ToastState {
  return produce(state, (draft: ToastState) => {
    switch (action.type) {
      case SET_TOAST:
        if (draft.message) {
          // toast is currently showing
          draft.queue.push({
            message: action.message,
            severity: action.severity,
            correlationId: action.correlationId,
          });
        } else {
          draft.message = action.message;
          draft.severity = action.severity;
          draft.correlationId = action.correlationId;
        }
        break;
      case CLEAR_TOAST:
        if (draft.queue.length) {
          const next = draft.queue.shift() as Toast;
          draft.message = next.message;
          draft.severity = next.severity;
          draft.correlationId = next.correlationId;
        } else {
          draft.message = undefined;
          draft.severity = undefined;
          draft.correlationId = undefined;
        }
        break;
    }
  });
}
