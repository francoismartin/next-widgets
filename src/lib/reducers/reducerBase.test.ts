import { BaseReducerState, reducer } from './reducerBase';
import { IdType } from '../../types';

const getTestState = (): BaseReducerState<IdType> => ({
  isFetching: true,
  isSubmitting: false,
  itemList: [],
});

test('utils', () => {
  const TEST_ACTION = {
    request: 'TEST_REQUEST' as const,
    success: 'TEST_SUCCESS' as const,
    failure: 'TEST_FAILURE' as const,
    apiFunction: jest.fn(),
  };
  const testReducer = reducer({ load: TEST_ACTION });
  const state = getTestState();
  expect(testReducer(state, { type: 'TEST_REQUEST' as const }).isFetching).toBe(
    true
  );
  const state2 = getTestState();
  const element = { id: 1 };
  const result = testReducer(state2, {
    type: 'TEST_SUCCESS' as const,
    response: [element],
  });
  expect(result).toEqual({
    isFetching: false,
    itemList: [element],
    isSubmitting: false,
  });
});
