import initStoryshots from '@storybook/addon-storyshots';
import { render } from '@testing-library/react';

const reactTestingLibrarySerializer = {
  print: (val, serialize) => serialize(val.container.firstChild),
  test: (val) => {
    return val && Object.prototype.hasOwnProperty.call(val, 'container');
  },
};

initStoryshots({
  framework: 'react',
  renderer: render,
  snapshotSerializers: [reactTestingLibrarySerializer],
});
