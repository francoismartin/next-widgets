// Debounce time between unique validation calls to the backend in milliseconds
export const uniqueValidationDebounce =
  process.env.NEXT_PUBLIC_UNIQUE_VALIDATION_DEBOUNCE;
