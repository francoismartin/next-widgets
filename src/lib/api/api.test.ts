import { getCookie, getCsrfToken } from './api';

describe('api', () => {
  describe('cookies', () => {
    const csrfToken =
      'w6yQ4YuA4fomSvwyhmtYrpA6YLlhHG3idTVVBe1FDj2tZkLAcRSok6WtrERGPCrB';
    const documentCookieCsrf = `Cookie: CookieConsent=true; Idea-5c024b95=900eefca-6531-4e40-98f3-90a4ed0cee8c; _ga=GA1.1.744548955.1578933912; hubspotutk=3c6af01680195c85496d8da01381685b; next-i18next=de; Idea-5c024b96=ce76f772-8c16-4b74-abdd-277dd9aba89b; _gcl_au=1.1.918568128.1587329434; _pk_id.1.1fff=e178481d53911b28.1587329440.1.1587329440.1587329440.; __hssrc=1; Idea-2f3735fc=0a4f8a34-0ace-4183-b9ee-83439983633b; __hstc=181257784.3c6af01680195c85496d8da01381685b.1578933913699.1589968086021.1591457399458.9; tabstyle=raw-tab; sessionid=aodu62t3dljxkzenerr3q7w4wzbfmdtf; csrftoken=${csrfToken}`;

    function setCookies(cookies: string | undefined) {
      if (!cookies) {
        document.cookie = '';
      } else {
        cookies
          .split(';')
          .forEach((cookie) => (document.cookie = cookie.trim()));
      }
    }

    // see https://stackoverflow.com/questions/179355/clearing-all-cookies-with-javascript
    function clearCookies() {
      const cookies = document.cookie.split(';');

      for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i];
        const eqPos = cookie.indexOf('=');
        const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
      }
    }

    beforeEach(() => {
      clearCookies();
    });

    describe('getCookie', () => {
      const documentCookie1 = 'Cookie1=true';
      const documentCookie2 = `${documentCookie1}; tabstyle=raw-tab`;
      const documentCookie3 = `${documentCookie2}; Cookie2=false`;
      const documentCookieSpecial = `${documentCookie3}; _pk_id.1.1fff=1.1.-.`;
      const documentCookieEncoded = `${documentCookie3}; username=Fran%C3%A7ois`;

      it.each`
        documentCookie           | input              | expectedOutput
        ${documentCookie1}       | ${'Cookie1'}       | ${'true'}
        ${documentCookie1}       | ${'Cookie2'}       | ${''}
        ${documentCookie1}       | ${'_pk_id.1.1fff'} | ${''}
        ${documentCookie2}       | ${'Cookie1'}       | ${'true'}
        ${documentCookie2}       | ${'tabstyle'}      | ${'raw-tab'}
        ${documentCookie2}       | ${'Cookie2'}       | ${''}
        ${documentCookie3}       | ${'Cookie1'}       | ${'true'}
        ${documentCookie3}       | ${'tabstyle'}      | ${'raw-tab'}
        ${documentCookie3}       | ${'Cookie2'}       | ${'false'}
        ${documentCookieSpecial} | ${'_pk_id.1.1fff'} | ${'1.1.-.'}
        ${documentCookieEncoded} | ${'username'}      | ${'François'}
        ${documentCookieCsrf}    | ${'csrftoken'}     | ${csrfToken}
        ${''}                    | ${'Cookie1'}       | ${''}
        ${undefined}             | ${'Cookie1'}       | ${''}
      `(
        'should return "$expectedOutput" when getting cookie "$input" from document.cookie as "$documentCookie"',
        ({ documentCookie, input, expectedOutput }) => {
          // given
          setCookies(documentCookie);

          // when
          const result = getCookie(input);

          // then
          expect(result).toEqual(expectedOutput);
        }
      );
    });

    describe('getCsrfToken', () => {
      it.each`
        documentCookie        | expectedOutput
        ${documentCookieCsrf} | ${csrfToken}
      `(
        'should return "$expectedOutput" when getting CSRF token from document.cookie as "$documentCookie"',
        ({ documentCookie, expectedOutput }) => {
          // given
          setCookies(documentCookie);

          // when
          const result = getCsrfToken();

          // then
          expect(result).toEqual(expectedOutput);
        }
      );
    });
  });
});
