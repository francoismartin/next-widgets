export enum Environment {
  // when app is run using `npm start` or `npm run start`
  DEVELOPMENT = 'development',
  // when app is run after building with `npm build` or `npm run build`
  PRODUCTION = 'production',
  // when app is run using `npm test` or `npm run test`
  TEST = 'test',
}

export const apiHost = (includeTrailingSlash = true): string => {
  let host;
  switch (process.env.NODE_ENV) {
    case Environment.TEST:
    // fall through
    case Environment.DEVELOPMENT:
      host = 'http://localhost:8000';
      break;
    case Environment.PRODUCTION:
      host = '';
      break;
  }
  if (includeTrailingSlash) {
    host += '/';
  }
  return host ?? '';
};
