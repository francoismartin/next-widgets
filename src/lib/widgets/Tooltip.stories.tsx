import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Tooltip } from './Tooltip';

export default {
  title: 'Widgets/Tooltip',
  component: Tooltip,
} as ComponentMeta<typeof Tooltip>;

const Template: ComponentStory<typeof Tooltip> = (args) => {
  return <Tooltip title="This is a tooltip!">{args.children}</Tooltip>;
};

export const Text = Template.bind({});
Text.args = {
  children: <span>Test</span>,
};
