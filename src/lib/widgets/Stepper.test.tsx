import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Step, Stepper } from './Stepper';

const steps = [
  { location: 'Step 1', text: 'First step' },
  { location: 'Step 2', text: 'Second Step' },
  { location: 'Step 3' },
];

describe('Stepper', function () {
  describe('Component', function () {
    it('should render the component without errors or warnings', async () => {
      render(
        <Stepper activeStep={0}>
          {steps.map((step) => (
            <Step key={'step-' + step.location}>
              <p>{step.location + step.text}</p>
            </Step>
          ))}
        </Stepper>
      );

      for (const step of steps) {
        await screen.findByText(step.location + step.text);
      }
    });
  });
});
