import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { CancelLabel, Dialog } from './Dialog';
import { Typography } from '@mui/material';

export default {
  title: 'Widgets/Dialog',
  component: Dialog,
} as ComponentMeta<typeof Dialog>;

const Template: ComponentStory<typeof Dialog> = (args) => (
  <Dialog {...args} open={true} title="Once upon a time...">
    <Typography variant="body2">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
      commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
      velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
      cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
      est laborum
    </Typography>
  </Dialog>
);

export const Open = Template.bind({});
Open.args = {};

export const FullWidth = Template.bind({});
FullWidth.args = { fullWidth: true, maxWidth: 'lg' };

export const CloseCancelLabel = Template.bind({});
CloseCancelLabel.args = { cancelLabel: CancelLabel.CLOSE };

export const WithConfirm = Template.bind({});
WithConfirm.args = {
  confirmLabel: 'Approve',
  confirmButtonProps: { onClick: () => null },
};
