import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import {
  AddIconButton,
  Button,
  DeleteIconButton,
  EditIconButton,
  FabButton,
} from './Buttons';

export default {
  title: 'Widgets/Buttons',
  component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof FabButton> = (args) => {
  return <FabButton {...args} />;
};

export const Default = Template.bind({});
Default.args = { icon: 'search' };

export const WithTooltip = Template.bind({});
WithTooltip.args = { ...Default.args, title: 'Tooltip' };

export const MediumButton = Template.bind({});
MediumButton.args = { ...Default.args, size: 'medium' };

export const SmallButton = Template.bind({});
SmallButton.args = { ...Default.args, size: 'small' };

export const Add: ComponentStory<typeof Button> = () => {
  return <AddIconButton />;
};

export const Edit: ComponentStory<typeof Button> = () => {
  return <EditIconButton />;
};

export const Delete: ComponentStory<typeof Button> = () => {
  return <DeleteIconButton />;
};
