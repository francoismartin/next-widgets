import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { TabItem, TabPane } from './Tab';

export default {
  title: 'Widgets/TabPane',
  component: TabPane,
} as ComponentMeta<typeof TabPane>;

const id = 'id';
const label = 'label';
const model: TabItem[] = [
  { id: 'hello', label: 'Hello', content: 'Hello' },
  {
    id: 'world',
    label: '<b>No HTML support here!</b>',
    content: <u>World</u>,
  },
  {
    id: 'linea',
    label: 'Linea',
    content: <hr />,
  },
];

const Template: ComponentStory<typeof TabPane> = (args) => (
  <TabPane {...args} id={id} label={label} model={model} />
);

export const Default = Template.bind({});
Default.args = {};

export const WithPanelBoxProps = Template.bind({});
WithPanelBoxProps.args = { panelBoxProps: { sx: { backgroundColor: 'red' } } };

export const WithSelection = Template.bind({});
WithSelection.args = { selected: 'linea' };
