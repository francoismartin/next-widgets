import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { Markdown } from './Markdown';

export default {
  title: 'Widgets/Markdown',
  component: Markdown,
} as ComponentMeta<typeof Markdown>;

const Template: ComponentStory<typeof Markdown> = (args) => (
  <Markdown {...args} />
);

export const Bold = Template.bind({});
Bold.args = { text: 'With **bold** and __also bold__ text' };

export const Italic = Template.bind({});
Italic.args = { text: 'With *italic* and _also italic_ text' };

export const CodeInline = Template.bind({});
CodeInline.args = { text: 'With code `console.log("Hello World!")` inline' };

export const CodeBlock = Template.bind({});
CodeBlock.args = {
  text: 'With code, as block:\n```ts\nexport const CodeInline = Template.bind({});\nCodeInline.args = { text: \'With code `console.log("Hello World!")` inline\' };\n```\nWith some text afterwards.',
};

export const Link = Template.bind({});
Link.args = { text: 'With [link](https://google.com) in text' };

export const LinkOpensInNewTab = Template.bind({});
LinkOpensInNewTab.args = {
  text: 'With [link](https://google.com) in text, [opening](https://google.com) in a new tab',
  openLinksInNewTab: true,
};

export const UnorderedList = Template.bind({});
UnorderedList.args = { text: '* Item 1\n* Item 2' };

export const OrderedList = Template.bind({});
OrderedList.args = {
  text: '1. Item 1\n1. Item 2',
};

export const OneSingleUnderscore = Template.bind({});
OneSingleUnderscore.args = {
  text: 'Hello A_gain',
  options: { sanitize: true },
};
