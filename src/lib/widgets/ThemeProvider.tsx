import React, { ComponentPropsWithoutRef, ReactElement } from 'react';

import {
  createTheme,
  ThemeProvider as MuiThemeProvider,
  StyledEngineProvider,
} from '@mui/material/styles';
import { CssBaseline } from '@mui/material';
import { Optional } from 'utility-types';

export enum GlobalClass {
  selectable = 'selectable',
}

const baseTheme = createTheme();

baseTheme.components = {
  MuiTooltip: {
    styleOverrides: {
      tooltip: {
        fontSize: '0.825rem',
      },
    },
  },
  MuiTypography: {
    styleOverrides: {
      subtitle1: {
        fontWeight: 'bold',
        lineHeight: 'inherit',
      },
    },
  },
  MuiAccordionSummary: {
    styleOverrides: {
      root: {
        paddingLeft: baseTheme.spacing(4.5),
        paddingRight: baseTheme.spacing(4.5),
      },
    },
  },
  MuiCssBaseline: {
    styleOverrides: {
      ['.' + GlobalClass.selectable]: {
        cursor: 'pointer',
      },
    },
  },
};

export const ThemeProvider = ({
  children,
  theme,
}: Optional<
  ComponentPropsWithoutRef<typeof MuiThemeProvider>,
  'theme'
>): ReactElement => (
  <StyledEngineProvider injectFirst>
    <MuiThemeProvider theme={theme ?? baseTheme}>
      <CssBaseline />
      {children}
    </MuiThemeProvider>
  </StyledEngineProvider>
);
