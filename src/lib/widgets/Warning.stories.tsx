import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Warning } from './Warning';

export default {
  title: 'Widgets/Warning',
  component: Warning,
} as ComponentMeta<typeof Warning>;

const Template: ComponentStory<typeof Warning> = (args) => {
  return <Warning {...args} />;
};

export const Text = Template.bind({});
Text.args = {
  tooltip: 'This might explode!',
};
