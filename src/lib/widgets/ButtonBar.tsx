import { styled } from '@mui/material/styles';
import { Z_INDEX_TOP_MOST } from '../utils';

export const ButtonBar = styled('div')(() => ({
  top: 'auto',
  right: 20,
  bottom: 20,
  left: 'auto',
  position: 'fixed',
  color: 'red',
  marginRight: 10,
  zIndex: Z_INDEX_TOP_MOST,
}));
