import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { TabItem, TabPane } from './Tab';
import { Markdown } from './Markdown';

const model: TabItem[] = [
  { id: 'one', label: 'One', content: 'First Panel' },
  { id: 'two', label: 'Two', content: <b>Second Panel</b> },
  {
    id: 'three',
    label: 'Three',
    content: <Markdown text={'Third Panel'} />,
  },
];
const tabPaneId = 'counter';

describe('Tab', () => {
  describe('Component', () => {
    it('should display tab pane with first tab selected', () => {
      render(<TabPane model={model} id={tabPaneId} label={tabPaneId} />);
      screen.getByLabelText(tabPaneId);
      screen.getByText('First Panel');
      // Label is visible...
      screen.getByText('Three');
      // ...but its content is NOT!
      expect(screen.queryByText('Third Panel')).not.toBeInTheDocument();
    });

    it.each`
      selected     | visiblePanel
      ${'one'}     | ${'First Panel'}
      ${'two'}     | ${'Second Panel'}
      ${'three'}   | ${'Third Panel'}
      ${'unknown'} | ${'First Panel'}
    `(
      'should display the selected panel when given tab ID is found',
      async ({ selected, visiblePanel }) => {
        render(
          <TabPane
            model={model}
            id={tabPaneId}
            label={tabPaneId}
            selected={selected}
          />
        );
        screen.getByText(visiblePanel);
      }
    );
  });
});
