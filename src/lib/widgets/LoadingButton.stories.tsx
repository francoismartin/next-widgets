import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { LoadingButton } from './LoadingButton';

export default {
  title: 'Widgets/LoadingButton',
  component: LoadingButton,
} as ComponentMeta<typeof LoadingButton>;

const Template: ComponentStory<typeof LoadingButton> = (args) => (
  <LoadingButton color={'primary'} {...args}>
    Submit
  </LoadingButton>
);

export const Primary = Template.bind({});
Primary.args = {};

export const PrimaryLoading = Template.bind({});
PrimaryLoading.args = {
  ...Primary.args,
  loading: true,
};

export const Secondary = Template.bind({});
Secondary.args = { color: 'secondary' };

export const SecondaryLoading = Template.bind({});
SecondaryLoading.args = { ...Secondary.args, loading: true };

export const Small = Template.bind({});
Small.args = { size: 'small' };

export const SmallLoading = Template.bind({});
SmallLoading.args = { ...Small.args, loading: true };

export const Large = Template.bind({});
Large.args = { size: 'large' };

export const LargeLoading = Template.bind({});
LargeLoading.args = { ...Large.args, loading: true };

export const Outlined = Template.bind({});
Outlined.args = { variant: 'outlined' };

export const OutlinedLoading = Template.bind({});
OutlinedLoading.args = { ...Outlined.args, loading: true };

export const Contained = Template.bind({});
Contained.args = { variant: 'contained' };

export const ContainedLoading = Template.bind({});
ContainedLoading.args = { ...Contained.args, loading: true };
