import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Dialog, CancelLabel } from './Dialog';

const title = 'Dialog Test';
const content = 'Dialog Content';

describe('Dialog', function () {
  describe('Component', function () {
    it('should render the component without errors or warnings', async () => {
      render(
        <Dialog
          open={true}
          isSubmitting={false} // read-only dialog
          fullWidth={true}
          maxWidth={'lg'}
          title={title}
          cancelLabel={CancelLabel.CLOSE}
        >
          {content}
        </Dialog>
      );

      await screen.findByText(title);
      await screen.findByText(content);
    });
  });
});
