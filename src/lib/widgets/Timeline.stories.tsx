import React from 'react';
import { ComponentMeta, ComponentStory, Meta } from '@storybook/react';
import { Timeline, TimelineElement } from './Timeline';
import { ThemeProvider } from './ThemeProvider';
import { $Values } from 'utility-types';
import { Unpacked } from '../../types';

export default {
  title: 'Widgets/Timeline',
  component: Timeline,
} as ComponentMeta<typeof Timeline>;

const Template: ComponentStory<typeof Timeline> = (args) => (
  <Timeline {...args} />
);

type Decorator = Unpacked<$Values<Pick<Meta, 'decorators'>>>;

const themeProviderDecorator: Decorator = (Story) => (
  <ThemeProvider>
    <Story />
  </ThemeProvider>
);

const longMessageDate = new Date(1900, 0, 1);

const longMessage = (): TimelineElement => {
  const date = new Date(longMessageDate.getTime());
  date.setDate(date.getDate() + 1);
  longMessageDate.setDate(date.getDate());
  return {
    icon: 'INFO',
    date,
    title: 'Lorem Ipsum',
    message:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor ' +
      'incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ' +
      'exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure ' +
      'dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ' +
      'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt ' +
      'mollit anim id est laborum.',
  };
};

const shortMessageDate = new Date(2021, 8, 21);

const shortMessage = (): TimelineElement => {
  const date = new Date(shortMessageDate.getTime());
  date.setDate(date.getDate() + 1);
  shortMessageDate.setDate(date.getDate());
  return {
    icon: 'WARN',
    date,
    title: 'Winter is coming',
    message: 'Brace yourselves',
  };
};

const noTitleMessage: TimelineElement = {
  icon: 'WARN',
  date: new Date(2021, 8, 21),
  message: 'There is no title here. Only a message.',
};

const smallerMessage: TimelineElement = {
  icon: 'WARN',
  date: new Date(2021, 8, 21),
  title: 'Smaller Message',
  message:
    "Title has variant 'body1' and message 'body2'. Additionally, color is 'textSecondary'.",
  titleProps: { variant: 'body1' },
  messageProps: { color: 'textSecondary', variant: 'body2' },
};

const styledMessage: TimelineElement = {
  icon: 'WARN',
  date: new Date(2021, 8, 21),
  title: 'Styled Message',
  titleProps: { style: { fontWeight: 600 } },
  message: 'Title is bold',
};

export const ShortMessages = Template.bind({});
ShortMessages.args = {
  elements: [shortMessage(), shortMessage(), shortMessage()],
};

export const LongMessages = Template.bind({});
LongMessages.args = { elements: [longMessage(), longMessage(), longMessage()] };

export const MixedLengthMessages = Template.bind({});
MixedLengthMessages.args = {
  elements: [
    shortMessage(),
    longMessage(),
    noTitleMessage,
    smallerMessage,
    styledMessage,
    shortMessage(),
    longMessage(),
  ],
};

export const ThemedTimeline = Template.bind({});
ThemedTimeline.args = {
  ...MixedLengthMessages.args,
};
ThemedTimeline.decorators = [themeProviderDecorator];
