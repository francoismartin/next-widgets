import React, { ReactElement } from 'react';

export type ExternalLinkProps = {
  to: string;
  sameTab?: boolean;
} & React.ComponentPropsWithoutRef<'a'>;

export const ExternalLink = ({
  to,
  sameTab,
  children,
  ...props
}: ExternalLinkProps): ReactElement => {
  return (
    <a
      href={to}
      target={sameTab ? undefined : '_blank'}
      rel="noopener noreferrer"
      {...props}
    >
      {children}
    </a>
  );
};
