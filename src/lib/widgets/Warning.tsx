import React from 'react';
import { Tooltip } from './Tooltip';
import { WarningIcon } from './icons';

type WarningProps = {
  tooltip: string | null;
  className?: string;
};

export const Warning = React.forwardRef<SVGSVGElement, WarningProps>(
  ({ tooltip, ...props }: WarningProps, ref) => {
    if (tooltip === null) {
      return null;
    } else {
      return (
        <Tooltip title={tooltip}>
          <WarningIcon
            {...props}
            ref={ref}
            fontSize="large"
            sx={{ color: 'rgb(255, 180, 0)', paddingLeft: 2 }}
          />
        </Tooltip>
      );
    }
  }
);
Warning.displayName = 'Warning';
