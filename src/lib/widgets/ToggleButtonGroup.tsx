import { styled } from '@mui/material/styles';
import { default as MuiToggleButtonGroup } from '@mui/material/ToggleButtonGroup';

export const ToggleButtonGroup = styled(MuiToggleButtonGroup)(() => ({
  marginRight: '2rem',
}));
