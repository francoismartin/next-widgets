import React, { ReactElement, useMemo } from 'react';
import { marked } from 'marked';
import MarkedOptions = marked.MarkedOptions;

export type MarkdownProps = {
  text: string;
  openLinksInNewTab?: boolean;
  options?: MarkedOptions;
};

/**
 * NOTE: {@code text} MUST be sanitized and trusted, no input normalization will be performed!
 * Renders {@code text} as markdown.
 * @constructor
 */
export const Markdown = ({
  text,
  openLinksInNewTab,
}: MarkdownProps): ReactElement => {
  const renderedMarkdown = useMemo(() => {
    if (!text) {
      return '';
    }
    const html = marked.parse(text);
    if (openLinksInNewTab) {
      return html.replace(
        new RegExp(/<a href="/g),
        '<a target="_blank" href="'
      );
    }
    return html;
  }, [text, openLinksInNewTab]);
  return <span dangerouslySetInnerHTML={{ __html: renderedMarkdown }} />;
};
