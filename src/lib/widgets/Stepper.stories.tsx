import React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { Step, Stepper } from './Stepper';
import { Typography } from '@mui/material';

const steps = [
  { location: 'Step 1', text: 'First step' },
  { location: 'Step 2', text: 'Second Step' },
  { location: 'Step 3' },
];

export default {
  title: 'Widgets/Stepper',
  component: Stepper,
} as ComponentMeta<typeof Stepper>;

const Template: ComponentStory<typeof Stepper> = (args) => (
  <Stepper {...args}>
    {steps.map((step) => (
      <Step key={'step-' + step.location}>
        <div>
          <Typography variant={'subtitle1'}>{step.location}</Typography>
          <Typography variant="body2" display="inline">
            {step.text}
          </Typography>
        </div>
      </Step>
    ))}
  </Stepper>
);

export const NoActiveStep = Template.bind({});
NoActiveStep.args = { activeStep: -1 };

export const FirstStepActive = Template.bind({});
FirstStepActive.args = { activeStep: 0 };

export const SecondStepActive = Template.bind({});
SecondStepActive.args = { activeStep: 1 };

export const ThirdStepActive = Template.bind({});
ThirdStepActive.args = { activeStep: 2 };

export const ThirdStepCompleted = Template.bind({});
ThirdStepCompleted.args = { activeStep: 3 };
