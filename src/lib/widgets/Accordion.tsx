import React, { ReactElement } from 'react';
import MuiAccordion, {
  AccordionProps as MuiAccordionProps,
} from '@mui/material/Accordion';
import {
  AccordionDetails as MuiAccordionDetails,
  AccordionSummary as MuiAccordionSummary,
  Typography,
} from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

export type AccordionProps = {
  caption: ReactElement | string;
  firstChild: boolean;
} & MuiAccordionProps;

export function Accordion({
  caption,
  children,
  firstChild,
  ...other
}: AccordionProps): ReactElement {
  return (
    <MuiAccordion
      {...other}
      sx={{
        boxShadow: 'none',
      }}
    >
      <MuiAccordionSummary
        expandIcon={<ExpandMoreIcon />}
        sx={[
          {
            border: 'solid',
            borderColor: 'divider',
            borderWidth: '0px 1px 1px 1px',
            '&.Mui-expanded': {
              borderWidth: '1px 1px 1px 1px',
            },
          },
          firstChild && {
            borderWidth: '1px 1px 1px 1px',
          },
        ]}
      >
        <Typography variant="h6">{caption}</Typography>
      </MuiAccordionSummary>
      <MuiAccordionDetails
        sx={{
          border: 'solid',
          borderColor: 'divider',
          borderWidth: '0px 1px 1px 1px',
        }}
      >
        {children}
      </MuiAccordionDetails>
    </MuiAccordion>
  );
}
