import React, { ReactElement, useCallback } from 'react';
import Snackbar from '@mui/material/Snackbar';
import { useDispatch, useSelector } from 'react-redux';
import { Alert } from '@mui/material';
import { clearToast, ToastSeverity } from '../reducers';
import { mailto } from '../utils';
import { SnackbarCloseReason } from '@mui/material/Snackbar/Snackbar';

export type InternalErrorContentProps = {
  correlationId: string;
} & ToastBarProps;

export const InternalErrorContent = ({
  correlationId,
  subjectPrefix,
  contactEmail,
}: InternalErrorContentProps): ReactElement => {
  return (
    <span>
      An unexpected error occurred. Please{' '}
      <a
        href={mailto(
          contactEmail ?? '',
          `${subjectPrefix}Unexpected Error`,
          `Correlation ID: ${correlationId}

Reproduction steps:
(please explain step by step what you did before the error happened)

Expected behavior:
(please explain what you would expect to happen if everything is working correctly)

Actual behavior:
(please explain what happened instead of what you expected)`
        )}
      >
        contact us
      </a>{' '}
      about this issue and refer to this ID: {correlationId}
    </span>
  );
};

type ToastBarProps = {
  subjectPrefix: string;
  contactEmail: string;
};

export function ToastBar({
  subjectPrefix,
  contactEmail,
}: ToastBarProps): ReactElement {
  const dispatch = useDispatch();

  const message = useSelector((state) => state.toast.message);
  const severity = useSelector((state) => state.toast.severity);
  const correlationId = useSelector((state) => state.toast.correlationId);

  const handleSnackBarClose = useCallback(
    (_event?: React.SyntheticEvent | Event, reason?: SnackbarCloseReason) => {
      if (reason === 'clickaway' && severity === ToastSeverity.ERROR) {
        return;
      }
      dispatch(clearToast());
    },
    [severity, dispatch]
  );

  return (
    <Snackbar
      open={message !== undefined || correlationId !== undefined}
      onClose={handleSnackBarClose}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      autoHideDuration={severity === ToastSeverity.ERROR ? null : 6000}
    >
      {severity && (
        <Alert
          onClose={
            severity === ToastSeverity.ERROR ? handleSnackBarClose : undefined
          }
          elevation={6}
          variant="filled"
          severity={severity}
          sx={{
            '& a': {
              color: 'white',
            },
          }}
        >
          {correlationId ? (
            <InternalErrorContent
              correlationId={correlationId}
              subjectPrefix={subjectPrefix}
              contactEmail={contactEmail}
            />
          ) : (
            <span>{message}</span>
          )}
        </Alert>
      )}
    </Snackbar>
  );
}
