import React, { ReactElement, ReactNode } from 'react';
import MuiTimeline from '@mui/lab/Timeline';
import MuiTimelineItem from '@mui/lab/TimelineItem';
import MuiTimelineSeparator from '@mui/lab/TimelineSeparator';
import MuiTimelineConnector from '@mui/lab/TimelineConnector';
import MuiTimelineContent from '@mui/lab/TimelineContent';
import MuiTimelineOppositeContent from '@mui/lab/TimelineOppositeContent';
import MuiTimelineDot from '@mui/lab/TimelineDot';
import { formatDate, formatTimestamp } from '../utils';
import { timelineIcons } from './icons';
import { $Keys } from 'utility-types';
import { TestId } from '../testId';
import { red } from './Colors';
import { Typography, TypographyProps } from '@mui/material';

export type TimelineElement = {
  icon: $Keys<typeof timelineIcons>;
  date?: Date;
  title?: ReactNode;
  message?: ReactNode;
  titleProps?: Omit<TypographyProps, 'ref'>;
  messageProps?: Omit<TypographyProps, 'ref'>;
};

const iconColor = red['A700'];

export type TimelineProps = {
  elements: TimelineElement[];
} & React.ComponentPropsWithoutRef<typeof MuiTimeline>;

export function Timeline({
  elements,
  ...timelineProps
}: TimelineProps): ReactElement {
  return (
    <MuiTimeline {...timelineProps}>
      {elements.map((element: TimelineElement, index: number) => (
        <MuiTimelineItem
          key={`timeline-item-${formatTimestamp(element.date, true)}-${
            element.title
          }-${element.message}`}
        >
          <MuiTimelineOppositeContent
            sx={{
              paddingTop: 1,
              // prevent wasting space on the left
              maxWidth: 'max-content',
              minWidth: 'max-content',
            }}
          ></MuiTimelineOppositeContent>

          <MuiTimelineSeparator sx={{ paddingBottom: 1 }}>
            <MuiTimelineDot
              variant="outlined"
              sx={{
                marginTop: 0,
                color: iconColor,
                borderColor: iconColor,
                marginBottom: 1,
              }}
              data-testid={TestId.TIMELINE_DOT_PREFIX + element.icon}
            >
              {timelineIcons[element.icon]}
            </MuiTimelineDot>
            {index < elements.length - 1 && (
              <MuiTimelineConnector data-testid={TestId.TIMELINE_CONNECTOR} />
            )}
          </MuiTimelineSeparator>
          <MuiTimelineContent sx={{ paddingTop: 0.5, paddingBottom: 3 }}>
            {element.date && (
              <Typography variant="body2" color="textSecondary">
                {formatDate(element.date)}
              </Typography>
            )}
            {element.title && (
              <Typography variant="h6" component="h1" {...element.titleProps}>
                {element.title}
              </Typography>
            )}
            {element.message && (
              <Typography
                sx={element.title ? undefined : { paddingTop: 0.5 }}
                {...element.messageProps}
              >
                {element.message}
              </Typography>
            )}
          </MuiTimelineContent>
        </MuiTimelineItem>
      ))}
    </MuiTimeline>
  );
}
