import React, { ReactNode } from 'react';

import InfoIcon from '@mui/icons-material/Info';
import WarningIcon from '@mui/icons-material/Warning';

export { default as LogoutIcon } from '@mui/icons-material/PowerSettingsNew';
export { default as HomeIcon } from '@mui/icons-material/Home';
export { default as ProjectIcon } from '@mui/icons-material/GridOn';
export { default as FeedbackIcon } from '@mui/icons-material/Feedback';
export { default as KeyIcon } from '@mui/icons-material/VpnKey';
export { default as MessageIcon } from '@mui/icons-material/Message';
export { default as WorkIcon } from '@mui/icons-material/Work';
export { default as WarningIcon } from '@mui/icons-material/Warning';
export { default as FaceIcon } from '@mui/icons-material/Face';
export { default as PeopleIcon } from '@mui/icons-material/People';
export { default as AddIcon } from '@mui/icons-material/Add';
export { default as SearchIcon } from '@mui/icons-material/Search';
export { default as CreateIcon } from '@mui/icons-material/Create';
export { default as DeleteIcon } from '@mui/icons-material/Delete';
export { default as CheckIcon } from '@mui/icons-material/Check';
export { default as DownloadIcon } from '@mui/icons-material/GetApp';
export { default as ArchiveIcon } from '@mui/icons-material/Archive';
export { default as UnarchiveIcon } from '@mui/icons-material/Unarchive';
export { default as MarkEmailReadIcon } from '@mui/icons-material/MarkEmailRead';
export { default as MarkEmailUnreadIcon } from '@mui/icons-material/MarkEmailUnread';

export const timelineIcons: Record<string, ReactNode> = {
  INFO: <InfoIcon />,
  WARN: <WarningIcon />,
};
