import React, { ReactElement, ReactNode } from 'react';
import { mailto } from '../utils';

export type EmailLinkProps = {
  email: string;
  subject?: string;
  body?: string;
  children?: ReactNode;
};

export const EmailLink = ({
  email,
  subject,
  body,
  children,
}: EmailLinkProps): ReactElement => {
  return <a href={mailto(email, subject, body)}>{children ?? email}</a>;
};
