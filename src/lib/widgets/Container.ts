import { styled } from '@mui/material/styles';

import { Container as MuiContainer } from '@mui/material';

export const Container = styled(MuiContainer)(() => ({
  fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
  flexGrow: 1,
}));
