import React, { ReactElement, ReactNode, useCallback, useState } from 'react';
import {
  default as MaterialUiDialog,
  DialogProps as MaterialUiDialogProps,
} from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import { ButtonProps, Typography } from '@mui/material';
import { LoadingButton } from './LoadingButton';

export function useDialogState<S>(): {
  item: S | null;
  setItem: (newItem: S) => void;
  onClose: () => void;
  open: boolean;
} {
  const [item, setItem] = useState<S | null>(null);
  const [open, setOpen] = useState<boolean>(false);
  const setItemCopy = useCallback((newItem: S) => {
    setItem(newItem);
    setOpen(true);
  }, []);

  const onClose = useCallback(() => {
    setOpen(false);
  }, []);

  return { item, setItem: setItemCopy, onClose, open };
}

export type DialogProps = {
  title: string | React.ReactNode;
  text?: string | React.ReactNode;
  confirmLabel?: string;
  cancelLabel?: string;
  confirmButtonProps?: ButtonProps;
  onClose?: () => void;
  isSubmitting: boolean;
  children?: React.ReactNode;
} & Omit<MaterialUiDialogProps, 'title'>;

export enum ConfirmLabel {
  CONFIRM = 'Confirm',
  SAVE = 'Save',
}

export enum CancelLabel {
  CANCEL = 'Cancel',
  CLOSE = 'Close',
}

type DialogSubtitleProps = { children: ReactNode };

export const DialogSubtitle = ({
  children,
}: DialogSubtitleProps): ReactElement => {
  return (
    <DialogContentText
      sx={{
        color: 'common.black',
        paddingTop: 2,
      }}
    >
      {children}
    </DialogContentText>
  );
};

export const Dialog = ({
  title,
  text = undefined,
  confirmLabel = ConfirmLabel.CONFIRM,
  cancelLabel = CancelLabel.CANCEL,
  children,
  isSubmitting,
  confirmButtonProps,
  onClose,
  ...other
}: DialogProps): ReactElement => (
  <MaterialUiDialog
    aria-labelledby="dialog-title"
    aria-describedby="dialog-description"
    onClose={onClose}
    {...other}
  >
    <DialogTitle id="dialog-title">
      <Typography variant="h5" component="span">
        {title}
      </Typography>
    </DialogTitle>
    <DialogContent>
      {text && (
        <DialogContentText id="dialog-description">{text}</DialogContentText>
      )}
      {children}
    </DialogContent>
    <DialogActions sx={{ margin: 1 }}>
      {onClose && (
        <Button color="primary" onClick={onClose}>
          {cancelLabel}
        </Button>
      )}
      {confirmButtonProps && (
        <LoadingButton
          sx={{ marginLeft: 1 }}
          type="submit"
          form="dialog-form"
          color="primary"
          variant="outlined"
          loading={isSubmitting}
          {...confirmButtonProps}
        >
          {confirmLabel}
        </LoadingButton>
      )}
    </DialogActions>
  </MaterialUiDialog>
);
