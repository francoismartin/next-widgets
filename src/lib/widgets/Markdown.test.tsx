import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { Markdown } from './Markdown';

describe('Markdown', () => {
  describe('Component', () => {
    const text = 'Google **is** a search engine.';
    const linkText =
      '[Google](https://www.google.com) is a [search engine](https://www.google.com).';

    it.each`
      openLinksInNewTab | text
      ${true}           | ${false}
      ${true}           | ${0}
      ${true}           | ${-0}
      ${true}           | ${null}
      ${true}           | ${undefined}
      ${true}           | ${NaN}
      ${false}          | ${false}
      ${false}          | ${0}
      ${false}          | ${-0}
      ${false}          | ${null}
      ${false}          | ${undefined}
      ${false}          | ${NaN}
    `(
      'should render `text` as `$text` as an empty string, with `openLinksInNewTab` set to $openLinksInNewTab',
      async ({ openLinksInNewTab, text }) => {
        // when
        render(<Markdown text={text} openLinksInNewTab={openLinksInNewTab} />);

        // then verify no error or warning is logged
        const elements = await screen.findAllByText('');
        expect(elements.length).toBeGreaterThanOrEqual(1);
        expect(screen.queryByText(String(text))).not.toBeInTheDocument();
      }
    );

    it.each`
      openLinksInNewTab
      ${true}
      ${false}
    `(
      'should render "is" as bold text instead of showing the raw markdown syntax "**is**", with `openLinksInNewTab` set to $openLinksInNewTab',
      async ({ openLinksInNewTab }) => {
        // when
        render(<Markdown text={text} openLinksInNewTab={openLinksInNewTab} />);

        // then
        await screen.findByText('is');
        expect(screen.queryByText('**is**')).not.toBeInTheDocument();
      }
    );

    async function assertLinksOpenIn(newTab: boolean) {
      const expectedTarget = newTab ? '_blank' : '';

      const link1 = (await screen.findByText('Google')) as HTMLAnchorElement;
      expect(link1.target).toEqual(expectedTarget);

      const link2 = (await screen.findByText(
        'search engine'
      )) as HTMLAnchorElement;
      expect(link2.target).toEqual(expectedTarget);
    }

    it('should render links and open them in the same tab by default', async () => {
      // when
      render(<Markdown text={linkText} />);

      // then
      await assertLinksOpenIn(false);
    });

    it('should render links and open them in a new tab if `openLinksInNewTab` is `true`', async () => {
      // when
      render(<Markdown text={linkText} openLinksInNewTab />);

      // then
      await assertLinksOpenIn(true);
    });
  });
});
