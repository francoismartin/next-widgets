import { MockServer, RequestVerifier, setupMockApi } from './testUtils';
import { rest } from 'msw';
import { StatusCodes } from 'http-status-codes';

describe('testUtils', () => {
  describe('RequestVerifier', () => {
    const url = 'http://localhost/test';

    let server: MockServer;

    beforeAll(() => {
      server = setupMockApi(
        rest.get(url, (_req, res, ctx) => res(ctx.status(StatusCodes.OK)))
      );
    });

    afterEach(() => {
      server.resetHandlers();
    });

    afterAll(() => {
      server.close();
    });

    it('should verify the requests', async () => {
      const verifier = new RequestVerifier(server);
      verifier.assertCount(0);
      await fetch(url);
      verifier.assertCount(1);
      verifier.assertCalled('/does-not-exist', 0);
      verifier.assertCalled(url, 1);
    });
  });
});
