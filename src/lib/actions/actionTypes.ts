import { Action } from 'redux';
import { ObjectOf } from '../../types';

type ApiFunction<RequestParams extends ObjectOf<RequestParams>, ResponseType> =
  | ((requestParams: RequestParams) => ResponseType)
  | (() => ResponseType)
  | ((requestParams: RequestParams) => void);

export type ActionType<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
  Name extends string = string
> = {
  request: `${Name}_REQUEST`;
  success: `${Name}_SUCCESS`;
  failure: `${Name}_FAILURE`;
  apiFunction: ApiFunction<RequestParams, ResponseType>;
};

export type AnyActionType = ActionType<never, unknown>;

export function declareAction<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
  Name extends string = string
>(
  name: Name,
  apiFunction: ApiFunction<RequestParams, ResponseType>
): ActionType<RequestParams, Awaited<ResponseType>, typeof name> {
  return {
    request: `${name}_REQUEST` as const,
    success: `${name}_SUCCESS` as const,
    failure: `${name}_FAILURE` as const,
    apiFunction,
  };
}

export const requestAction = <
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType,
  OnSuccessAction extends Action = Action,
  Name extends string = string
>(
  actionType: ActionType<RequestParams, ResponseType, Name>,
  requestParams: RequestParams = {} as RequestParams,
  onSuccess?: OnSuccessAction
): Action<typeof actionType.request> & RequestParams => ({
  type: actionType.request,
  ...requestParams,
  onSuccess,
});
