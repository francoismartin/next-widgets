import {
  call,
  ForkEffect,
  put,
  takeLatest,
  takeLeading,
} from 'redux-saga/effects';
import { AnyAction } from 'redux';
import { Environment, redirectExternal } from '../api';
import { ActionType, AnyActionType } from './actionTypes';
import { logger } from '../logger';
import {
  getCorrelationId,
  isError,
  isResponse,
  serializeFetch,
  serializeResponse,
} from '../utils';
import { serializeError } from 'serialize-error';
import { StatusCodes } from 'http-status-codes';
import { setToast, ToastSeverity } from '../reducers';
import { ObjectOf } from '../../types';

const log = logger('Sagas');

function takeApi(
  actionType: AnyActionType,
  apiObject: unknown,
  loginUrl: string,
  latest: boolean,
  handledErrors?: StatusCodes[]
): ForkEffect<never> {
  const boundApi = actionType.apiFunction.bind(apiObject);

  function* saga(action: AnyAction) {
    try {
      // TODO improve typing
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const result = yield call(boundApi, action);
      const resultAction = {
        type: actionType.success,
        requestArgs: action,
        response: result,
      };
      yield put(resultAction);
      if (action.onSuccess !== undefined) {
        yield put(action.onSuccess);
      }
    } catch (e) {
      const errorAction = { type: actionType.failure, error: e };
      if (isResponse(e) && e?.status) {
        if (!handledErrors?.includes(e?.status)) {
          yield put(errorAction);
          yield handleError(e, actionType, loginUrl);
        } else {
          // In case of handled errors, we serialize the response
          // TODO improve typing
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          const serializedError = yield call(serializeResponse, e);
          yield put({ type: actionType.failure, error: serializedError });
        }
      } else {
        yield put(errorAction);
      }
    }
  }

  if (latest) {
    return takeLatest(actionType.request, saga);
  } else {
    return takeLeading(actionType.request, saga);
  }
}

type TakeApiFunction = (
  actionType: AnyActionType,
  handledErrors?: StatusCodes[]
) => ForkEffect<never>;

export type TakeApi = {
  latest: TakeApiFunction;
  leading: TakeApiFunction;
};

/**
 * Creates an object which gives access to the equivalent of {@link takeLatest}
 * and {@link takeLeading} method equivalents to perform API calls.
 *
 * @param apiObject API object on which the functions passed in as `apiFunction`
 *                  will be called.
 * @param loginUrl to redirect to if the user is not logged in when making a
 *                 request.
 */
export const takeApiFactory = (
  apiObject: unknown,
  loginUrl: string
): TakeApi => ({
  latest: (actionType: AnyActionType, handledErrors?: StatusCodes[]) =>
    takeApi(actionType, apiObject, loginUrl, true, handledErrors),
  leading: (actionType: AnyActionType, handledErrors?: StatusCodes[]) =>
    takeApi(actionType, apiObject, loginUrl, false, handledErrors),
});

function* handleError<
  RequestParams extends ObjectOf<RequestParams>,
  ResponseType
>(
  e: unknown,
  actionType: ActionType<RequestParams, ResponseType>,
  loginUrl: string
) {
  const errorId = getCorrelationId(e);
  const errorLog = {
    action: actionType.failure,
    json: {},
    correlationId: errorId,
    message: 'Saga threw an error.',
  };
  const errorAction = setToast({
    severity: ToastSeverity.ERROR,
    correlationId: errorId,
  });

  if (process.env.NODE_ENV === Environment.TEST) {
    // don't swallow errors during testing
    throw e;
  } else if (isError(e)) {
    errorLog.json = serializeError(e);
    log.error(errorLog);
    yield put(errorAction);
  } else if (isResponse(e) && isSessionExpired(e)) {
    errorAction.severity = ToastSeverity.WARNING;
    errorAction.message = 'Please wait until we log you back in...';
    yield put(errorAction);
    redirectExternal(loginUrl);
    // In case of `StatusCodes.CONFLICT` we NEVER log the error
  } else if (isResponse(e) && e.status !== StatusCodes.CONFLICT) {
    serializeFetch(e, (res) => {
      errorLog.json = res;
      log.error(errorLog);
    });
    yield put(errorAction);
  } else {
    // wrap into object in case e is not an object to avoid deserialization errors
    errorLog.json = { e };
    log.error(errorLog);
    yield put(errorAction);
  }
}

function isSessionExpired(response: Response) {
  return response.status === StatusCodes.FORBIDDEN;
}

// Cannot be typed, see https://github.com/Microsoft/TypeScript/issues/2983#issuecomment-230404301
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function* redirect({ destination }: AnyAction) {
  yield call(redirectExternal, destination);
}
