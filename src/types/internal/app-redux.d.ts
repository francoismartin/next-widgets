import 'react-redux';
import { reducers } from '../lib/reducers/reducers';

/**
 * Uses TypeScript's module augmentation to re-define the type of {@code DefaultRootState}.
 *
 * This allows writing code like this:
 * ```typescript
 * useSelector(state => state.user)
 * ```
 * instead of this:
 * ```typescript
 * useSelector<State>(state => state.user)
 * ```
 *
 * @see https://github.com/DefinitelyTyped/DefinitelyTyped/pull/41031
 */
declare module 'react-redux' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultRootState extends ReturnType<typeof reducers> {}
}
