import { CSSProperties } from 'react';
import { Theme } from '@mui/material/styles';

export type StyleMap = Record<string, CSSProperties>;
export type ThemeProps = {
  theme?: Theme;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type AnyFunction = (...args: any[]) => any;

export type IdType = {
  id: number;
};

/**
 * Unpacks an array and returns the type of a single element.
 *
 * ### Example
 *
 * ```ts
 * type Foo = Unpacked<Array<string>>  // type Foo = string
 * type Bar = Unpacked<string[]>       // type Bar = string
 * ```
 */
export type Unpacked<T> = T extends (infer U)[] ? U : T;

// see https://github.com/typescript-eslint/typescript-eslint/issues/2063#issuecomment-632823847
type NoElements<T> = { [P in keyof T]: never };
export type EmptyObject = NoElements<Record<string, never>>;
export type AnyObject = Record<string, unknown>;
export type ObjectOf<T> = { [P in keyof T]: T[P] };

// The opposite of `Readonly<T>`
export type Writeable<T> = { -readonly [P in keyof T]: T[P] };
// The opposite of `DeepReadonly<T>`
export type DeepWriteable<T> = {
  -readonly [P in keyof T]: DeepWriteable<T[P]>;
};

/**
 * NOTE: {@code ReactNode}s MUST have a "key" defined!
 */
export type AdditionalActionButtonFactory<T> = (
  item?: T
) => React.ReactNode | React.ReactNode[];
