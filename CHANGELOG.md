# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [10.0.0](https://gitlab.com/biomedit/next-widgets/compare/9.9.1...10.0.0) (2022-10-20)


### ⚠ BREAKING CHANGES

* **ColoredStatus:** `status.color` allowed values have changed.

### Features

* change colors to theme's primary ([9f65814](https://gitlab.com/biomedit/next-widgets/commit/9f65814278abb66dc3471c79726617657aa91b4b))
* **CheckboxArrayField:** use Mui Checkbox instead of default one ([e81f940](https://gitlab.com/biomedit/next-widgets/commit/e81f94032a2d8069c1a918f663a521e1e383ce92))
* **ColoredStatus:** turn ColoredStatus into a Chip ([e4c2e7c](https://gitlab.com/biomedit/next-widgets/commit/e4c2e7c201514c2ddf526759e6ff9f459abe2e21))
* **EnhancedTable:** use colored buttons ([ddbaecc](https://gitlab.com/biomedit/next-widgets/commit/ddbaecc190c0aa449c9f5d34211531026bfa5fd3))
* **Header:** allow header to be a link ([dc84baa](https://gitlab.com/biomedit/next-widgets/commit/dc84baabd0d15c9c10e8955657c8f09524ae28d1))
* remove shadows ([222e6d0](https://gitlab.com/biomedit/next-widgets/commit/222e6d07acc5af79e97eae914a29f2795e2c27a9))

### [9.9.1](https://gitlab.com/biomedit/next-widgets/compare/9.9.0...9.9.1) (2022-10-10)

## [9.9.0](https://gitlab.com/biomedit/next-widgets/compare/9.8.0...9.9.0) (2022-10-06)


### Features

* **Tab:** expose `TabItem` ID specification ([1c3a084](https://gitlab.com/biomedit/next-widgets/commit/1c3a0847ccf973cfa1e2b4f865b6621d94d25c81))


### Bug Fixes

* **EnhancedTable:** use IdType instead of IdRequired ([2a0707b](https://gitlab.com/biomedit/next-widgets/commit/2a0707bb99e3349b3b4e727b3177373da2fd6cc7))

## [9.8.0](https://gitlab.com/biomedit/next-widgets/compare/9.7.2...9.8.0) (2022-09-28)


### Features

* **ListPage:** make expanded `Accordion` configurable ([1a14a88](https://gitlab.com/biomedit/next-widgets/commit/1a14a889c321880fa3b095ec65d34020ef91e493))


### Bug Fixes

* **typescript:** compilation errors due to upgrade ([e1bcb2b](https://gitlab.com/biomedit/next-widgets/commit/e1bcb2b7f3f775cece0cf2795b22754c01fae0c2)), closes [#60](https://gitlab.com/biomedit/next-widgets/issues/60)

### [9.7.2](https://gitlab.com/biomedit/next-widgets/compare/9.7.1...9.7.2) (2022-09-19)

### [9.7.1](https://gitlab.com/biomedit/next-widgets/compare/9.7.0...9.7.1) (2022-07-28)

## [9.7.0](https://gitlab.com/biomedit/next-widgets/compare/9.6.0...9.7.0) (2022-07-07)


### Features

* **Buttons:** force FabButtons to a 1/1 ratio ([779ad8b](https://gitlab.com/biomedit/next-widgets/commit/779ad8be62c547692a299921a9abb6a5c6a4f998)), closes [#46](https://gitlab.com/biomedit/next-widgets/issues/46)

## [9.6.0](https://gitlab.com/biomedit/next-widgets/compare/9.5.0...9.6.0) (2022-06-10)


### Features

* **EnhancedTable:** allow to define additional action buttons ([7cc73eb](https://gitlab.com/biomedit/next-widgets/commit/7cc73ebae270e0f7bd5b3bda56ec2f48bdd67188)), closes [#49](https://gitlab.com/biomedit/next-widgets/issues/49)
* **EnhancedTable:** make the hover over text in the project header field names more explanatory ([bd4181f](https://gitlab.com/biomedit/next-widgets/commit/bd4181fece2eac50f649114b056c9ec5f78ae1df)), closes [#50](https://gitlab.com/biomedit/next-widgets/issues/50)

## [9.5.0](https://gitlab.com/biomedit/next-widgets/compare/9.4.0...9.5.0) (2022-05-25)


### Features

* **TabPane:** add panelBoxProps ([c385b25](https://gitlab.com/biomedit/next-widgets/commit/c385b25e582dc9f76281c4ca6e40adf20c6e8f70)), closes [#41](https://gitlab.com/biomedit/next-widgets/issues/41)

## [9.4.0](https://gitlab.com/biomedit/next-widgets/compare/9.3.0...9.4.0) (2022-05-19)


### Features

* **validators:** add pgpFingerprint and mustBe validators ([c059f0e](https://gitlab.com/biomedit/next-widgets/commit/c059f0e5daada9d485ffec376fc46f04fbec2f81))

## [9.3.0](https://gitlab.com/biomedit/next-widgets/compare/9.2.0...9.3.0) (2022-05-10)


### Features

* **ColoredStatus:** `component` as possible `ColoredStatus` argument ([94b40b5](https://gitlab.com/biomedit/next-widgets/commit/94b40b59155fd8ff2a6aa6828ffc79badef7deac))


### Bug Fixes

* **validator:** relax constraints on group name ([b16a31f](https://gitlab.com/biomedit/next-widgets/commit/b16a31ff4a1d1cbdb6a94620523bbc6d3535bec1)), closes [#44](https://gitlab.com/biomedit/next-widgets/issues/44)

## [9.2.0](https://gitlab.com/biomedit/next-widgets/compare/9.1.0...9.2.0) (2022-05-06)


### Features

* **MultilineField:** add MultilineField component ([cb1826d](https://gitlab.com/biomedit/next-widgets/commit/cb1826d2ff30487108dc5c92c7ba59573da2c647))

## [9.1.0](https://gitlab.com/biomedit/next-widgets/compare/9.0.0...9.1.0) (2022-05-06)


### Features

* add `lightgrey` as possible `StatusColor` option ([235a279](https://gitlab.com/biomedit/next-widgets/commit/235a279935b342d71f9ce4cf59ac96df665a3ad8))


### Bug Fixes

* **EnhancedTable:** force action buttons on the same line ([7305061](https://gitlab.com/biomedit/next-widgets/commit/730506123e5b4cbf40668ff6ec9dad9ebb9cfa13))

## [9.0.0](https://gitlab.com/biomedit/next-widgets/compare/8.2.0...9.0.0) (2022-04-26)


### ⚠ BREAKING CHANGES

* **DeleteDialog:** Below are listed the components/functions that present breaking changes.

**DeleteDialog**

- The `deleteItemName` parameter has been renamed to `confirmationText`.
- The `confirmationText` parameter has been renamed to `confirmationHelper`.

**ListBase**

- The `deleteItemName` parameter has been renamed to `deleteConfirmationText`.

**useList & useDeleteDialog**

- The `getItemName` parameter has been renamed to `getDeleteConfirmationText`.
- The `deleteItemName` return value has been renamed to `deleteConfirmationText`.

**ListPage & EnhancedTable**

- The `getItemName` parameter has been renamed to `getDeleteConfirmationText`.

* **DeleteDialog:** change naming of confirmation text related parameters ([22df495](https://gitlab.com/biomedit/next-widgets/commit/22df495e8809d71933f94c316feaa52544171e34))

## [8.2.0](https://gitlab.com/biomedit/next-widgets/compare/8.1.0...8.2.0) (2022-04-22)


### Features

* **buttons:** change delete button color to red ([cc81e82](https://gitlab.com/biomedit/next-widgets/commit/cc81e82f7044d7da6fd6a914291fc405b62eb6ba)), closes [#40](https://gitlab.com/biomedit/next-widgets/issues/40)
* **DeleteDialog:** ask for a safer confirmation text ([911c841](https://gitlab.com/biomedit/next-widgets/commit/911c841c5d2ce5d444de26aeff2702d42f706f39))
* **ListHooks:** expand ListModel typing to accept ReactElements as caption ([16cd3b2](https://gitlab.com/biomedit/next-widgets/commit/16cd3b2b6077c2d035db5957d5b8b361ecbd973a))
* **Stepper:** use theme instead of hardcoded colors ([11268af](https://gitlab.com/biomedit/next-widgets/commit/11268af50607a140492ae858cc79298c8e20d386))

## [8.1.0](https://gitlab.com/biomedit/next-widgets/compare/8.0.3...8.1.0) (2022-03-30)


### Features

* in case of handled error, serialize the error if it's a Response ([38e8e83](https://gitlab.com/biomedit/next-widgets/commit/38e8e838bac64f6667e03dca22822a13dc30343f))

### [8.0.3](https://gitlab.com/biomedit/next-widgets/compare/8.0.2...8.0.3) (2022-03-16)


### Bug Fixes

* **EnhancedTable:** fix canEdit and canDelete check for showActions display column ([ec95768](https://gitlab.com/biomedit/next-widgets/commit/ec95768dc3315144ac87eb5108a6433f8f6380f5)), closes [#32](https://gitlab.com/biomedit/next-widgets/issues/32)

### [8.0.2](https://gitlab.com/biomedit/next-widgets/compare/8.0.1...8.0.2) (2022-03-11)


### Bug Fixes

* replace 'snarkdown' with 'marked' as Markdown renderer ([b168408](https://gitlab.com/biomedit/next-widgets/commit/b168408034d3113edd76a2265662cbea75e05244)), closes [#10](https://gitlab.com/biomedit/next-widgets/issues/10)

### [8.0.1](https://gitlab.com/biomedit/next-widgets/compare/8.0.0...8.0.1) (2022-03-08)


### Bug Fixes

* when retrieving, we are submitting, and NOT fetching ([86dcc03](https://gitlab.com/biomedit/next-widgets/commit/86dcc0334acd4cd1a2dc263fac0c3b0585ef1a8a))

## [8.0.0](https://gitlab.com/biomedit/next-widgets/compare/7.1.0...8.0.0) (2022-03-07)


### ⚠ BREAKING CHANGES

* Import directly from `@mui/material` instead and be
aware of tree-shaking (see https://mui.com/guides/minimizing-bundle-size/)
in development mode.

### Features

* add `retrieve` action type ([265d2e9](https://gitlab.com/biomedit/next-widgets/commit/265d2e9b6216f28f1e8f727c24372aa2582fb9e6))
* remove pure or simple Material UI widgets ([4ed51c8](https://gitlab.com/biomedit/next-widgets/commit/4ed51c81c6821670eed4632d7e09de2ee6149ebf)), closes [#36](https://gitlab.com/biomedit/next-widgets/issues/36)

## [7.1.0](https://gitlab.com/biomedit/next-widgets/compare/7.0.0...7.1.0) (2022-02-25)


### Features

* **form/validations:** make noValidate property optional ([8c56854](https://gitlab.com/biomedit/next-widgets/commit/8c56854820d96d0a423354a95d5f2b461b36e9f5))

## [7.0.0](https://gitlab.com/biomedit/next-widgets/compare/6.11.1...7.0.0) (2022-02-17)


### ⚠ BREAKING CHANGES

* **Checkbox:** `handleChange` of `CheckboxArrayFieldProps` has been renamed to `onChange`.

### Features

* **Dialog:** do not limit confirm/cancel label to enum values ([0fe4619](https://gitlab.com/biomedit/next-widgets/commit/0fe4619506dfb4ea2682c244816050b58b8d5f72))
* **FormDialog:** `title` could be a React node and not only a string ([ffbd6a0](https://gitlab.com/biomedit/next-widgets/commit/ffbd6a0d5062143a89f8482b56c9cdcdd1753141))


### Bug Fixes

* **Form/validations:** disable HTML 5 validations on Form component ([1385048](https://gitlab.com/biomedit/next-widgets/commit/138504830308290cb88789963c8b2e2a37005ee7)), closes [#34](https://gitlab.com/biomedit/next-widgets/issues/34)


* **Checkbox:** remove hacks and workarounds ([e9f5ce4](https://gitlab.com/biomedit/next-widgets/commit/e9f5ce4629ef66154e08e1e9d761f2855a93f190)), closes [#33](https://gitlab.com/biomedit/next-widgets/issues/33)

### [6.11.1](https://gitlab.com/biomedit/next-widgets/compare/6.11.0...6.11.1) (2022-02-02)


### Bug Fixes

* **EnhancedArrayFieldProps:** use `key` instead of `id` for TKeyName ([d5f6b95](https://gitlab.com/biomedit/next-widgets/commit/d5f6b95dea3041505a3e9dee162c55af01435bef))

## [6.11.0](https://gitlab.com/biomedit/next-widgets/compare/6.10.0...6.11.0) (2022-01-19)


### Features

* **AutocompleteField:** add possibility to externally reset to empty choice ([7f88dcf](https://gitlab.com/biomedit/next-widgets/commit/7f88dcf57e09d3c2bdfd9c8bc98a7bb3254fe80c))

## [6.10.0](https://gitlab.com/biomedit/next-widgets/compare/6.9.0...6.10.0) (2021-12-23)


### Features

* **commitizen:** add commitizen to the project ([23869c7](https://gitlab.com/biomedit/next-widgets/commit/23869c719725ebffa6c4031f1d875296bd88ab08))
* **commitlint:** add commitlint pre-commit hook ([2115258](https://gitlab.com/biomedit/next-widgets/commit/21152587e389c533c8a659797b818942a4820f77))


### Bug Fixes

* **husky:** fix husky to restore pre commit hooks ([f8cfe5c](https://gitlab.com/biomedit/next-widgets/commit/f8cfe5c4bbf26ba07b934fe55e9ea16ce321f4b9))

## [6.9.0](https://gitlab.com/biomedit/next-widgets/compare/6.8.1...6.9.0) (2021-12-18)


### Features

* **formatDate:** accept `null` as input value as well ([16c0d33](https://gitlab.com/biomedit/next-widgets/commit/16c0d334b887f68ffb8cf4124a1e9c5cab8378b3))

### [6.8.1](https://gitlab.com/biomedit/next-widgets/compare/6.8.0...6.8.1) (2021-12-17)

## [6.8.0](https://gitlab.com/biomedit/next-widgets/compare/6.7.0...6.8.0) (2021-12-16)


### Features

* **testUtils:** add method `expectAllToBeInTheDocument` to allow passing in an array of elements ([5558e55](https://gitlab.com/biomedit/next-widgets/commit/5558e5596f91a176984eb76a31d498efa7d937d4))

## [6.7.0](https://gitlab.com/biomedit/next-widgets/compare/6.6.1...6.7.0) (2021-12-10)


### Features

* **Buttons:** add buttons for marking email (un)read ([6cb6563](https://gitlab.com/biomedit/next-widgets/commit/6cb6563e98d0778cb748ba8f99b3537e0196848b))

### [6.6.1](https://gitlab.com/biomedit/next-widgets/compare/6.6.0...6.6.1) (2021-12-08)


### Bug Fixes

* **reducerBase:** convert id to string for del and update reducers ([8737e60](https://gitlab.com/biomedit/next-widgets/commit/8737e604ae08ed206ae5d66986006712bf4264ae))

## [6.6.0](https://gitlab.com/biomedit/next-widgets/compare/6.5.1...6.6.0) (2021-11-30)


### Features

* **EnhancedTable:** make column width customizable ([6670b03](https://gitlab.com/biomedit/next-widgets/commit/6670b0391eae2227586c2b05ea69edb6b8efe2b1))

### [6.5.1](https://gitlab.com/biomedit/next-widgets/compare/6.5.0...6.5.1) (2021-11-23)

## [6.5.0](https://gitlab.com/biomedit/next-widgets/compare/6.4.0...6.5.0) (2021-11-20)


### Features

* **utils/seasonal:** add new seasonal functions ([4346aa6](https://gitlab.com/biomedit/next-widgets/commit/4346aa60718ccd8f77b645ed1f84c1e436a903c6))

## [6.4.0](https://gitlab.com/biomedit/next-widgets/compare/6.3.0...6.4.0) (2021-11-17)


### Features

* **AutocompleteField:** Autocomplete `TextField` size should be parameterizable with default to 'small' ([9839d21](https://gitlab.com/biomedit/next-widgets/commit/9839d21406b79e3e7dd1749f33c43403d025c179))


### Bug Fixes

* **AutocompleteField:** clear selected choice(s) if they are not present in choices (anymore) ([884b95f](https://gitlab.com/biomedit/next-widgets/commit/884b95f3903ed4a2139086f7206fd04fc538b411))

## [6.3.0](https://gitlab.com/biomedit/next-widgets/compare/6.2.0...6.3.0) (2021-11-12)


### Features

* use correlationId from header of a response in error handling ([3f23fc3](https://gitlab.com/biomedit/next-widgets/commit/3f23fc39c8a496cb4cdb3129b4a3dda7ba65d991))

## [6.2.0](https://gitlab.com/biomedit/next-widgets/compare/6.1.0...6.2.0) (2021-11-10)


### Features

* **SelectField:** make `SelectField` more customizable ([386e2a7](https://gitlab.com/biomedit/next-widgets/commit/386e2a7ec2ecc55fb4a60251f8c39cae7e34539d))

## [6.1.0](https://gitlab.com/biomedit/next-widgets/compare/6.0.2...6.1.0) (2021-11-02)


### Features

* **AutocompleteField:** allow passing in an `onChange` callback ([76de326](https://gitlab.com/biomedit/next-widgets/commit/76de326d5fe0a467a0f13bf2168ea84a2355b895))

### [6.0.2](https://gitlab.com/biomedit/next-widgets/compare/6.0.1...6.0.2) (2021-10-28)

### [6.0.1](https://gitlab.com/biomedit/next-widgets/compare/6.0.0...6.0.1) (2021-10-21)


### Bug Fixes

* **HiddenField:** allow falsy values like `false` and `0` to be used as `value` ([0a9770d](https://gitlab.com/biomedit/next-widgets/commit/0a9770d59ab1cc1fe9914456eb890e53f31e2acf))
* **HiddenField:** make sure form state is always updated when `defaultValue` changes ([1e09d1f](https://gitlab.com/biomedit/next-widgets/commit/1e09d1ff3599cb16fa9cab32076cdf39d0a5ab44)), closes [portal#458](https://gitlab.com/biomedit/portal/issues/458)
* **sagas:** fix type error ([24e51e8](https://gitlab.com/biomedit/next-widgets/commit/24e51e80b5b2d218b37f755af199581df3637d30))

## [6.0.0](https://gitlab.com/biomedit/next-widgets/compare/5.0.0...6.0.0) (2021-10-13)


### ⚠ BREAKING CHANGES

* **npm:** It is only possible to import `from '@biomedit/next-widgets'`.
Importing from a subpath, like `from '@biomedit/next-widgets/esm/lib/widgets/Tooltip'`
is no longer possible.

Not having an export map allows importing from any file, even if it is not exported in
the main `index.ts` file.
This means users could accidentally import from private files we do not choose to
export.
Should we change something in a non-exported file and a user accidentally imported it,
it could result in a breaking change for them even if we do not declare a release to
include one.
An additional advantage is that IDE's will show less imports to choose
from, as previously it would also include all subpaths, now IDE's should be able
to unambiguously import from `next-widgets`.

To migrate, change all imports in your application to use `from '@biomedit/next-widgets'`
exclusively.

For example, change the following code:
```
import { Tooltip } from '@biomedit/next-widgets/esm/lib/widgets/Tooltip';
```
into the following:
```
import { Tooltip } from '@biomedit/next-widgets';
```

Signed-off-by: martinfrancois <f.martin@fastmail.com>
* **npm:** material-ui version 4 was replaced with material-ui version 5.

To migrate, [migrate your application to use material-ui version 5](https://mui.com/guides/migration-v4/#migrate-from-jss).
* The styles applied to `UserMenuItem` are now the default styling
of `ListItemAction` and `ListItemButton`.
The `UserMenuItem` component was removed.

The `UserMenuItem` component was merely a styled version of the `ListItemAction`
and `LogoutMenuItem` applied the same styles as `UserMenuItem` but on
`ListItemButton`.
Moving the styling from `UserMenuItem` to `ListItemBase` makes `UserMenuItem`
just an alias to the `ListItemAction` component, making it unnecessary.

Defining the styles applied to `UserMenuItem` as the default style for
`ListItemAction` and `ListItemButton` might lead to unexpected style
changes for users of those components directly, but it can be overwritten if
not desired.

To migrate, use the `ListItemAction` component instead of the `UserMenuItem`
component.
If you are using `ListItemAction` or `ListItemButton` and find the new styling
undesirable, pass the `textClasses` and `iconClasses` props to customize the
styling.
* `StepConnector` is no longer exported.

It does not make sense to use the `StepConnector` component
on its own, but rather to use `Stepper` instead.
There are no disadvantages to using `Stepper`, as
`StepConnector` anyways only makes sense to be used together
with the `Stepper` component.

To migrate, use `Stepper` from `next-widgets` directly instead
of using `StepConnector` in your application in a `Stepper`
from material-ui.

* migrate from JSS to emotion ([65f802b](https://gitlab.com/biomedit/next-widgets/commit/65f802b912f05a2a6a4b109f50608ca215ad8097)), closes [#17](https://gitlab.com/biomedit/next-widgets/issues/17)
* move `UserMenuItem` and `LogoutMenuItem` styles into `ListItemBase` ([4e81861](https://gitlab.com/biomedit/next-widgets/commit/4e81861c9a7097abc803404f2f6e788de9573043)), closes [#17](https://gitlab.com/biomedit/next-widgets/issues/17)


### build

* **npm:** add export map ([61c7439](https://gitlab.com/biomedit/next-widgets/commit/61c743928adb62101b34865bc4af7152176392ad))
* **npm:** remove material-ui 4 dependencies ([63f9936](https://gitlab.com/biomedit/next-widgets/commit/63f99364ece45a6c105a9ce71e498d7761af96ca)), closes [#17](https://gitlab.com/biomedit/next-widgets/issues/17)

## [5.0.0](https://gitlab.com/biomedit/next-widgets/compare/4.6.0...5.0.0) (2021-09-30)


### ⚠ BREAKING CHANGES

* **actionTypes & sagas:** The `apiFunction` argument on the `takeApi` method
(returned by `latest` and `leading` of `takeApiFactory`)
was moved to the `declareAction` method as a mandatory second argument.

Passing the `apiFunction` to the `takeApi` method makes it impossible to perform
type checks when dispatching an action to ensure only properties which are
available can be used, as the `takeApi` method is used for defining the sagas
that are aggregated in a `rootSaga` function, after which the necessary
types cannot be inferred anymore.

Adding the `apiFunction` argument to `declareAction` allows when passing the
resulting `ActionType` to the `requestAction` method to create an action which
restricts typing to only allow request parameters which are available for the
respective `apiFunction`.

To migrate, move the value of the `apiFunction` argument from the `.latest(` and `.leading(`
method calls to the `declareAction` method of the corresponding `ActionType`,
omitting ".request" (to pass in the `ActionType` directly).
Then, change all dispatches of actions related to `ActionType` to create the
action using the `requestAction` method instead of creating an action object
directly.
Make sure to move `onSuccess` if previously used in the action object's properties
to the third argument of the `requestAction` method instead of including it in the
`requestParams`.

For example, change the following code:
```tsx
yield takeApi.latest(LOAD_PROJECTS, api.listProjects),

export const LOAD_PROJECTS = declareAction('LOAD_PROJECTS');

dispatch({ type: LOAD_PROJECTS.request, ordering: 'name', onSuccess: { type: "SUCCESS" } });
```
into the following:
```tsx
yield takeApi.latest(LOAD_PROJECTS),

export const LOAD_PROJECTS = declareAction('LOAD_PROJECTS', api.listProjects);

dispatch(requestAction(LOAD_PROJECTS, { ordering: 'name' }, { type: "SUCCESS" }));
```
* **types:** An object of type `Action` must be provided to the `action` property
in the props of `ListItemAction` and `UserMenuItem` components and the `UserMenuItemModel` type.

Using `ActionType` in the `ListItemAction` and `UserMenuItem` component and the `UserMenuItemModel`
limits the usage to actions which perform API-like functions (which require actions like `_REQUEST`, `_SUCCESS` etc.).
However, this limits the flexibility in which actions can be used, which is why the type was changed to `Action` instead.

To migrate, add to the value of the `action` prop passed to the `ListItemAction` and `UserMenuItem` components and in
the `UserMenuItemModel` type a suffix of `.request`.
For example, change the following code:
```tsx
export const ADD_ACTION = declareAction('ADD_ACTION');
<ListItemAction action={ADD_ACTION} />
```
into the following:
```tsx
export const ADD_ACTION = declareAction('ADD_ACTION');
<ListItemAction action={ADD_ACTION.request} />
```

### Bug Fixes

* **frontend/ListPage:** fix DOM nesting bug on ListPage ([99a6e65](https://gitlab.com/biomedit/next-widgets/commit/99a6e657b6683f70eec38b5a74ee172a731703ab)), closes [#16](https://gitlab.com/biomedit/next-widgets/issues/16)


* **actionTypes & sagas:** improve type safety of redux actions involving API calls ([19a4e82](https://gitlab.com/biomedit/next-widgets/commit/19a4e8255e5465dcbb9ed45fb5433e152c8b94ce)), closes [#2](https://gitlab.com/biomedit/next-widgets/issues/2)
* **types:** change the type of `action` in `ListItemActionProps`, `UserMenuItemProps` and `UserMenuItemModel` from `ActionType` to `Action` ([39a8352](https://gitlab.com/biomedit/next-widgets/commit/39a83525c0aa291cbf3e5525c4a65c7f232476d8)), closes [#2](https://gitlab.com/biomedit/next-widgets/issues/2)

## [4.6.0](https://gitlab.com/biomedit/next-widgets/compare/4.5.0...4.6.0) (2021-09-29)


### Features

* **ListPage:** add a separator between list item's content and action buttons ([17189ee](https://gitlab.com/biomedit/next-widgets/commit/17189ee8c74d6b22a25202ddf5a022b94d71110d))

## [4.5.0](https://gitlab.com/biomedit/next-widgets/compare/4.4.1...4.5.0) (2021-09-24)


### Features

* **Timeline:** add possibility to customize title and message `Typography` components ([dab7e61](https://gitlab.com/biomedit/next-widgets/commit/dab7e61f3867678c278d4cfdf5d488924634d1e4))

### [4.4.1](https://gitlab.com/biomedit/next-widgets/compare/4.4.0...4.4.1) (2021-09-16)


### Bug Fixes

* **frontend/Timeline:** prevent message date to overlap with message icon. ([d1e85a0](https://gitlab.com/biomedit/next-widgets/commit/d1e85a0ccb674cec4536458030f109aa2eff0958))

## [4.4.0](https://gitlab.com/biomedit/next-widgets/compare/4.3.2...4.4.0) (2021-09-13)


### Features

* **Buttons:** add tooltip to `AddIconButton`, `EditIconButton` and `DeleteIconButton` ([f067b75](https://gitlab.com/biomedit/next-widgets/commit/f067b755d1a3eb989307e760af90001aaa3717d4))
* **EnhancedTable:** allow `canEdit` and `canDelete` to be functions ([2abd087](https://gitlab.com/biomedit/next-widgets/commit/2abd0876e14b7414835d5a32043ac0f29b1759eb))

### [4.3.2](https://gitlab.com/biomedit/next-widgets/compare/4.3.1...4.3.2) (2021-09-09)


### Bug Fixes

* **EnhancedTable:** fix a bug where updating `canEdit` or `canDelete` after the first render would NOT have an effect ([cb27cb0](https://gitlab.com/biomedit/next-widgets/commit/cb27cb09ec6baeb5aa7aea3fb6662e1ed2e91226))

### [4.3.1](https://gitlab.com/biomedit/next-widgets/compare/4.3.0...4.3.1) (2021-08-20)

## [4.3.0](https://gitlab.com/biomedit/next-widgets/compare/4.2.0...4.3.0) (2021-08-18)


### Features

* **Description:** allow styling of 'Description' ([a8cd401](https://gitlab.com/biomedit/next-widgets/commit/a8cd401ee8cd8bd88c38d870c6aa3d38e9183077))
* **PageBase:** title could be specified as array ([38c3ffe](https://gitlab.com/biomedit/next-widgets/commit/38c3ffea063f68c705466800e4e5f279d7d8c303))

## [4.2.0](https://gitlab.com/biomedit/next-widgets/compare/4.1.0...4.2.0) (2021-08-17)


### Features

* **Buttons:** add "Archive" and "Unarchive" icon buttons ([7f56d8c](https://gitlab.com/biomedit/next-widgets/commit/7f56d8c37edfc26be0820154a5b753280fd3b2b8))
* **DeleteDialog:** prompt user to type the name in a text field to confirm deletion when `deleteItem` is specified ([e7e4548](https://gitlab.com/biomedit/next-widgets/commit/e7e4548a1068fb140cd200df1a9cb1357a2840d0))
* **ListPage:** allow adding additional action buttons to `ListPage` ([361c74f](https://gitlab.com/biomedit/next-widgets/commit/361c74f343f52937435e3dbee87c122d5e9f2b85))

## [4.1.0](https://gitlab.com/biomedit/next-widgets/compare/4.0.1...4.1.0) (2021-08-05)


### Features

* **Markdown:** open links in markdown in a new tab when the prop `openLinksInNewTab` is set to `true` ([2f229bd](https://gitlab.com/biomedit/next-widgets/commit/2f229bd4b89688d74897139af2f66791758d84b0))


### Bug Fixes

* **SelectField:** show required error when losing focus without selection ([8136a76](https://gitlab.com/biomedit/next-widgets/commit/8136a769263dbb2b5afd42623d75dc0fbb88ef5a))
* **SelectField:** show select control in red in case of an error ([a9def18](https://gitlab.com/biomedit/next-widgets/commit/a9def18a38555869007bbb38f96ceae693301e56))

### [4.0.1](https://gitlab.com/biomedit/next-widgets/compare/4.0.0...4.0.1) (2021-07-19)

## [4.0.0](https://gitlab.com/biomedit/next-widgets/compare/3.4.1...4.0.0) (2021-07-19)


### ⚠ BREAKING CHANGES

* **EnhancedTable:** `@material-ui/core` version 4.12.0 included a
* **EnhancedTable:** in the types, which means this version is affected as well.

To migrate, upgrade the version of `@material-ui/core` in your `package.json` to at least **4.12.1**.
In order to use a version of `@material-ui/core` **prior to 4.12.0**, use `next-widgets` version **3.4.0 or lower**.

### Bug Fixes

* **EnhancedTable:** apply changes to prop name due to changes after upgrading to `@material-ui/core` version 4.12.1 ([5c896f4](https://gitlab.com/biomedit/next-widgets/commit/5c896f4eeddc3e0ea4e7a4546c9e8b1fbbf59396)), closes [/github.com/mui-org/material-ui/pull/23789#issuecomment-876249753](https://github.com/mui-org/material-ui/pull/23789/issues/issuecomment-876249753)

### [3.4.1](https://gitlab.com/biomedit/next-widgets/compare/3.4.0...3.4.1) (2021-07-19)


### Bug Fixes

* **SelectField:** prevent error from occurring when a `value` is used in a `SelectField` which doesn't exist in `choices` ([6fc7a32](https://gitlab.com/biomedit/next-widgets/commit/6fc7a32109cde1af27e33e6d8d1af270e3c669e5))

## [3.4.0](https://gitlab.com/biomedit/next-widgets/compare/3.3.0...3.4.0) (2021-07-16)


### Features

* **testUtils:** add method `expectToBeInTheDocument` ([416c79a](https://gitlab.com/biomedit/next-widgets/commit/416c79a1d2dca66b164dabb6b2dc279c8d5ec951))

## [3.3.0](https://gitlab.com/biomedit/next-widgets/compare/3.2.1...3.3.0) (2021-07-12)


### Features

* **sortTypes:** add support for `accessor` with array values when using it together with `sortType: caseInsensitive` ([ed70cbe](https://gitlab.com/biomedit/next-widgets/commit/ed70cbe1fa62504f1613e0ac53bac3aacb01e23e))

### [3.2.1](https://gitlab.com/biomedit/next-widgets/compare/3.2.0...3.2.1) (2021-07-05)


### Bug Fixes

* **testUtils/RequestVerifier:** always show all requests if an assertion fails ([c3efbf2](https://gitlab.com/biomedit/next-widgets/commit/c3efbf2157c6e394f06b1b47ed2d9d81ff440f68))

## [3.2.0](https://gitlab.com/biomedit/next-widgets/compare/3.1.0...3.2.0) (2021-07-02)


### Features

* **testUtils:** add method `fillTextboxes` ([4575b1a](https://gitlab.com/biomedit/next-widgets/commit/4575b1a49920780ba23c9b15df40d462880361ab))

## [3.1.0](https://gitlab.com/biomedit/next-widgets/compare/3.0.1...3.1.0) (2021-07-02)


### Features

* **testUtils:** support mocking the `Trans` component as well with `mockI18n` ([96935d0](https://gitlab.com/biomedit/next-widgets/commit/96935d089e72ecf8dc93c9791d4203e5f130a7c8))

### [3.0.1](https://gitlab.com/biomedit/next-widgets/compare/3.0.0...3.0.1) (2021-07-01)

## [3.0.0](https://gitlab.com/biomedit/next-widgets/compare/2.5.6...3.0.0) (2021-07-01)


### ⚠ BREAKING CHANGES

* **utils:** Since required parameters cannot follow optional parameters (TS1016) and `email` is a required parameter,
it wouldn't be possible to make this change without changing the order of the parameters to have the `email` as the first
parameter.
An alternative would've been to make the `emaiL` address optional as well, but since it doesn't really make sense to have
a `mailto` without an email address, this doesn't make sense.

To migrate, change all calls to `mailto` to put the `email` parameter first, instead of last.
The new signature is the following: `email: string, subject?: string, body?: string`

### Features

* **utils:** make `subject` and `body` of the `mailto` function optional ([b53fc05](https://gitlab.com/biomedit/next-widgets/commit/b53fc05954ecb420fa8c3705952a27e76af59c64))
* **widgets:** add `EmailLink` component ([60e41f0](https://gitlab.com/biomedit/next-widgets/commit/60e41f0f36c665784d52e19ff9372b5b27069e57))

### [2.5.6](https://gitlab.com/biomedit/next-widgets/compare/2.5.5...2.5.6) (2021-07-01)

### [2.5.5](https://gitlab.com/biomedit/next-widgets/compare/2.5.4...2.5.5) (2021-07-01)

### [2.5.4](https://gitlab.com/biomedit/next-widgets/compare/2.5.3...2.5.4) (2021-06-28)


### Bug Fixes

* **CheckboxField:** remove materialui warning about `classes` receiving prop of type `boolean` ([4cecd1b](https://gitlab.com/biomedit/next-widgets/commit/4cecd1bc289aa1d49300a64d70cb11aac6550132))

### [2.5.3](https://gitlab.com/biomedit/next-widgets/compare/2.5.2...2.5.3) (2021-06-28)

### [2.5.2](https://gitlab.com/biomedit/next-widgets/compare/2.5.1...2.5.2) (2021-06-25)

### [2.5.1](https://gitlab.com/biomedit/next-widgets/compare/2.5.0...2.5.1) (2021-06-25)

## [2.5.0](https://gitlab.com/biomedit/next-widgets/compare/2.4.0...2.5.0) (2021-06-25)


### Features

* **ExternalLink:** add widget to make an external link which opens in a new tab by default ([1c246e2](https://gitlab.com/biomedit/next-widgets/commit/1c246e2c958a0c5c674040307234a8f5d100d4bc))

## [2.4.0](https://gitlab.com/biomedit/next-widgets/compare/2.3.0...2.4.0) (2021-06-25)


### Features

* **CheckboxField:** add visual feedback when required validation fails ([299f532](https://gitlab.com/biomedit/next-widgets/commit/299f532f47b194c6cba1badf74250e899045190f))

## [2.3.0](https://gitlab.com/biomedit/next-widgets/compare/2.2.0...2.3.0) (2021-06-25)


### Features

* **CheckboxField:** enable validating a `CheckboxField` to be `required` ([3ab66b9](https://gitlab.com/biomedit/next-widgets/commit/3ab66b9bac0e0629cddf3130bff9abeb5ccf8224))

## [2.2.0](https://gitlab.com/biomedit/next-widgets/compare/2.1.0...2.2.0) (2021-06-24)


### Features

* **sagas:** allow to define error status codes (4xx - 5xx) which are handled in `takeApi` as an additional optional parameter ([8776877](https://gitlab.com/biomedit/next-widgets/commit/87768777704ab13e49233ab8d9024da82201ecbc))

## [2.1.0](https://gitlab.com/biomedit/next-widgets/compare/2.0.1...2.1.0) (2021-06-23)


### Features

* **FixedChildrenHeight:** forward other props which are passed in to `Box` ([4c552ac](https://gitlab.com/biomedit/next-widgets/commit/4c552ac8284d872a6d4ab322d25c4816fc0b181d))

### [2.0.1](https://gitlab.com/biomedit/next-widgets/compare/2.0.0...2.0.1) (2021-06-22)


### Bug Fixes

* circular dependencies ([93d005e](https://gitlab.com/biomedit/next-widgets/commit/93d005e022f624f390ae1db1bae567942ac7350b))

## [2.0.0](https://gitlab.com/biomedit/next-widgets/compare/1.1.1...2.0.0) (2021-06-21)


### ⚠ BREAKING CHANGES

* The `getTestFileAsString` method is very specific to a setup and
depends on the folder structure of `node_modules`, if the method is not used in
the app itself.
Therefore it doesn't make sense to include it in a library, but to keep it in the
apps themselves.

To migrate, copy the implementation to your own code.

* remove `getTestFileAsString` method ([ab98258](https://gitlab.com/biomedit/next-widgets/commit/ab98258bc093d022db5f06f0502c1715c66a02e2))

### [1.1.1](https://gitlab.com/biomedit/next-widgets/compare/1.1.0...1.1.1) (2021-06-21)

## [1.1.0](https://gitlab.com/biomedit/next-widgets/compare/1.0.1...1.1.0) (2021-06-18)


### Features

* **EnhancedTable:** allow disabling filtering and pagination ([fce58e1](https://gitlab.com/biomedit/next-widgets/commit/fce58e16398d7483c48f3d33105351709d42d76c))

### 1.0.1 (2021-06-11)
